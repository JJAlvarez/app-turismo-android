package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.InformationLugarEvent;
import com.javieralvarez.kinal.app_turismo.interactors.InformationLugarInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.InformationLugarInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.InformationLugarView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationLugarPresenterImpl implements InformationLugarPresenter {

    private EventBus eventBus;
    private InformationLugarView view;
    private InformationLugarInteractor interactor;

    public InformationLugarPresenterImpl(InformationLugarView view, Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.view = view;
        this.interactor = new InformationLugarInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    @Subscribe
    public void onEventMainThread(InformationLugarEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onLugarReady(event.getLugares());
            } else {
                view.onError(event.getError());
            }
        }
    }

    @Override
    public void getLugar(int id) {
        interactor.execute(id);
    }
}
