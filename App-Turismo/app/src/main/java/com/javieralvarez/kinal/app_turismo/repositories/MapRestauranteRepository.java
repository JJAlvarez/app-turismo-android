package com.javieralvarez.kinal.app_turismo.repositories;

/**
 * Created by Javier on 20/07/2016.
 */
public interface MapRestauranteRepository {

    void execute(int idRestaurante);
}
