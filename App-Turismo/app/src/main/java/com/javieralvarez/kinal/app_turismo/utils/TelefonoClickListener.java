package com.javieralvarez.kinal.app_turismo.utils;

/**
 * Created by Javier on 15/07/2016.
 */
public interface TelefonoClickListener {

    void onTelefonoClick(int telefono);
}
