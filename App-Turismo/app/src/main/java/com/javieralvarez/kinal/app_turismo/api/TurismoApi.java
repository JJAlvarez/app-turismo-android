package com.javieralvarez.kinal.app_turismo.api;

import com.javieralvarez.kinal.app_turismo.model.Comentarios;
import com.javieralvarez.kinal.app_turismo.model.Departamento;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.model.ServerResponse;
import com.javieralvarez.kinal.app_turismo.model.Servicio;
import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;
import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;
import com.javieralvarez.kinal.app_turismo.model.Token;
import com.javieralvarez.kinal.app_turismo.model.TokenTwitter;
import com.javieralvarez.kinal.app_turismo.model.Usuario;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Javier on 28/04/2016.
 */
public interface TurismoApi {

    String API = "http://192.168.1.8:3000";
    String TURISMO_REST_PUBLIC_API = "/api/v1/";

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "usuario/login")
    Call<Token> login(@Field("correo") String correo, @Field("password") String password);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "comentariorestaurante")
    Call<Comentarios> addComentarioRestaurante(@Header("Authorization") String authorization, @Field("id_restaurante") int id,
                                    @Field("comentario")String comentario, @Field("usuario") String usuario);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "comentariohotel")
    Call<Comentarios> addComentarioHotel(@Header("Authorization") String authorization, @Field("id_hotel") int id,
                                    @Field("comentario")String comentario, @Field("usuario") String usuario);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "comentariolugar")
    Call<Comentarios> addComentarioLugar(@Header("Authorization") String authorization, @Field("id_lugarturistico") int id,
                                    @Field("comentario")String comentario, @Field("usuario") String usuario);

    @DELETE(TURISMO_REST_PUBLIC_API + "comentario/{id}")
    Call<Comentarios> deleteComentario(@Header("Authorization") String authorization, @Path("id") int id);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "usuario/registro")
    Call<Usuario> registro(@Field("nombre") String nombre, @Field("nick") String nick, @Field("telefono") int telefono
                            ,@Field("correo") String correo, @Field("password") String password, @Field("direccion") String direccion);

    @GET(TURISMO_REST_PUBLIC_API + "departamento")
    Call<List<Departamento>> getAllDepartamentos(@Header("Authorization") String authorization);

    @GET(TURISMO_REST_PUBLIC_API + "departamento/restaurante/{id}")
    Call<List<Restaurante>> getRestaurantesDepartamento(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "departamento/hotel/{id}")
    Call<List<Hotel>> getHotelesDepartamento(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "departamento/lugar/{id}")
    Call<List<LugarTuristico>> getLugaresDepartamento(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "comentariohotel/{id}")
    Call<List<Comentarios>> getHotelComentarios(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "comentariorestaurante/{id}")
    Call<List<Comentarios>> getRestauranteComentarios(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "comentariolugar/{id}")
    Call<List<Comentarios>> getLugarComentarios(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "sucursalrestaurante/restaurante/{id}")
    Call<List<SucursalRestaurante>> getRestaurantSucursales(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "sucursalhotel/hotel/{id}")
    Call<List<SucursalHotel>> getHotelSucursales(@Header("Authorization") String authorization, @Path("id") int id);

    @GET(TURISMO_REST_PUBLIC_API + "token_twitter")
    Call<TokenTwitter> getTokenForTwitterUser();

    @GET(TURISMO_REST_PUBLIC_API + "hotel")
    Call<List<Hotel>> getAllHotels(@Header("Authorization") String authorization );

    @GET(TURISMO_REST_PUBLIC_API + "restaurante")
    Call<List<Restaurante>> getAllRestaurantes(@Header("Authorization") String authorization);

    @GET(TURISMO_REST_PUBLIC_API + "lugarturistico")
    Call<List<LugarTuristico>> getAllLugares(@Header("Authorization") String authorization);

    @GET(TURISMO_REST_PUBLIC_API + "servicio")
    Call<List<Servicio>> getAllServicios();

    @GET(TURISMO_REST_PUBLIC_API + "sucursalhotel")
    Call<List<SucursalHotel>> getAllSucursalesHotel();

    @GET(TURISMO_REST_PUBLIC_API + "sucursalrestaurante")
    Call<List<SucursalRestaurante>> getAllSucursalRestaurante();

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "departamento")
    void agregarDepartamento(@Field("nombre") String nombre, @Field("descripcion") String descripcion,
                             Callback<Departamento> departamentoCallback);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "hotel")
    void agregarHotel(@Field("nombre") String nombre, @Field("clasificacion") int clasificacion,
                      Callback<Hotel> hotelCallback);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "restaurante")
    void agregarRestaurante(@Field("nombre") String nombre, @Field("clasificacion") int clasificacion,
                            Callback<Restaurante> restauranteCallback);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "servicio")
    void agregarServicio(@Field("nombre") String nombre, @Field("descripcion") String descripcion,
                         Callback<Servicio> servicioCallback);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "sucursalhotel")
    void agregarSucursalHotel(@Field("direccion") String direccion, @Field("id_hotel") int id_hotel, @Field("id_departamento") int id_departamento,
                              Callback<SucursalHotel> sucursalHotelCallback);

    @FormUrlEncoded
    @POST(TURISMO_REST_PUBLIC_API + "sucursalrestaurante")
    void agregarSucursalRestaurante(@Field("direccion") String direccion, @Field("id_restaurante") int id_restaurante, @Field("id_departamento") int id_departamento,
                                    Callback<SucursalRestaurante> sucursalRestauranteCallback);

    @FormUrlEncoded
    @DELETE(TURISMO_REST_PUBLIC_API + "departamento")
    void eliminarDepartamento(@Field("id_departamento") int idDepartamento, Callback<ServerResponse> serverResponseCallback);

    @FormUrlEncoded
    @DELETE(TURISMO_REST_PUBLIC_API + "hotel")
    void eliminarHotel(@Field("id_hotel") int idHotel, Callback<ServerResponse> serverResponseCallback);

    @FormUrlEncoded
    @DELETE(TURISMO_REST_PUBLIC_API + "restaurante")
    void eliminarRestaurante(@Field("id_restaurante") int idRestaurante, Callback<ServerResponse> serverResponseCallback);

    @FormUrlEncoded
    @DELETE(TURISMO_REST_PUBLIC_API + "servicio")
    void eliminarServicio(@Field("id_servicio") int idServicio, Callback<ServerResponse> serverResponseCallback);

    @FormUrlEncoded
    @DELETE(TURISMO_REST_PUBLIC_API + "sucursalhotel")
    void eliminarSucursalHotel(@Field("id_sucursal") int idSucursal, Callback<ServerResponse> serverResponseCallback);

    @FormUrlEncoded
    @DELETE(TURISMO_REST_PUBLIC_API + "sucursalrestaurante")
    void eliminarSucursalRestaurante(@Field("id_sucursal") int idSucursal, Callback<ServerResponse> serverResponseCallback);

    @FormUrlEncoded
    @PUT(TURISMO_REST_PUBLIC_API + "departamento")
    void editarDepartamento(@Field("id_departamento") int idDepartamento, @Field("nombre") String nombre,
                            @Field("descripcion") String descripcion, Callback<Departamento> departamentoCallback);

    @FormUrlEncoded
    @PUT(TURISMO_REST_PUBLIC_API + "hotel")
    void editarHotel(@Field("id_hotel") int idHotel, @Field("nombre") String nombre,
                            @Field("clasificacion") int clasificacion, Callback<Hotel> hotelCallback);

    @FormUrlEncoded
    @PUT(TURISMO_REST_PUBLIC_API + "restaurante")
    void editarRestaurante(@Field("id_restaurante") int idRestaurante, @Field("nombre") String nombre,
                           @Field("clasificacion") int clasificacion, Callback<Restaurante> restauranteCallback);

    @FormUrlEncoded
    @PUT(TURISMO_REST_PUBLIC_API + "servicio")
    void editarServicio(@Field("id_servicio") int idDepartamento, @Field("nombre") String nombre,
                            @Field("descripcion") String descripcion, Callback<Servicio> servicioCallback);

    @FormUrlEncoded
    @PUT(TURISMO_REST_PUBLIC_API + "sucursalhotel")
    void editarSucursalHotel(@Field("id_sucursal") int idSucursal, @Field("direccion") String direccion, @Field("id_departamento") int idDepartamento,
                             @Field("id_hotel") int idHotel, Callback<SucursalHotel> sucursalHotelCallback);

    @FormUrlEncoded
    @PUT(TURISMO_REST_PUBLIC_API + "sucursalrestaurante")
    void editarSucursalRestaurante(@Field("id_sucursal") int idSucursal, @Field("direccion") String direccion, @Field("id_departamento") int idDepartamento,
                                   @Field("id_restaurante") int idRestaurante, Callback<SucursalRestaurante> sucursalRestauranteCallback);

    @GET(TURISMO_REST_PUBLIC_API + "departamento/{id}")
    Call<Departamento> departamentoPorId(@Header("Authorization") String authorization, @Path("id") int id_departamento);

    @GET(TURISMO_REST_PUBLIC_API + "hotel/{id}")
    Call<Hotel> hotelPorId(@Header("Authorization") String authorization, @Path("id") int id_hotel);

    @GET(TURISMO_REST_PUBLIC_API + "restaurante/{id}")
    Call<Restaurante> restaurantePorId(@Header("Authorization") String authorization, @Path("id") int id_restaurante);

    @GET(TURISMO_REST_PUBLIC_API + "lugarturistico/{id}")
    Call<LugarTuristico> lugarPorId(@Header("Authorization") String authorization, @Path("id") int id_lugar);

    @GET(TURISMO_REST_PUBLIC_API + "sucursalrestaurante/{id}")
    Call<SucursalRestaurante> sucursalRestaurantePorId(@Path("id") int id_sucursal);

    @GET(TURISMO_REST_PUBLIC_API + "sucursalhotel/{id}")
    Call<SucursalHotel> sucursalHotelPorId(@Path("id") int id_sucursal);
}
