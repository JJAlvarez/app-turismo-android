package com.javieralvarez.kinal.app_turismo.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.DepartamentoAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.FragmentDepartamentoAdapter;
import com.javieralvarez.kinal.app_turismo.model.Departamento;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetallesDepartamentoActivity extends AppCompatActivity {

    @Bind(R.id.toolbar_detalles_departamento)
    Toolbar toolbarDetallesDepartamento;
    @Bind(R.id.tabs_departamento)
    TabLayout tabsDepartamento;
    @Bind(R.id.viewpager)
    ViewPager viewpager;

    private Departamento mDepartamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_departamento);
        ButterKnife.bind(this);

        setSupportActionBar(toolbarDetallesDepartamento);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setDepartmentTitle();
        setViewPager();
        setTabs();
    }

    private void setDepartmentTitle() {
        if (getIntent().hasExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG)) {
            mDepartamento = getIntent().getParcelableExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG);
        }
        getSupportActionBar().setTitle(mDepartamento.getmNombre());
    }

    private void setTabs() {
        tabsDepartamento.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabsDepartamento.setupWithViewPager(viewpager);
    }

    private void setViewPager() {
        viewpager.setAdapter(new FragmentDepartamentoAdapter(
                getSupportFragmentManager()));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
