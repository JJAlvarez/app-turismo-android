package com.javieralvarez.kinal.app_turismo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.model.Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroActivity extends AppCompatActivity {

    private Button mBtnCancelar, mBtnRegistrar;
    private TextView mTxtNombre, mTxtTelefono, mTxtCorreo, mTxtNick, mTxtPassword, mTxtDireccion;
    private TurismoApp mApp;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        mApp = (TurismoApp)getApplication();

        mTxtNombre = (TextView) findViewById(R.id.txt_name_registro);
        mTxtTelefono = (TextView) findViewById(R.id.txt_phone_registro);
        mTxtCorreo = (TextView) findViewById(R.id.txt_email_registro);
        mTxtNick = (TextView) findViewById(R.id.txt_nick_registro);
        mTxtPassword = (TextView) findViewById(R.id.txt_password_registro);
        mTxtDireccion = (TextView) findViewById(R.id.txt_direccion_registro);

        this.mBtnCancelar = (Button) findViewById(R.id.btn_cancel);

        mBtnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        this.mBtnRegistrar = (Button) findViewById(R.id.btn_registrar);

        progress = new ProgressDialog(RegistroActivity.this);
        progress.setTitle(getString(R.string.registrando));
        progress.setMessage(getString(R.string.please_wait));

        mBtnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.show();
                mApp.getApi().registro(mTxtNombre.getText().toString(), mTxtNick.getText().toString(),
                        Integer.parseInt(mTxtTelefono.getText().toString()), mTxtCorreo.getText().toString(), mTxtPassword.getText().toString(),
                        mTxtDireccion.getText().toString()).enqueue(new Callback<Usuario>() {
                    @Override
                    public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                        if(response.body().getmIdUsuario() != 0 && response.body().getmNombre() != null) {
                            Toast.makeText(RegistroActivity.this, "Registro completado", Toast.LENGTH_SHORT).show();
                            Intent login = new Intent(RegistroActivity.this, InicioSesionActivity.class);
                            progress.dismiss();
                            startActivity(login);
                            finish();
                        } else {
                            Toast.makeText(RegistroActivity.this, "Campos Erroneos, vuelva a intentarlo", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Usuario> call, Throwable t) {

                    }
                });
            }
        });
    }
}
