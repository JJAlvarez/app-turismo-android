package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 27/04/2016.
 */
public class Servicio implements Parcelable{

    @SerializedName("id_servicio")
    private int mIdServicio;

    @SerializedName("nombre")
    private String mNombre;

    @SerializedName("descripcion")
    private String mDescripcion;

    public Servicio() {
    }

    public int getmIdServicio() {
        return mIdServicio;
    }

    public void setmIdServicio(int mIdServicio) {
        this.mIdServicio = mIdServicio;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public String getmDescripcion() {
        return mDescripcion;
    }

    public void setmDescripcion(String mDescripcion) {
        this.mDescripcion = mDescripcion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIdServicio);
        dest.writeString(this.mNombre);
        dest.writeString(this.mDescripcion);
    }

    protected Servicio(Parcel in) {
        this.mIdServicio = in.readInt();
        this.mNombre = in.readString();
        this.mDescripcion = in.readString();
    }

    public static final Creator<Servicio> CREATOR = new Creator<Servicio>() {
        @Override
        public Servicio createFromParcel(Parcel source) {
            return new Servicio(source);
        }

        @Override
        public Servicio[] newArray(int size) {
            return new Servicio[size];
        }
    };
}
