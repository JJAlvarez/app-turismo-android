package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesLugaresEvent {

    private String error;
    private List<LugarTuristico> lugares;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<LugarTuristico> getLugares() {
        return lugares;
    }

    public void setLugares(List<LugarTuristico> lugares) {
        this.lugares = lugares;
    }
}
