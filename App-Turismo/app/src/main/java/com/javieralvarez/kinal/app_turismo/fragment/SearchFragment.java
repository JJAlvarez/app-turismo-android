package com.javieralvarez.kinal.app_turismo.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.FavoritesFragmentAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {

    @Bind(R.id.appbartabs)
    TabLayout appbartabs;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    private String query;
    public static final String QUERY = "query";
    public static final String TIPO = "tipo";

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static enum TIPO_BUSQUEDA {
        BUSQUEDA_FAVORITO,
        BUSQUEDA_TODOS
    }

    private TIPO_BUSQUEDA tipoBusqueda;

    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance(String query, TIPO_BUSQUEDA tipo) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(QUERY, query);
        args.putSerializable(TIPO, tipo);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        setViewPager();
        setTabs();
        return view;
    }

    public void setQuery(String query) {

    }

    private void setTabs() {
        appbartabs.setTabMode(TabLayout.MODE_FIXED);
        appbartabs.setupWithViewPager(viewpager);
    }

    private void setViewPager() {
        viewpager.setOffscreenPageLimit(3);
        viewpager.setAdapter(new FavoritesFragmentAdapter(
                getActivity().getSupportFragmentManager()));
    }
}
