package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 02/05/2016.
 */
public class SucursalHotel implements Parcelable {

    @SerializedName("id_sucursal")
    private int mIdSucursal;

    @SerializedName("direccion")
    private String mDireccion;

    @SerializedName("latitud")
    private float mLatitud;

    @SerializedName("longitud")
    private float mLongitud;

    @SerializedName("telefono")
    private int telefono;

    @SerializedName("hotelIdHotel")
    private int mIdHotel;

    @SerializedName("departamentoIdDepartamento")
    private int mIdDepartamento;

    public int getmIdSucursal() {
        return mIdSucursal;
    }

    public void setmIdSucursal(int mIdSucursal) {
        this.mIdSucursal = mIdSucursal;
    }

    public String getmDireccion() {
        return mDireccion;
    }

    public void setmDireccion(String mDireccion) {
        this.mDireccion = mDireccion;
    }

    public float getmLatitud() {
        return mLatitud;
    }

    public void setmLatitud(float mLatitud) {
        this.mLatitud = mLatitud;
    }

    public float getmLongitud() {
        return mLongitud;
    }

    public void setmLongitud(float mLongitud) {
        this.mLongitud = mLongitud;
    }

    public int getmIdHotel() {
        return mIdHotel;
    }

    public void setmIdHotel(int mIdHotel) {
        this.mIdHotel = mIdHotel;
    }

    public int getmIdDepartamento() {
        return mIdDepartamento;
    }

    public void setmIdDepartamento(int mIdDepartamento) {
        this.mIdDepartamento = mIdDepartamento;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIdSucursal);
        dest.writeString(this.mDireccion);
        dest.writeFloat(this.mLatitud);
        dest.writeFloat(this.mLongitud);
        dest.writeInt(this.mIdHotel);
        dest.writeInt(this.mIdDepartamento);
        dest.writeInt(this.telefono);
    }

    protected SucursalHotel(Parcel in) {
        this.mIdSucursal = in.readInt();
        this.mDireccion = in.readString();
        this.mLatitud = in.readFloat();
        this.mLongitud = in.readFloat();
        this.mIdHotel = in.readInt();
        this.mIdDepartamento = in.readInt();
        this.telefono = in.readInt();
    }

    public static final Parcelable.Creator<SucursalHotel> CREATOR = new Parcelable.Creator<SucursalHotel>() {
        @Override
        public SucursalHotel createFromParcel(Parcel source) {
            return new SucursalHotel(source);
        }

        @Override
        public SucursalHotel[] newArray(int size) {
            return new SucursalHotel    [size];
        }
    };
}
