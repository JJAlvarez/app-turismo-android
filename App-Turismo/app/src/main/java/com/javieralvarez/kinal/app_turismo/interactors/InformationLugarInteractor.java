package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 14/07/2016.
 */
public interface InformationLugarInteractor {

    void execute(int id);
}
