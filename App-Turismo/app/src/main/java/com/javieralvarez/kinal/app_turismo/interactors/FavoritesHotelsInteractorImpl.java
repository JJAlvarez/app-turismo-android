package com.javieralvarez.kinal.app_turismo.interactors;

import com.javieralvarez.kinal.app_turismo.repositories.FavoritesHotelsRepository;
import com.javieralvarez.kinal.app_turismo.repositories.FavoritesHotelsRepositoryImpl;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesHotelsInteractorImpl implements FavoritesHotelsInteractor {

    private FavoritesHotelsRepository repository;

    public FavoritesHotelsInteractorImpl() {
        this.repository = new FavoritesHotelsRepositoryImpl();
    }

    @Override
    public void execute() {
        repository.getHoteles();
    }
}
