package com.javieralvarez.kinal.app_turismo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;
import com.javieralvarez.kinal.app_turismo.utils.DeleteComentarioClickListener;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Javier on 27/06/2016.
 */
public class ComentariosAdapter extends RecyclerView.Adapter<ComentariosAdapter.ViewHolder> {

    private Context mContext;
    private List<Comentarios> mData;
    private DeleteComentarioClickListener listener;

    public ComentariosAdapter(Context context, List<Comentarios> data, DeleteComentarioClickListener listener) {
        PreferencesUtil.init(context);
        mData = data;
        mContext = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comentario, parent, false);

        return new ViewHolder(itemView, parent.getContext());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Comentarios comentarios = mData.get(position);
        holder.comentarioTexto.setText(comentarios.getmComentario());
        holder.comentarioUsuario.setText(comentarios.getmUsuario());
        if(comentarios.getmUsuario().equals(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_NAME, ""))) {
            holder.eliminarComentario.setVisibility(View.VISIBLE);
            holder.setDeleteClick(comentarios);
        } else {
            holder.eliminarComentario.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void add(Comentarios comentario) {
        mData.add(comentario);
        notifyDataSetChanged();
    }

    public void deleteComentario(Comentarios comentario) {
        mData.remove(comentario);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView comentarioUsuario;
        TextView comentarioTexto;
        @Bind(R.id.eliminar_comentario)
        ImageButton eliminarComentario;

        public ViewHolder(View itemView, Context context) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            comentarioTexto = (TextView) itemView.findViewById(R.id.comentario_texto);
            comentarioUsuario = (TextView) itemView.findViewById(R.id.comentario_usuario);
        }

        public void setDeleteClick(final Comentarios comentario) {
            eliminarComentario.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDelete(comentario);
                }
            });
        }
    }

    public void setComentarios(List<Comentarios> newItems) {
        mData.clear();
        mData.addAll(newItems);
        notifyDataSetChanged();
    }
}
