package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.SucursalHotelEvent;
import com.javieralvarez.kinal.app_turismo.interactors.SucursalesHotelInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.SucursalesHotelInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.SucursalesHotelView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 07/07/2016.
 */
public class SucursalesHotelPresenterImpl implements SucursalesHotelPresenter {

    private EventBus eventBus;
    private SucursalesHotelInteractor interactor;
    private SucursalesHotelView view;

    public SucursalesHotelPresenterImpl(SucursalesHotelView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new SucursalesHotelInteractorImpl(activity);
    }

    @Override
    public void getSucursales(int id) {
        interactor.execute(id);
    }

    @Override
    @Subscribe
    public void onEventMainThread(SucursalHotelEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onSucursalesReady(event.getHotels());
            } else {
                view.onError(event.getError());
            }
        }
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        if(view != null) {
            eventBus.unregister(this);
            view = null;
        }
    }
}
