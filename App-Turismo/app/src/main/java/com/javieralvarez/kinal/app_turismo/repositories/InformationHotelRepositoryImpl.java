package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.InformationHotelEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.Hotel_Table;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;
import com.raizlabs.android.dbflow.sql.language.Select;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationHotelRepositoryImpl implements InformationHotelRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public InformationHotelRepositoryImpl(Activity activity) {
        PreferencesUtil.init(activity);
        this.app = (TurismoApp)activity.getApplication();
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void getFavoriteHotel(int id) {
        Hotel hotel = new Select().from(Hotel.class).where(Hotel_Table.mIdHotel.is(id)).querySingle();
        post(hotel);
    }

    @Override
    public void getAPIHotel(int id) {
        app.getApi().hotelPorId(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<Hotel>() {
                    @Override
                    public void onResponse(Call<Hotel> call, Response<Hotel> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<Hotel> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }

    private void post(Hotel hotel) {
        InformationHotelEvent event = new InformationHotelEvent();
        event.setHotel(hotel);
        event.setError(null);
        eventBus.post(event);
    }

    private void postError(String error) {
        InformationHotelEvent event = new InformationHotelEvent();
        event.setError(error);
        eventBus.post(event);
    }
}
