package com.javieralvarez.kinal.app_turismo.interactors;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;

/**
 * Created by Javier on 27/06/2016.
 */
public interface ComentariosInteractor {

    void execute(ComentariosFragment.TIPO_COMENTARIO tipo, int id);
    void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo_comentario, int id, String comentario);
    void deleteComentario(Comentarios comentarios);
}
