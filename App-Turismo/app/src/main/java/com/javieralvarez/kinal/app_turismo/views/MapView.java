package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;

import java.util.List;

/**
 * Created by Javier on 01/07/2016.
 */
public interface MapView {

    void onRestaurantesReady(List<Restaurante> restaurantes);
    void onHotelsReady(List<Hotel> hoteles);
    void onLugaresReady(List<LugarTuristico> lugares);
    void onError(String msg);

    void getRestaurantesLocations();
    void getHotelsLocations();
    void getLugaresLocation();
}
