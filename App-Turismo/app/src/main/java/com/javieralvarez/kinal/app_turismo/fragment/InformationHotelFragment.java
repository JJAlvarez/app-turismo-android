package com.javieralvarez.kinal.app_turismo.fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.SendButton;
import com.facebook.share.widget.ShareButton;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesHotelActivity;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.presenters.InformationHotelPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.InformationHotelPresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.InformationHotelView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformationHotelFragment extends Fragment implements InformationHotelView {

    @Bind(R.id.fbShare)
    ShareButton fbShare;
    @Bind(R.id.fbSend)
    SendButton fbSend;
    @Bind(R.id.img_inf_hotel)
    ImageView imgInfHotel;
    @Bind(R.id.txt_inf_hotel_descripcion)
    TextView txtInfHotelDescripcion;
    @Bind(R.id.elements_information_hotel)
    RelativeLayout elementsInformationHotel;
    @Bind(R.id.progressInformationHotel)
    ProgressBar progressInformationHotel;
    @Bind(R.id.container)
    NestedScrollView container;

    private Hotel mHotel;
    private int idHotel;
    private TIPO_INFORMACION tipo_informacion;

    private InformationHotelPresenter presenter;

    public enum TIPO_INFORMACION {
        TIPO_FAVORITO,
        TIPO_API
    }

    public InformationHotelFragment(TIPO_INFORMACION tipo_informacion) {
        this.tipo_informacion = tipo_informacion;
    }

    public InformationHotelFragment() {
    }

    public static InformationHotelFragment newInstance(TIPO_INFORMACION tipo_informacion) {
        InformationHotelFragment fragment = new InformationHotelFragment(tipo_informacion);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_information_hotel, container, false);
        ButterKnife.bind(this, view);
        presenter = new InformationHotelPresenterImpl(this, getActivity());
        presenter.onCreate();
        if (getActivity().getIntent().hasExtra(HotelAdapter.HOTEL_ID_TAG)) {
            idHotel = getActivity().getIntent().getIntExtra(HotelAdapter.HOTEL_ID_TAG, 0);
        }
        getHotel(idHotel);
        return view;
    }

    private void getHotel(int idHotel) {
        if(tipo_informacion == TIPO_INFORMACION.TIPO_FAVORITO) {
            getFavoriteHotel(idHotel);
        } else {
            getAPIHotel(idHotel);
        }
    }

    private void setupHotel(Hotel hotel) {
        this.mHotel = hotel;
        setTitleHotel();
        txtInfHotelDescripcion.setText(hotel.getmDescripcion());
        Glide.with(this).load(hotel.getmUrlImagen()).into(imgInfHotel);
        setContentFacebook();
        hideProgress();
        showElements();
    }

    private void setContentFacebook() {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(mHotel.getmUrlLogo()))
                .build();
        fbShare.setShareContent(content);
        fbSend.setShareContent(content);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void getAPIHotel(int id) {
        presenter.getAPIHotel(id);
        hideElements();
        showProgress();
    }

    @Override
    public void getFavoriteHotel(int id) {
        presenter.getFavoriteHotel(id);
        hideElements();
        showProgress();
    }

    private void setTitleHotel() {
        ((DetallesHotelActivity)getActivity()).onHotelReady(mHotel);
    }

    @Override
    public void onHotelReady(Hotel hotel) {
        setupHotel(hotel);
    }

    @Override
    public void onError(String error) {
        Snackbar.make(container, "Error al obtener el hotel, intentelo mas tarde", Snackbar.LENGTH_LONG).show();
        hideProgress();
        showElements();
    }

    @Override
    public void hideElements() {
        elementsInformationHotel.setVisibility(View.GONE);
    }

    @Override
    public void showElements() {
        elementsInformationHotel.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressInformationHotel.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressInformationHotel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
