package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.presenters.UpdatePresenter;
import com.javieralvarez.kinal.app_turismo.presenters.UpdatePresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;
import com.javieralvarez.kinal.app_turismo.views.UpdateView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantFragment extends Fragment implements UpdateView, TelefonoClickListener {

    @Bind(R.id.swipeContainerRestaurante)
    SwipeRefreshLayout swipeContainerRestaurante;
    @Bind(R.id.container_restaurante)
    RelativeLayout containerRestaurante;
    @Bind(R.id.restaurant_rec_view)
    RecyclerView restaurantRecView;
    @Bind(R.id.progressBarRestaurantes)
    ProgressBar progressBarRestaurantes;

    private RestaurantAdapter restaurantAdapter;

    private UpdatePresenter presenter;

    public RestaurantFragment() {
        // Required empty public constructor
    }

    public static RestaurantFragment newInstance() {
        RestaurantFragment fragment = new RestaurantFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);
        ButterKnife.bind(this, view);
        setSwipeToRefresh();
        setupRecycler();
        setupAdapter();
        presenter = new UpdatePresenterImpl(this, getActivity());
        presenter.onCreate();
        showProgress();
        getUpdates();
        return view;
    }

    private void hideProgress() {
        restaurantRecView.setVisibility(View.VISIBLE);
        progressBarRestaurantes.setVisibility(View.GONE);
    }

    private void showProgress() {
        restaurantRecView.setVisibility(View.GONE);
        progressBarRestaurantes.setVisibility(View.VISIBLE);
    }

    private void setupAdapter() {
        restaurantAdapter = new RestaurantAdapter(getActivity(), getActivity(), new ArrayList<Restaurante>(), this);
        restaurantRecView.setAdapter(restaurantAdapter);
        restaurantRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void setupRecycler() {
        restaurantRecView.setHasFixedSize(true);
    }

    private void setSwipeToRefresh() {
        swipeContainerRestaurante.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUpdates();
            }
        });

        swipeContainerRestaurante.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void getUpdates() {
        presenter.getUpdates(UpdateEvent.TIPO_UPDATE.UPDATE_RESTAURANTE);
    }

    @Override
    public void onUpdate(List<Object> updates) {
        ArrayList<Restaurante> restaurantesUpdates = (ArrayList<Restaurante>) (Object) updates;
        restaurantAdapter.updateAll(restaurantesUpdates);
        if (swipeContainerRestaurante.isRefreshing()) {
            swipeContainerRestaurante.setRefreshing(false);
        }
        if (progressBarRestaurantes.getVisibility() == View.VISIBLE) {
            hideProgress();
        }
    }

    @Override
    public void onError() {
        Snackbar.make(containerRestaurante, getString(R.string.restaurante_error_message), Snackbar.LENGTH_LONG).show();
        if (swipeContainerRestaurante.isRefreshing()) {
            swipeContainerRestaurante.setRefreshing(false);
        }
    }

    @Override
    public void onTelefonoClick(int telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + Uri.encode(String.valueOf(telefono))));
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
