package com.javieralvarez.kinal.app_turismo.repositories;

/**
 * Created by Javier on 01/07/2016.
 */
public interface MapRepository {

    void getRestaurantesLocations();
    void getHotelsLocations();
    void getLuaresLocation();

}
