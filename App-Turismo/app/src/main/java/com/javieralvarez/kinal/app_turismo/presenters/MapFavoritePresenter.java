package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.MapEvent;

/**
 * Created by Javier on 13/07/2016.
 */
public interface MapFavoritePresenter {

    void onCreate();
    void onDestroy();

    void getRestaurantes();
    void getHoteles();
    void getLugares();

    void onEventMainThread(MapEvent event);
}
