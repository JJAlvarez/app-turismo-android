package com.javieralvarez.kinal.app_turismo.utils;

import com.javieralvarez.kinal.app_turismo.model.Restaurante;

/**
 * Created by Javier on 04/07/2016.
 */
public interface ClickRestauranteListener {

    void onClick(Restaurante restaurante);
}
