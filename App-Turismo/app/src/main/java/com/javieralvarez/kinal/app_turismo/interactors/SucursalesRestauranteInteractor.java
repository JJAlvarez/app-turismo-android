package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 06/07/2016.
 */
public interface SucursalesRestauranteInteractor {

    void execute(int id);
}
