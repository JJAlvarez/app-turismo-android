package com.javieralvarez.kinal.app_turismo.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.ClickRestauranteListener;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Javier on 04/07/2016.
 */
public class FavoritesRestaurantAdapter extends RecyclerView.Adapter<FavoritesRestaurantAdapter.ViewHolder> {

    private ArrayList<Restaurante> restaurantes;
    private Context context;
    private ClickRestauranteListener listener;
    private TelefonoClickListener telListener;
    private int lastPosition = -1;

    public FavoritesRestaurantAdapter(ArrayList<Restaurante> restaurantes, Context context, ClickRestauranteListener listener,
                                      TelefonoClickListener telListener) {
        this.restaurantes = restaurantes;
        this.context = context;
        this.listener = listener;
        this.telListener = telListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_restaurante, parent, false);

        ViewHolder hvh = new ViewHolder(itemView);

        return hvh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Restaurante restaurante = restaurantes.get(position);
        setAnimation(holder.cardViewRestaurante, position);
        holder.lblName.setText(restaurante.getmNombre());
        holder.lblPhone.setText(String.valueOf(restaurante.getmPbx()));
        Glide.with(context).load(restaurante.getmUrlLogo()).into(holder.imgRestaurant);
        holder.setClickListener(restaurante, listener);
    }

    @Override
    public int getItemCount() {
        return restaurantes.size();
    }

    public void favorites(List<Restaurante> restaurantes) {
        this.restaurantes.clear();
        this.restaurantes.addAll(restaurantes);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.btn_call_restaurant)
        CircleImageView btnCallRestaurant;
        @Bind(R.id.img_restaurant)
        ImageView imgRestaurant;
        @Bind(R.id.lbl_name)
        TextView lblName;
        @Bind(R.id.lbl_phone)
        TextView lblPhone;
        @Bind(R.id.card_view_restaurante)
        CardView cardViewRestaurante;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setClickListener(final Restaurante restaurante, final ClickRestauranteListener listener) {
            cardViewRestaurante.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(restaurante);
                }
            });
            this.btnCallRestaurant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    telListener.onTelefonoClick(restaurante.getmPbx());
                }
            });
        }

        public void clearAnimation() {
            cardViewRestaurante.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            animation.setDuration(500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        ((ViewHolder) holder).clearAnimation();
    }
}
