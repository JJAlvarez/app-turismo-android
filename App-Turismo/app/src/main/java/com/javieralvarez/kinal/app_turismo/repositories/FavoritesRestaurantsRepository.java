package com.javieralvarez.kinal.app_turismo.repositories;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesRestaurantsRepository {

    void getRestaurants();
}
