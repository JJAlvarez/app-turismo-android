package com.javieralvarez.kinal.app_turismo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.fragment.InformationRestauranteFragment;
import com.javieralvarez.kinal.app_turismo.fragment.SucursalesFragment;

/**
 * Created by Javier on 05/06/2016.
 */
public class FragmentRestauranteAdapter extends FragmentPagerAdapter {

    private int PAGE_COUNT = 3;

    private String tabTitles[] =
            new String[] { "Informacion", "Sucursales", "Comentarios"};

    public FragmentRestauranteAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = InformationRestauranteFragment.newInstance();
                break;
            case 1:
                fragment = SucursalesFragment.newInstance();
                break;
            case 2:
                fragment = ComentariosFragment.newInstance(ComentariosFragment.TIPO_COMENTARIO.RESTAURANTE);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
