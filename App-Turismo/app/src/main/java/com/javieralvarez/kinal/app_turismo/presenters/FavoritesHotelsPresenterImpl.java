package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.FavoritesHotelsEvent;
import com.javieralvarez.kinal.app_turismo.interactors.FavoritesHotelsInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.FavoritesHotelsInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.FavoritesHotelsView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesHotelsPresenterImpl implements FavoritesHotelsPresenter {

    private EventBus eventBus;
    private FavoritesHotelsView view;
    private FavoritesHotelsInteractor interactor;

    public FavoritesHotelsPresenterImpl(FavoritesHotelsView view) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new FavoritesHotelsInteractorImpl();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void getHoteles() {
        view.hideElements();
        view.showProgress();
        interactor.execute();
    }

    @Override
    @Subscribe
    public void onEventMainThread(FavoritesHotelsEvent event) {
        if(view != null) {
            if(event.getError() == null) {
              view.onFavoritesReady(event.getHotels());
            }
        }
    }
}
