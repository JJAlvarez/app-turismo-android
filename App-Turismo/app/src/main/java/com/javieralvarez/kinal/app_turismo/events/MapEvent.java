package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;

import java.util.List;

/**
 * Created by Javier on 01/07/2016.
 */
public class MapEvent {

    public static enum TIPO_MAPA {
        MAPA_RESTAURANTE,
        MAPA_HOTEL,
        MAPA_LUGAR
    }

    private TIPO_MAPA tipo_mapa;
    private String error;
    private List<Hotel> hotels;
    private List<LugarTuristico> lugarTuristicos;
    private List<Restaurante> restaurantes;

    public TIPO_MAPA getTipo_mapa() {
        return tipo_mapa;
    }

    public void setTipo_mapa(TIPO_MAPA tipo_mapa) {
        this.tipo_mapa = tipo_mapa;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }

    public List<LugarTuristico> getLugarTuristicos() {
        return lugarTuristicos;
    }

    public void setLugarTuristicos(List<LugarTuristico> lugarTuristicos) {
        this.lugarTuristicos = lugarTuristicos;
    }

    public List<Restaurante> getRestaurantes() {
        return restaurantes;
    }

    public void setRestaurantes(List<Restaurante> restaurantes) {
        this.restaurantes = restaurantes;
    }
}
