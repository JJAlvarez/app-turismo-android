package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Restaurante;

/**
 * Created by Javier on 20/07/2016.
 */
public interface MapRestauranteView {

    void getSucursales(int idRestaurante);
    void onError(String error);
    void onSucursalesReady(Restaurante restaurante);

}
