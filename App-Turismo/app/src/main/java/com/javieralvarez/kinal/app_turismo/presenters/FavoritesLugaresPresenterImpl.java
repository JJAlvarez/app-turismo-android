package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.FavoritesLugaresEvent;
import com.javieralvarez.kinal.app_turismo.interactors.FavoritesLugaresInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.FavoritesLugaresInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.FavoritesLugaresView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesLugaresPresenterImpl implements FavoritesLugaresPresenter {

    private EventBus eventBus;
    private FavoritesLugaresView view;
    private FavoritesLugaresInteractor interactor;

    public FavoritesLugaresPresenterImpl(FavoritesLugaresView view) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new FavoritesLugaresInteractorImpl();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void getPlaces() {
        view.hideElements();
        view.showProgress();
        interactor.execute();
    }

    @Override
    @Subscribe
    public void onEventMainThread(FavoritesLugaresEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onFavoritesReady(event.getLugares());
            }
        }
    }
}
