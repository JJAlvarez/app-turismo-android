package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.SucursalHotelEvent;

/**
 * Created by Javier on 07/07/2016.
 */
public interface SucursalesHotelPresenter {

    void getSucursales(int id);
    void onEventMainThread(SucursalHotelEvent event);

    void onCreate();
    void onDestroy();
}
