package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.presenters.UpdatePresenter;
import com.javieralvarez.kinal.app_turismo.presenters.UpdatePresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;
import com.javieralvarez.kinal.app_turismo.views.UpdateView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelFragment extends Fragment implements UpdateView, TelefonoClickListener {

    @Bind(R.id.swipeContainerHotel)
    SwipeRefreshLayout swipeContainerHotel;
    @Bind(R.id.container_hotel)
    RelativeLayout containerHotel;
    @Bind(R.id.hotel_rec_view)
    RecyclerView hotelRecView;
    @Bind(R.id.progressBarHoteles)
    ProgressBar progressBarHoteles;
    private HotelAdapter hotelAdapter;

    private UpdatePresenter presenter;

    public HotelFragment() {
        // Required empty public constructor
    }

    public static HotelFragment newInstance() {
        HotelFragment fragment = new HotelFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_hotel, container, false);
        ButterKnife.bind(this, view);
        presenter = new UpdatePresenterImpl(this, getActivity());
        presenter.onCreate();
        setSwipeToRefresh();
        setupRecycler();
        setupAdapter();
        getUpdates();

        return view;
    }

    private void setupAdapter() {
        hotelAdapter = new HotelAdapter(getActivity(), new ArrayList<Hotel>(), this);
        hotelRecView.setAdapter(hotelAdapter);
        hotelRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void setupRecycler() {
        hotelRecView.setHasFixedSize(true);
    }

    private void setSwipeToRefresh() {
        swipeContainerHotel.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUpdates();
            }
        });

        swipeContainerHotel.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void getUpdates() {
        if(!swipeContainerHotel.isRefreshing()) {
            showProgress();
            hideElements();
        }
        presenter.getUpdates(UpdateEvent.TIPO_UPDATE.UPDATE_HOTEL);
    }

    @Override
    public void onUpdate(List<Object> updates) {
        ArrayList<Hotel> hotelUpdates = (ArrayList<Hotel>) (Object) updates;
        hotelAdapter.updateAll(hotelUpdates);
        showElements();
        hideProgress();
        if(swipeContainerHotel.isRefreshing()) {
            swipeContainerHotel.setRefreshing(false);
        }
    }

    @Override
    public void onError() {
        Snackbar.make(containerHotel, getString(R.string.hotel_error_message), Snackbar.LENGTH_LONG).show();
        if (swipeContainerHotel.isRefreshing()) {
            swipeContainerHotel.setRefreshing(false);
        }
    }

    private void hideElements(){
        hotelRecView.setVisibility(View.GONE);
    }

    private void hideProgress(){
        progressBarHoteles.setVisibility(View.GONE);
    }

    private void showElements() {
        hotelRecView.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        progressBarHoteles.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onTelefonoClick(int telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + Uri.encode(String.valueOf(telefono))));
        startActivity(intent);
    }
}
