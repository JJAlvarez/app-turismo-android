package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;

/**
 * Created by Javier on 30/06/2016.
 */
public interface UpdatePresenter {

    void onCreate();
    void onDestroy();

    void getUpdates(UpdateEvent.TIPO_UPDATE tipo_update);
    void onEventMainThread(UpdateEvent event);
}
