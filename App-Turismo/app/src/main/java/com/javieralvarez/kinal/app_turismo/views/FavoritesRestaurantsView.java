package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Restaurante;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesRestaurantsView {

    void getFavoritesRestaurants();
    void onFavoritesReady(List<Restaurante> restauranteList);

    void showElements();
    void hideElements();
    void showProgress();
    void hideProgress();
}
