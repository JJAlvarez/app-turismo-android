package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.repositories.ComentarioRepository;
import com.javieralvarez.kinal.app_turismo.repositories.ComentarioRepositoryImpl;

/**
 * Created by Javier on 28/06/2016.
 */
public class AddComentarioInteractorImpl implements AddComentarioInteractor {

    private ComentarioRepository repository;

    public AddComentarioInteractorImpl(Activity activity) {
        repository = new ComentarioRepositoryImpl(activity);
    }
    @Override
    public void execute(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario) {
        repository.addComentario(tipo, id, comentario);
    }
}
