package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Comentarios;

import java.util.List;

/**
 * Created by Javier on 01/07/2016.
 */
public class UpdateComentarioEvent {

    public static enum TIPO_COMENTARIO {
        COMENTARIO_RESTAURANTE,
        COMENTARIO_HOTEL,
        COMENTARIO_LUGAR
    }

    private String error;
    private List<Comentarios> updates;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Comentarios> getUpdates() {
        return updates;
    }

    public void setUpdates(List<Comentarios> updates) {
        this.updates = updates;
    }
}
