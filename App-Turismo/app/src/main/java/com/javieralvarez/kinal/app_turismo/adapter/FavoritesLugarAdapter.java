package com.javieralvarez.kinal.app_turismo.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.utils.ClickLugarListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Javier on 04/07/2016.
 */
public class FavoritesLugarAdapter extends RecyclerView.Adapter<FavoritesLugarAdapter.ViewHolder> {

    private ArrayList<LugarTuristico> lugares;
    private Context context;
    private ClickLugarListener listener;
    private int lastPosition = -1;

    public FavoritesLugarAdapter(ArrayList<LugarTuristico> lugares, Context context, ClickLugarListener listener) {
        this.lugares = lugares;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lugar, parent, false);

        ViewHolder hvh = new ViewHolder(itemView);

        return hvh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LugarTuristico lugarTuristico = lugares.get(position);
        setAnimation(holder.cardViewLugar, position);
        holder.lblNameLugar.setText(lugarTuristico.getmNombre());
        holder.lblDireccionLugar.setText(lugarTuristico.getmDireccion());
        Glide.with(context).load(lugarTuristico.getmUrlLogo()).into(holder.imgLugar);
        holder.setClickListener(lugarTuristico, listener);
    }

    @Override
    public int getItemCount() {
        return lugares.size();
    }

    public void favorites(List<LugarTuristico> favorites) {
        this.lugares.clear();
        this.lugares.addAll(favorites);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.img_lugar)
        ImageView imgLugar;
        @Bind(R.id.lbl_name_lugar)
        TextView lblNameLugar;
        @Bind(R.id.lbl_direccion_lugar)
        TextView lblDireccionLugar;
        @Bind(R.id.card_view_lugar)
        CardView cardViewLugar;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setClickListener(final LugarTuristico lugar, final ClickLugarListener listener) {
            cardViewLugar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(lugar);
                }
            });
        }

        public void clearAnimation() {
            cardViewLugar.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            animation.setDuration(500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        ((ViewHolder) holder).clearAnimation();
    }
}
