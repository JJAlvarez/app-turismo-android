package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.MapRestauranteEvent;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 20/07/2016.
 */
public class MapRestauranteRepositoryImpl implements MapRestauranteRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public MapRestauranteRepositoryImpl(Activity activity) {
        PreferencesUtil.init(activity);
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
    }

    @Override
    public void execute(int idRestaurante) {
        app.getApi().restaurantePorId(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), idRestaurante)
                .enqueue(new Callback<Restaurante>() {
                    @Override
                    public void onResponse(Call<Restaurante> call, Response<Restaurante> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<Restaurante> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    private void postError(String msg) {
        MapRestauranteEvent event = new MapRestauranteEvent();
        event.setError(msg);
        eventBus.post(event);
    }

    private void post(Restaurante restaurante) {
        MapRestauranteEvent event = new MapRestauranteEvent();
        event.setRestaurante(restaurante);
        event.setError(null);
        eventBus.post(event);
    }
}
