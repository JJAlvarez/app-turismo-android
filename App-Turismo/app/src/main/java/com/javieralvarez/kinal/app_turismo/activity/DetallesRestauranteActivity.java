package com.javieralvarez.kinal.app_turismo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.FragmentRestauranteAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.model.Restaurante_Table;
import com.raizlabs.android.dbflow.sql.language.Select;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetallesRestauranteActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @Bind(R.id.mapa_restaurante)
    FloatingActionButton mapaRestaurante;
    @Bind(R.id.appbartabs_restaurante)
    TabLayout tabLayout;
    @Bind(R.id.viewpager_restaurante)
    ViewPager viewPager;
    private Toolbar mToolbar;
    private Restaurante mRestaurante;
    private static final int PAGE_COMENTARIOS = 2;
    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_restaurante);
        ButterKnife.bind(this);

        setupToolbar();

        getSupportActionBar().setTitle("");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setViewPager();
        setTabs();
    }

    public void onRestaurantReady(Restaurante restaurante) {
        mRestaurante = restaurante;
        getSupportActionBar().setTitle(mRestaurante.getmNombre());
        setFavorite();
        invalidateOptionsMenu();
        onCreateOptionsMenu(mMenu);
    }

    private void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_detalles_restaurante);
        setSupportActionBar(mToolbar);
    }

    private void setInformation() {
        if (getIntent().hasExtra(RestaurantAdapter.RESTAURANTE_DETALLES_TAG)) {
            mRestaurante = getIntent().getParcelableExtra(RestaurantAdapter.RESTAURANTE_DETALLES_TAG);
        }

        getSupportActionBar().setTitle(mRestaurante.getmNombre());

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setTabs() {
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setViewPager() {
        viewPager.setAdapter(new FragmentRestauranteAdapter(
                getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == PAGE_COMENTARIOS) {
            mapaRestaurante.hide();
        } else {
            mapaRestaurante.show();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        if (mRestaurante != null) {
            getMenuInflater().inflate(R.menu.description_menu, menu);
            MenuItem menuItem = menu.findItem(R.id.action_do_favorite);
            menuItem.setIcon(mRestaurante.getFavorite() ? R.drawable.ic_favorite : R.drawable.ic_unfavorite);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_do_favorite) {
            if (mRestaurante != null) {
                if (mRestaurante.getFavorite()) {
                    item.setIcon(R.drawable.ic_unfavorite);
                    mRestaurante.setFavorite(false);
                    deleteFavorite();
                } else {
                    item.setIcon(R.drawable.ic_favorite);
                    mRestaurante.setFavorite(true);
                    saveFavorite();
                }
            }
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteFavorite() {
        mRestaurante.delete();
    }

    private void saveFavorite() {
        mRestaurante.save();
    }

    private boolean getDatabaseReferenceObject() {
        Restaurante restaurante = new Select().from(Restaurante.class).where(Restaurante_Table.mIdRestaurante
                .is(mRestaurante.getmIdRestaurante())).querySingle();
        return restaurante == null;
    }

    private void setFavorite() {
        if (getDatabaseReferenceObject()) {
            mRestaurante.setFavorite(false);
        } else {
            mRestaurante.setFavorite(true);
        }
    }

    @OnClick(R.id.mapa_restaurante)
    public void onClick() {
        Intent map = new Intent(DetallesRestauranteActivity.this, MapRestauranteActivity.class);
        map.putExtra(RestaurantAdapter.RESTAURANTE_ID_TAG, mRestaurante.getmIdRestaurante());
        startActivity(map);
    }
}
