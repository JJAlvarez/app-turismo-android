package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.DepartmentRestaurantEvent;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentRestaurantPresenter {

    void onCreate();
    void onDestroy();

    void getRestaurantes(int id);
    void onEventMainThread(DepartmentRestaurantEvent event);
}
