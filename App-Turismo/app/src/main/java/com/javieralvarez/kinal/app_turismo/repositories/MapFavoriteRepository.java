package com.javieralvarez.kinal.app_turismo.repositories;

/**
 * Created by Javier on 13/07/2016.
 */
public interface MapFavoriteRepository {

    void getRestaurantes();
    void getHoteles();
    void getLugares();
}
