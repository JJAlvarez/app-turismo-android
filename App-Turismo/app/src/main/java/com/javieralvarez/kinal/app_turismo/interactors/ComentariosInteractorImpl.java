package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;
import com.javieralvarez.kinal.app_turismo.repositories.ComentarioRepository;
import com.javieralvarez.kinal.app_turismo.repositories.ComentarioRepositoryImpl;

/**
 * Created by Javier on 27/06/2016.
 */
public class ComentariosInteractorImpl implements ComentariosInteractor {

    private ComentarioRepository repository;

    public ComentariosInteractorImpl(Activity activity) {
        this.repository = new ComentarioRepositoryImpl(activity);
    }

    @Override
    public void execute(ComentariosFragment.TIPO_COMENTARIO tipo, int id) {
        repository.getComentarios(tipo, id);
    }

    @Override
    public void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo_comentario, int id, String comentario) {
        repository.addComentario(tipo_comentario, id, comentario);
    }

    @Override
    public void deleteComentario(Comentarios comentarios) {
        repository.deleteComentario(comentarios);
    }
}
