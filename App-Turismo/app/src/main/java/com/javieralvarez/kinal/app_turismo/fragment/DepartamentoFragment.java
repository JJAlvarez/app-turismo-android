package com.javieralvarez.kinal.app_turismo.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.adapter.DepartamentoAdapter;
import com.javieralvarez.kinal.app_turismo.model.Departamento;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DepartamentoFragment extends Fragment {

    SwipeRefreshLayout swipeContainer;
    @Bind(R.id.departamento_rec_view)
    RecyclerView mDepartamentosListView;

    private DepartamentoAdapter departamentoAdapter;

    public DepartamentoFragment() {
        // Required empty public constructor
    }

    public static DepartamentoFragment newInstance() {
        DepartamentoFragment fragment = new DepartamentoFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        PreferencesUtil.init(getActivity());
        View view = inflater.inflate(R.layout.fragment_departamento, container, false);
        ButterKnife.bind(this, view);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainerDepartamentos);
        setupRecycler();
        setupAdapter();
        setSwipeToRefresh();
        updateData();
        return view;
    }

    private void setupRecycler() {
        mDepartamentosListView.setHasFixedSize(true);
    }

    private void setupAdapter() {
        departamentoAdapter = new DepartamentoAdapter(getActivity(), new ArrayList<Departamento>());
        mDepartamentosListView.setAdapter(departamentoAdapter);
        mDepartamentosListView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void setSwipeToRefresh() {
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateData();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    public void updateData() {
        ((TurismoApp) getActivity().getApplication()).getApi().getAllDepartamentos(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""))
                .enqueue(new Callback<List<Departamento>>() {
                    @Override
                    public void onResponse(Call<List<Departamento>> call, Response<List<Departamento>> response) {
                        departamentoAdapter.clear();
                        departamentoAdapter.addAll(response.body());
                        if (swipeContainer.isRefreshing()) {
                            swipeContainer.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Departamento>> call, Throwable t) {
                        if (swipeContainer.isRefreshing()) {
                            swipeContainer.setRefreshing(false);
                        }
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
