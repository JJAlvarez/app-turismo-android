package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.SucursalesRestauranteAdapter;
import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;
import com.javieralvarez.kinal.app_turismo.presenters.SucursalesRestaurantePresenter;
import com.javieralvarez.kinal.app_turismo.presenters.SucursalesRestaurantePresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.SucursalClickListener;
import com.javieralvarez.kinal.app_turismo.views.SucursalesRestauranteView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SucursalesFragment extends Fragment implements SucursalesRestauranteView, SucursalClickListener {


    @Bind(R.id.sucursal_rec_view)
    RecyclerView sucursalRecView;
    @Bind(R.id.swipeContainerSucursales)
    SwipeRefreshLayout swipeContainerSucursales;
    @Bind(R.id.progressBarSucursal)
    ProgressBar progressBarSucursal;
    @Bind(R.id.container_sucursales)
    RelativeLayout containerSucursales;


    private SucursalesRestaurantePresenter presenter;
    private SucursalesRestauranteAdapter adapter;
    private int restauranteId;

    public SucursalesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sucursales, container, false);
        ButterKnife.bind(this, view);
        presenter = new SucursalesRestaurantePresenterImpl(this, getActivity());
        presenter.onCreate();
        setupId();
        setupRecycler();
        setSwipeToRefresh();
        setupAdapter();
        getSucursales(restauranteId);
        return view;
    }

    private void setupAdapter() {
        adapter = new SucursalesRestauranteAdapter(new ArrayList<SucursalRestaurante>(), getContext(), this);
        sucursalRecView.setAdapter(adapter);
        sucursalRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void setupRecycler() {
        sucursalRecView.setHasFixedSize(true);
    }

    private void setupId() {
        if (getActivity().getIntent().hasExtra(RestaurantAdapter.RESTAURANTE_ID_TAG)) {
            restauranteId = getActivity().getIntent().getIntExtra(RestaurantAdapter.RESTAURANTE_ID_TAG, 0);
        }
    }

    private void setSwipeToRefresh() {
        swipeContainerSucursales.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSucursales(restauranteId);
            }
        });

        swipeContainerSucursales.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void getSucursales(int id) {
        presenter.getSucursales(restauranteId);
    }

    @Override
    public void onSucursalesReady(List<SucursalRestaurante> sucursales) {
        adapter.setSucursales(sucursales);
        showElements();
        hideProgress();
        if(swipeContainerSucursales.isRefreshing()) {
            swipeContainerSucursales.setRefreshing(false);
        }
    }

    @Override
    public void showElements() {
        sucursalRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressBarSucursal.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        sucursalRecView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBarSucursal.setVisibility(View.GONE);
    }

    @Override
    public void onError() {
        Snackbar.make(containerSucursales, getString(R.string.sucursales_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onClick(SucursalRestaurante sucursal) {
        try
        {
            String url = "waze://?ll=" + sucursal.getmLatitud() + "," + sucursal.getmLongitud() + "&z=10&navigate=yes";
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
            startActivity( intent );
        }
        catch ( ActivityNotFoundException ex  )
        {
            Intent intent =
                    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
            startActivity(intent);
        }
    }

    public static Fragment newInstance() {
        return new SucursalesFragment();
    }
}
