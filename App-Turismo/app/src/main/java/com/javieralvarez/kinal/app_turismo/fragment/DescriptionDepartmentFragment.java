package com.javieralvarez.kinal.app_turismo.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.DepartamentoAdapter;
import com.javieralvarez.kinal.app_turismo.model.Departamento;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DescriptionDepartmentFragment extends Fragment {


    @Bind(R.id.img_inf_hotel)
    ImageView imgInfHotel;
    @Bind(R.id.txt_inf_hotel_descripcion)
    TextView txtInfHotelDescripcion;

    private Departamento mDepartamento;

    public DescriptionDepartmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_description_department, container, false);
        ButterKnife.bind(this, view);
        if(getActivity().getIntent().hasExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG)) {
            mDepartamento = getActivity().getIntent().getParcelableExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG);
        }
        setDepartamento();
        return view;
    }

    private void setDepartamento() {
        txtInfHotelDescripcion.setText(mDepartamento.getmDescripcion());
        Glide.with(this).load(mDepartamento.getmUrlImagen()).into(imgInfHotel);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static DescriptionDepartmentFragment newInstance() {
        return new DescriptionDepartmentFragment();
    }
}
