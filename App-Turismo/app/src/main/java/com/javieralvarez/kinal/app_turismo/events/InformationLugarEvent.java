package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationLugarEvent {

    private String error;
    private LugarTuristico lugares;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public LugarTuristico getLugares() {
        return lugares;
    }

    public void setLugares(LugarTuristico lugares) {
        this.lugares = lugares;
    }
}
