package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.MapRestauranteEvent;

/**
 * Created by Javier on 20/07/2016.
 */
public interface MapRestaurantePresenter {

    void onCreate();
    void onDestroy();
    void onEventMainThread(MapRestauranteEvent event);
    void getSucursales(int idRestaurante);
}
