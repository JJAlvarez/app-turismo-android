package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.SucursalesHotelRepository;
import com.javieralvarez.kinal.app_turismo.repositories.SucursalesHotelRepositoryImpl;

/**
 * Created by Javier on 07/07/2016.
 */
public class SucursalesHotelInteractorImpl implements SucursalesHotelInteractor {

    private SucursalesHotelRepository repository;

    public SucursalesHotelInteractorImpl(Activity activity) {
        this.repository = new SucursalesHotelRepositoryImpl(activity);
    }

    @Override
    public void execute(int id) {
        repository.getSucursales(id);
    }
}
