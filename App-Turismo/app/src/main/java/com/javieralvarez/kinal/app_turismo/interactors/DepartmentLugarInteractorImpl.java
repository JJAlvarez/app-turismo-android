package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.DepartmentLugarRepository;
import com.javieralvarez.kinal.app_turismo.repositories.DepartmentLugarRepositoryImpl;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentLugarInteractorImpl implements DepartmentLugarInteractor {

    private DepartmentLugarRepository repository;

    public DepartmentLugarInteractorImpl(Activity activity) {
        this.repository = new DepartmentLugarRepositoryImpl(activity);
    }

    @Override
    public void execute(int id) {
        repository.getLugares(id);
    }
}
