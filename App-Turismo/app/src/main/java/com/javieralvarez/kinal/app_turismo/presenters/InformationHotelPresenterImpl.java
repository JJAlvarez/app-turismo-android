package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.InformationHotelEvent;
import com.javieralvarez.kinal.app_turismo.interactors.InformationHotelInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.InformationHotelInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.InformationHotelView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationHotelPresenterImpl implements InformationHotelPresenter {

    private EventBus eventBus;
    private InformationHotelView view;
    private InformationHotelInteractor interactor;

    public InformationHotelPresenterImpl(InformationHotelView view, Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.view = view;
        interactor = new InformationHotelInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void getFavoriteHotel(int id) {
        interactor.getFavoriteHotel(id);
    }

    @Override
    public void getAPIHotel(int id) {
        interactor.getAPIHotel(id);
    }

    @Override
    @Subscribe
    public void onEventMainThread(InformationHotelEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onHotelReady(event.getHotel());
            } else {
                view.onError(event.getError());
            }
        }
    }
}
