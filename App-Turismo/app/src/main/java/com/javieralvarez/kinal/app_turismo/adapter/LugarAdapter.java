package com.javieralvarez.kinal.app_turismo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesLugarActivity;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javier on 18/05/2016.
 */
public class LugarAdapter extends RecyclerView.Adapter<LugarAdapter.LugarViewHolder> {

    public static final String LUGAR_ID_TAG = "lugarID";
    private Context mContext;
    private Activity mActivity;
    private ArrayList<LugarTuristico> mData;

    public static String LUGAR_DETALLES_TAG = "lugar";
    private int lastPosition = -1;

    public LugarAdapter(Activity activity, Context context, ArrayList<LugarTuristico> data) {
        this.mActivity = activity;
        this.mContext = context;
        this.mData = data;
    }

    @Override
    public LugarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lugar, parent, false);

        LugarViewHolder lvh = new LugarViewHolder(view);

        return lvh;
    }

    @Override
    public void onBindViewHolder(LugarViewHolder holder, int position) {
        LugarTuristico lugar = mData.get(position);
        setAnimation(holder.mView, position);
        holder.bindLugar(lugar);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void updateAll(ArrayList<LugarTuristico> lugarUpdates) {
        mData.clear();
        mData.addAll(lugarUpdates);
        notifyDataSetChanged();
    }

    public void addLugares(List<LugarTuristico> lugares) {
        mData.clear();
        mData.addAll(lugares);
        notifyDataSetChanged();
    }

    public class LugarViewHolder extends RecyclerView.ViewHolder {

        private ImageView mLogo;
        private TextView mLblNombre;
        private TextView mLblDireccion;
        private CardView mView;

        public LugarViewHolder(View itemView) {
            super(itemView);
            this.mLogo = (ImageView) itemView.findViewById(R.id.img_lugar);
            this.mLblNombre = (TextView) itemView.findViewById(R.id.lbl_name_lugar);
            this.mLblDireccion = (TextView) itemView.findViewById(R.id.lbl_direccion_lugar);
            this.mView = (CardView) itemView.findViewById(R.id.card_view_lugar);
        }

        public void bindLugar(final LugarTuristico lugar) {
            mLblNombre.setText(lugar.getmNombre());
            mLblDireccion.setText(lugar.getmDireccion());
            Glide.with(mContext).load(lugar.getmUrlLogo()).into(mLogo);
            this.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, DetallesLugarActivity.class);
                    intent.putExtra(LUGAR_ID_TAG, lugar.getmIdLugarturistico());
                    mContext.startActivity(intent);
                }
            });
        }

        public void clearAnimation() {
            mView.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            animation.setDuration(500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(LugarViewHolder holder) {
        ((LugarViewHolder) holder).clearAnimation();
    }
}
