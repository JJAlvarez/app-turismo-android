package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.InformationRestauranteEvent;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 15/07/2016.
 */
public class InformationRestauranteRepositoryImpl implements InformationRestauranteRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public InformationRestauranteRepositoryImpl(Activity activity) {
        PreferencesUtil.init(activity);
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
    }

    @Override
    public void getResturante(int id) {
        app.getApi().restaurantePorId(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<Restaurante>() {
                    @Override
                    public void onResponse(Call<Restaurante> call, Response<Restaurante> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<Restaurante> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }

    private void post(Restaurante restuarante) {
        InformationRestauranteEvent event = new InformationRestauranteEvent();
        event.setError(null);
        event.setRestaurante(restuarante);
        eventBus.post(event);
    }

    private void postError(String error) {
        InformationRestauranteEvent event = new InformationRestauranteEvent();
        event.setError(error);
        eventBus.post(event);
    }
}
