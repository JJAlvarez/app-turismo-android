package com.javieralvarez.kinal.app_turismo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.FragmentHotelAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.Hotel_Table;
import com.raizlabs.android.dbflow.sql.language.Select;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetallesHotelActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @Bind(R.id.mapa_hotel)
    FloatingActionButton mapaHotel;
    @Bind(R.id.viewpager_hotel)
    ViewPager viewPager;
    @Bind(R.id.appbartabs_hotel)
    TabLayout tabLayout;
    private Toolbar mToolbar;
    private Hotel mHotel = null;

    private static final int PAGE_COMENTARIOS = 2;

    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_hotel);
        ButterKnife.bind(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_detalles_hotel);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setViewPager();
        setTabs();
    }

    public void onHotelReady(Hotel hotel) {
        mHotel = hotel;
        getSupportActionBar().setTitle(mHotel.getmNombre());
        setFavorite();
        invalidateOptionsMenu();
        onCreateOptionsMenu(mMenu);
    }

    private void setFavorite() {
        if (getDatabaseReferenceObject()) {
            mHotel.setFavorite(false);
        } else {
            mHotel.setFavorite(true);
        }
    }

    private void setTabs() {
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setViewPager() {
        viewPager.setAdapter(new FragmentHotelAdapter(
                getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(this);
        viewPager.setOffscreenPageLimit(3);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == PAGE_COMENTARIOS) {
            mapaHotel.hide();
        } else {
            mapaHotel.show();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.mMenu = menu;
        if (mHotel != null) {
            getMenuInflater().inflate(R.menu.description_menu, menu);
            MenuItem menuItem = menu.findItem(R.id.action_do_favorite);
            menuItem.setIcon(mHotel.getFavorite() ? R.drawable.ic_favorite : R.drawable.ic_unfavorite);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_do_favorite) {

            if (mHotel != null) {
                if (mHotel.getFavorite()) {
                    item.setIcon(R.drawable.ic_unfavorite);
                    mHotel.setFavorite(false);
                    deleteFavorite();
                } else {
                    item.setIcon(R.drawable.ic_favorite);
                    mHotel.setFavorite(true);
                    saveFavorite();
                }

                return true;
            }
        } else if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteFavorite() {
        mHotel.delete();
    }

    private void saveFavorite() {
        mHotel.save();
    }

    private boolean getDatabaseReferenceObject() {
        Hotel hotel = new Select().from(Hotel.class).where(Hotel_Table.mIdHotel.is(mHotel.getmIdHotel())).querySingle();
        return hotel == null;
    }

    @OnClick(R.id.mapa_hotel)
    public void onClick() {
        Intent map = new Intent(DetallesHotelActivity.this, MapHotelActicity.class);
        map.putExtra(HotelAdapter.HOTEL_ID_TAG, mHotel.getmIdHotel());
        startActivity(map);
    }
}
