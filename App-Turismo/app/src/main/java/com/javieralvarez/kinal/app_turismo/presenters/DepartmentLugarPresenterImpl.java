package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.DepartmentLugarEvent;
import com.javieralvarez.kinal.app_turismo.interactors.DepartmentLugarInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.DepartmentLugarInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.DepartmentLugarView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentLugarPresenterImpl implements DepartmentLugarPresenter {

    private EventBus eventBus;
    private DepartmentLugarInteractor interactor;
    private DepartmentLugarView view;

    public DepartmentLugarPresenterImpl(DepartmentLugarView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new DepartmentLugarInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void getLugares(int id) {
        interactor.execute(id);
    }

    @Override
    @Subscribe
    public void onEventMainThread(DepartmentLugarEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onLugaresReady(event.getLugares());
            } else {
                view.onError(event.getError());
            }
        }
    }
}
