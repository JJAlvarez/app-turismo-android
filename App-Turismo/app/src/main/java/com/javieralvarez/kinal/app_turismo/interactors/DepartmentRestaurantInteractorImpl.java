package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.DepartmentRestaurantRepository;
import com.javieralvarez.kinal.app_turismo.repositories.DepartmentRestaurantRepositoryImpl;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentRestaurantInteractorImpl implements DepartmentRestaurantInteractor {

    private DepartmentRestaurantRepository repository;

    public DepartmentRestaurantInteractorImpl(Activity activity) {
        this.repository = new DepartmentRestaurantRepositoryImpl(activity);
    }
    @Override
    public void execute(int id) {
        repository.getRestaurantes(id);
    }
}
