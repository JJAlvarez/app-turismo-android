package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.ComentariosEvent;
import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.interactors.ComentariosInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.ComentariosInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.ComentariosView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 27/06/2016.
 */
public class ComentariosPresenterImpl implements ComentariosPresenter {

    private EventBus eventBus;
    private ComentariosInteractor interactor;
    private ComentariosView view;

    public ComentariosPresenterImpl(ComentariosView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new ComentariosInteractorImpl(activity);
    }
    @Override
    public void onPause() {
        eventBus.unregister(this);
    }

    @Override
    public void onResume() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        if(view != null) {
            view = null;
        }
    }

    @Override
    @Subscribe
    public void onEventMainThread(ComentariosEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.setComentarios(event.getComentariosList());
            } else {
                view.onError(event.getError());
            }
        }
    }

    @Override
    public void getComentarios(ComentariosFragment.TIPO_COMENTARIO tipo, int id) {
        if(view != null) {
            view.hideComentarios();
            view.showProgressBar();
            interactor.execute(tipo, id);
        }
    }

    @Override
    public void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario) {
        interactor.addComentario(tipo, id, comentario);
    }
}
