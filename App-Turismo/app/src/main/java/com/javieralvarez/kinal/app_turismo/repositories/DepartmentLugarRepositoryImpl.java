package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.DepartmentLugarEvent;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentLugarRepositoryImpl implements DepartmentLugarRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public DepartmentLugarRepositoryImpl(Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
        PreferencesUtil.init(activity);
    }

    @Override
    public void getLugares(int id) {
        app.getApi().getLugaresDepartamento(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<LugarTuristico>>() {
                    @Override
                    public void onResponse(Call<List<LugarTuristico>> call, Response<List<LugarTuristico>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<LugarTuristico>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    private void post(List<LugarTuristico> lugares) {
        DepartmentLugarEvent event = new DepartmentLugarEvent();
        event.setError(null);
        event.setLugares(lugares);
        eventBus.post(event);
    }

    private void postError(String msg) {
        DepartmentLugarEvent event = new DepartmentLugarEvent();
        event.setError(msg);
        eventBus.post(event);
    }
}
