package com.javieralvarez.kinal.app_turismo.utils;

/**
 * Created by JOSUE on 11/5/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesUtil {

    public final static String PREF_USER_ID = "user_id";
    public final static String PREF_USER_MAIL = "user_mail";
    public final static String PREF_USER_NAME = "user_name";
    public final static String PREF_USER_TOKEN = "user_token";
    private final static String PREFERENCES = "turismo_guatemala_preferences";
    private static SharedPreferences mPreferences;

    public static void init(Context context) {
        mPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    public static String getStringPreference(String key, String defaultValue) {
        return mPreferences.getString(key, defaultValue);
    }

    public static void setStringPreference(String key, String value) {
        mPreferences.edit().putString(key, value).commit();
    }

    public static boolean getBooleanPreference(String key, boolean defaultValue) {
        return mPreferences.getBoolean(key, defaultValue);
    }

    public static void setBooleanPreference(String key, boolean value) {
        mPreferences.edit().putBoolean(key, value).commit();
    }

    public static int getIntegerPreference(String key, int defaultValue) {
        return mPreferences.getInt(key, defaultValue);
    }

    public static void setIntegerPreference(String key, int value) {
        mPreferences.edit().putInt(key, value).commit();
    }

    public static void logOut() {
        mPreferences.edit().remove(PREF_USER_ID).apply();
        mPreferences.edit().remove(PREF_USER_MAIL).apply();
        mPreferences.edit().remove(PREF_USER_NAME).apply();
        mPreferences.edit().remove(PREF_USER_TOKEN).apply();
    }

    public static boolean contains(String key) {
        return mPreferences.contains(key);
    }

}

