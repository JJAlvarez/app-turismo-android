package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.MapHotelRepository;
import com.javieralvarez.kinal.app_turismo.repositories.MapHotelRepositoryImpl;

/**
 * Created by Javier on 20/07/2016.
 */
public class MapHotelInteractorImpl implements MapHotelInteractor {

    private MapHotelRepository repository;

    public MapHotelInteractorImpl(Activity activity) {
        this.repository = new MapHotelRepositoryImpl(activity);
    }

    @Override
    public void execute(int idHotel) {
        repository.execute(idHotel);
    }
}
