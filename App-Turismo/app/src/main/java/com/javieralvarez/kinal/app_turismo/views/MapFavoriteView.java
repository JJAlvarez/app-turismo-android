package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;

import java.util.List;

/**
 * Created by Javier on 13/07/2016.
 */
public interface MapFavoriteView {

    void onLugaresReady(List<LugarTuristico> lugares);
    void onHotelesReady(List<Hotel> hoteles);
    void onRestaurantesReady(List<Restaurante> restaurantes);

    void getRestaurantes();
    void getHoteles();
    void getLugares();

    void onError(String error);
}
