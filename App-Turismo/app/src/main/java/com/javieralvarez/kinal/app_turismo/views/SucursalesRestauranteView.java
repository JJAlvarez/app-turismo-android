package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;

import java.util.List;

/**
 * Created by Javier on 06/07/2016.
 */
public interface SucursalesRestauranteView {

    void getSucursales(int id);
    void onSucursalesReady(List<SucursalRestaurante> sucursales);

    void showElements();
    void showProgress();
    void hideElements();
    void hideProgress();

    void onError();
}
