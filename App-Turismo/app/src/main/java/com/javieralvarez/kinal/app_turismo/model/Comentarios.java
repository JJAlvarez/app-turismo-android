package com.javieralvarez.kinal.app_turismo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 27/06/2016.
 */
public class Comentarios {

    @SerializedName("id_comentario")
    private int mIdComentario;

    @SerializedName("comentario")
    private String mComentario;

    @SerializedName("usuario")
    private String mUsuario;

    public int getmIdComentario() {
        return mIdComentario;
    }

    public void setmIdComentario(int mIdComentario) {
        this.mIdComentario = mIdComentario;
    }

    public String getmComentario() {
        return mComentario;
    }

    public void setmComentario(String mComentario) {
        this.mComentario = mComentario;
    }

    public String getmUsuario() {
        return mUsuario;
    }

    public void setmUsuario(String mUsuario) {
        this.mUsuario = mUsuario;
    }

    public Comentarios() {
    }
}
