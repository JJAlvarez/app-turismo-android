package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.SucursalesRestauranteRepository;
import com.javieralvarez.kinal.app_turismo.repositories.SucursalesrestauranteRepositoryImpl;

/**
 * Created by Javier on 06/07/2016.
 */
public class SucursalesRestauranteInteractorImpl implements SucursalesRestauranteInteractor {

    private SucursalesRestauranteRepository repository;

    public SucursalesRestauranteInteractorImpl(Activity activity) {
        repository = new SucursalesrestauranteRepositoryImpl(activity);
    }

    @Override
    public void execute(int id) {
        repository.getSucursales(id);
    }
}
