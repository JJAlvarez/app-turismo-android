package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Comentarios;

import java.util.List;

/**
 * Created by Javier on 27/06/2016.
 */
public interface ComentariosView {

    void hideComentarios();
    void showComentarios();
    void showProgressBar();
    void hideProgressBar();

    void onError(String msg);
    void setComentarios(List<Comentarios> comentarios);

    void deleteComentario();
    void onDeleteError();
}
