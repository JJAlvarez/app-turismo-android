package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 27/04/2016.
 */
public class TipoComida implements Parcelable{

    @SerializedName("id_tipocomida")
    private int mIdTipoComida;

    @SerializedName("nombre")
    private String mNombre;

    @SerializedName("descripcion")
    private String mDescripcion;

    public TipoComida() {
    }

    public int getmIdTipoComida() {
        return mIdTipoComida;
    }

    public void setmIdTipoComida(int mIdTipoComida) {
        this.mIdTipoComida = mIdTipoComida;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public String getmDescripcion() {
        return mDescripcion;
    }

    public void setmDescripcion(String mDescripcion) {
        this.mDescripcion = mDescripcion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIdTipoComida);
        dest.writeString(this.mNombre);
        dest.writeString(this.mDescripcion);
    }

    protected TipoComida(Parcel in) {
        this.mIdTipoComida = in.readInt();
        this.mNombre = in.readString();
        this.mDescripcion = in.readString();
    }

    public static final Parcelable.Creator<TipoComida> CREATOR = new Parcelable.Creator<TipoComida>() {
        @Override
        public TipoComida createFromParcel(Parcel source) {
            return new TipoComida(source);
        }

        @Override
        public TipoComida[] newArray(int size) {
            return new TipoComida[size];
        }
    };
}
