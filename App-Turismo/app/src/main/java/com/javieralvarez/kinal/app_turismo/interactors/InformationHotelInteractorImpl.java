package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.InformationHotelRepository;
import com.javieralvarez.kinal.app_turismo.repositories.InformationHotelRepositoryImpl;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationHotelInteractorImpl implements InformationHotelInteractor {

    private InformationHotelRepository repository;

    public InformationHotelInteractorImpl(Activity activity) {
        this.repository = new InformationHotelRepositoryImpl(activity);
    }

    @Override
    public void getFavoriteHotel(int id) {
        repository.getFavoriteHotel(id);
    }

    @Override
    public void getAPIHotel(int id) {
        repository.getAPIHotel(id);
    }
}
