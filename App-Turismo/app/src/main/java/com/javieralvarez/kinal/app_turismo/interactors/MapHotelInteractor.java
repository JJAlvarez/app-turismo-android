package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 20/07/2016.
 */
public interface MapHotelInteractor {

    void execute(int idHotel);
}
