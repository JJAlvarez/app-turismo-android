package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;

import java.util.List;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentLugarView {

    void showElements();
    void showProgress();
    void hideElements();
    void hideProgress();
    void getLugares();
    void onLugaresReady(List<LugarTuristico> lugares);

    void onError(String error);
}
