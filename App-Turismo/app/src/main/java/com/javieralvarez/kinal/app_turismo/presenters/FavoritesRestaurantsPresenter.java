package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.FavoritesRestaurantsEvent;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesRestaurantsPresenter {

    void onCreate();
    void onDestroy();

    void onEventMainThread(FavoritesRestaurantsEvent event);
    void getRestaurantes();
}
