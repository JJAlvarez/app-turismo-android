package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.FavoritesRestaurantsEvent;
import com.javieralvarez.kinal.app_turismo.interactors.FavoritesRestaurantsInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.FavoritesRestaurantsInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.FavoritesRestaurantsView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesRestaurantsPresenterImpl implements FavoritesRestaurantsPresenter {

    private EventBus eventBus;
    private FavoritesRestaurantsView view;
    private FavoritesRestaurantsInteractor interactor;

    public FavoritesRestaurantsPresenterImpl(FavoritesRestaurantsView view) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new FavoritesRestaurantsInteractorImpl();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    @Subscribe
    public void onEventMainThread(FavoritesRestaurantsEvent event) {
        if(view != null) {
            if (event.getError() == null) {
                view.onFavoritesReady(event.getRestaurantes());
            }
        }
    }

    @Override
    public void getRestaurantes() {
        view.hideElements();
        view.showProgress();
        interactor.execute();
    }
}
