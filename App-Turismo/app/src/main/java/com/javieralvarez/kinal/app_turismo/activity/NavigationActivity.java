package com.javieralvarez.kinal.app_turismo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.facebook.login.LoginManager;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.FavoritesFragmentAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.FragmentsAdapter;
import com.javieralvarez.kinal.app_turismo.fragment.SearchFragment;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;
import com.twitter.sdk.android.Twitter;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String SEARCH_TAG = "search";
    @Bind(R.id.toolbar_main)
    Toolbar toolbarMain;
    @Bind(R.id.appbartabs)
    TabLayout appbartabs;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.launch_map_button)
    FloatingActionButton launchMapButton;
    @Bind(R.id.search_fragment_container)
    FrameLayout searchFragmentContainer;
    @Bind(R.id.main_content)
    CoordinatorLayout mainContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);
        setSupportActionBar(toolbarMain);
        getSupportActionBar().setTitle("Turismo Guatemala");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbarMain, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setViewPager();
        setTabs();
        onMapClick();
    }

    private void setTabs() {
        appbartabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        appbartabs.setupWithViewPager(viewpager);
    }

    private void setViewPager() {
        viewpager.setOffscreenPageLimit(4);
        viewpager.setAdapter(new FragmentsAdapter(
                getSupportFragmentManager()));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Fragment frag = getSupportFragmentManager().findFragmentByTag(SEARCH_TAG);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        if (frag != null) {
            getSupportFragmentManager().popBackStack();
        }
        super.onBackPressed();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_main) {
            setMainViewPager();
        } else if (id == R.id.nav_favorite) {
            setFavoriteViewPager();
        } else if (id == R.id.nav_sigout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setMainViewPager() {
        viewpager.setOffscreenPageLimit(4);
        viewpager.setAdapter(new FragmentsAdapter(getSupportFragmentManager()));
        viewpager.getAdapter().notifyDataSetChanged();
        setTabs();
        getSupportActionBar().setTitle("Turismo Guatemala");
        onMapClick();
    }

    private void setFavoriteViewPager() {
        viewpager.setOffscreenPageLimit(3);
        viewpager.setAdapter(new FavoritesFragmentAdapter(getSupportFragmentManager()));
        viewpager.getAdapter().notifyDataSetChanged();
        setTabs();
        getSupportActionBar().setTitle(getString(R.string.favorite_toolbar_title));
        onFavoriteMapClick();
    }

    private void logout() {
        Intent logout = new Intent(this, InicioSesionActivity.class);
        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PreferencesUtil.init(this);
        PreferencesUtil.logOut();
        Twitter.logOut();
        LoginManager.getInstance().logOut();
        startActivity(logout);
    }

    public void onMapClick() {
        launchMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(NavigationActivity.this, MapActivity.class);
                startActivity(mapIntent);
            }
        });
    }

    private void onFavoriteMapClick() {
        launchMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(NavigationActivity.this, MapFavoriteActivity.class);
                startActivity(mapIntent);
            }
        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menus, menu);
//        final MenuItem searchItem = menu.findItem(R.id.action_search);
//        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//
//        setSearchViewOnActionExpanded(searchItem, searchView);
//        setSearchQuery(searchView);
//        return true;
//    }

    public void setSearchViewOnActionExpanded(MenuItem searchItem, final SearchView searchView) {
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    searchView.setQuery("", false);
                    searchView.clearFocus();
                    getSupportFragmentManager().popBackStack();
                    return true;
                } else {
                    return true;
                }
            }
        });
    }

    public void setSearchQuery(SearchView searchView) {
        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        SearchFragment searchFragment = null;

                        if (getSupportFragmentManager().findFragmentByTag(SEARCH_TAG) != null) {
                            searchFragment = (SearchFragment) getSupportFragmentManager().findFragmentByTag(SEARCH_TAG);
                            searchFragment.setQuery(query);
                        } else {
                            doTrasaction(searchFragment, query);
                        }
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return false;
                    }
                }
        );
    }

    private void doTrasaction(SearchFragment searchFragment, String query) {
        searchFragment = SearchFragment.newInstance(query, SearchFragment.TIPO_BUSQUEDA.BUSQUEDA_TODOS);
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.search_fragment_container, searchFragment, SEARCH_TAG)
                .addToBackStack(null)
                .commit();
    }
}
