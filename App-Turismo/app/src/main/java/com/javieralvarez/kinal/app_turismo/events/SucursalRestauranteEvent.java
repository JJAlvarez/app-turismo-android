package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;

import java.util.List;

/**
 * Created by Javier on 06/07/2016.
 */
public class SucursalRestauranteEvent {

    private String error;
    private List<SucursalRestaurante> sucursales;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<SucursalRestaurante> getSucursales() {
        return sucursales;
    }

    public void setSucursales(List<SucursalRestaurante> sucursales) {
        this.sucursales = sucursales;
    }
}
