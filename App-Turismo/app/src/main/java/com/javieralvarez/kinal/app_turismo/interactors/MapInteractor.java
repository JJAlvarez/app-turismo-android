package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 01/07/2016.
 */
public interface MapInteractor {

    void getRestaurantesLocations();
    void getHotelsLocations();
    void getLuaresLocation();

}
