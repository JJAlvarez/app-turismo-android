package com.javieralvarez.kinal.app_turismo.utils;

import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;

/**
 * Created by Javier on 06/07/2016.
 */
public interface SucursalClickListener {

    void onClick(SucursalRestaurante sucursal);
}
