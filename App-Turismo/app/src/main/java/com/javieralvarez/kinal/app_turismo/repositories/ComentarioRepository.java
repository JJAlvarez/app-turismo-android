package com.javieralvarez.kinal.app_turismo.repositories;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;

/**
 * Created by Javier on 27/06/2016.
 */
public interface ComentarioRepository {

    void getComentarios(ComentariosFragment.TIPO_COMENTARIO tipo, int id);
    void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario);

    void deleteComentario(Comentarios comentarios);
}
