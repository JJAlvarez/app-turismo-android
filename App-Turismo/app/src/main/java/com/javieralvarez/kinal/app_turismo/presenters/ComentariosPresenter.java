package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.ComentariosEvent;
import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;

/**
 * Created by Javier on 27/06/2016.
 */
public interface ComentariosPresenter {

    void onPause();
    void onResume();
    void onDestroy();

    void onEventMainThread(ComentariosEvent event);
    void getComentarios(ComentariosFragment.TIPO_COMENTARIO tipo, int id);
    void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario);
}
