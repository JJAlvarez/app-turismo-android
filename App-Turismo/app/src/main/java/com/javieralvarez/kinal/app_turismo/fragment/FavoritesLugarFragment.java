package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesLugarActivity;
import com.javieralvarez.kinal.app_turismo.adapter.FavoritesLugarAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.LugarAdapter;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.presenters.FavoritesLugaresPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.FavoritesLugaresPresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.ClickLugarListener;
import com.javieralvarez.kinal.app_turismo.views.FavoritesLugaresView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesLugarFragment extends Fragment implements FavoritesLugaresView, ClickLugarListener {

    @Bind(R.id.lugar_rec_view)
    RecyclerView lugarRecView;
    @Bind(R.id.swipeContainerLugar)
    SwipeRefreshLayout swipeContainerLugar;
    @Bind(R.id.progressBarLugares)
    ProgressBar progressBarLugares;
    @Bind(R.id.container_lugar)
    RelativeLayout container;

    private FavoritesLugaresPresenter presenter;
    private FavoritesLugarAdapter adapter;

    public FavoritesLugarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lugar_turistico, container, false);
        ButterKnife.bind(this, view);

        presenter = new FavoritesLugaresPresenterImpl(this);
        presenter.onCreate();
        setupRecycler();
        setupAdapter();
        setSwipeToRefresh();
        getFavoritesPlaces();
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerLugar.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFavoritesPlaces();
            }
        });

        swipeContainerLugar.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setupAdapter() {
        adapter = new FavoritesLugarAdapter(new ArrayList<LugarTuristico>(), getContext(), this);
        lugarRecView.setAdapter(adapter);
        lugarRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void setupRecycler() {
        lugarRecView.setHasFixedSize(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void getFavoritesPlaces() {
        presenter.getPlaces();
    }

    @Override
    public void onFavoritesReady(List<LugarTuristico> lugares) {
        adapter.favorites(lugares);
        showElements();
        hideProgress();
        if(swipeContainerLugar.isRefreshing()) {
            swipeContainerLugar.setRefreshing(false);
        }
    }

    @Override
    public void showElements() {
        lugarRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        lugarRecView.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressBarLugares.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarLugares.setVisibility(View.GONE);
    }

    @Override
    public void onClick(LugarTuristico lugarTuristico) {
        Intent intent = new Intent(getActivity(), DetallesLugarActivity.class);
        intent.putExtra(LugarAdapter.LUGAR_ID_TAG, lugarTuristico.getmIdLugarturistico());
        startActivity(intent);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public static FavoritesLugarFragment newInstance() {
        return new FavoritesLugarFragment();
    }
}
