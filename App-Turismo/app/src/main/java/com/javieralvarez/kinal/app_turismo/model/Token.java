package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 26/05/2016.
 */
public class Token implements Parcelable {

    @SerializedName("token")
    private String mToken;

    @SerializedName("usuario")
    private Usuario mUsuario;

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        this.mToken = token;
    }

    public Usuario getUsuario() {
        return mUsuario;
    }

    public void setUsuario(Usuario usuario) {
        this.mUsuario = usuario;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mToken);
        dest.writeParcelable(mUsuario, flags);
    }

    protected Token(Parcel in) {
        mToken = in.readString();
        mUsuario = in.readParcelable(Usuario.class.getClassLoader());
    }

    public static final Creator<Token> CREATOR = new Creator<Token>() {
        @Override
        public Token createFromParcel(Parcel in) {
            return new Token(in);
        }

        @Override
        public Token[] newArray(int size) {
            return new Token[size];
        }
    };
}
