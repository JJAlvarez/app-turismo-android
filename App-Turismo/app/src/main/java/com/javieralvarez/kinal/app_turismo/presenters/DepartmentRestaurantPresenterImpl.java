package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.DepartmentRestaurantEvent;
import com.javieralvarez.kinal.app_turismo.interactors.DepartmentRestaurantInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.DepartmentRestaurantInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.DepartmentRestaurantView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentRestaurantPresenterImpl implements DepartmentRestaurantPresenter {

    private EventBus eventBus;
    private DepartmentRestaurantInteractor interactor;
    private DepartmentRestaurantView view;

    public DepartmentRestaurantPresenterImpl(DepartmentRestaurantView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new DepartmentRestaurantInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void getRestaurantes(int id) {
        interactor.execute(id);
    }

    @Override
    @Subscribe
    public void onEventMainThread(DepartmentRestaurantEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onRestaurantesReady(event.getRestaurantes());
            } else {
                view.onError(event.getError());
            }
        }
    }
}
