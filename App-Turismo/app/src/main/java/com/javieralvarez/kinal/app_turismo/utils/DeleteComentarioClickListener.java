package com.javieralvarez.kinal.app_turismo.utils;

import com.javieralvarez.kinal.app_turismo.model.Comentarios;

/**
 * Created by Javier on 06/07/2016.
 */
public interface DeleteComentarioClickListener {

    void onDelete(Comentarios comentarios);
}
