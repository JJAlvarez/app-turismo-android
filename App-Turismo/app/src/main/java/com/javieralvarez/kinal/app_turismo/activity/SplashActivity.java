package com.javieralvarez.kinal.app_turismo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.model.Departamento;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;
import com.twitter.sdk.android.Twitter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    public static String DEPARTAMENTOS_TAG = "departamentos";
    public static String RESTAURANTES_TAG = "restaurantes";
    public static String HOTELES_TAG = "hoteles";
    public static String LUGARES_TAG = "lugares";
    private TurismoApp mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        PreferencesUtil.init(this);
        mApp = (TurismoApp) getApplication();

        if (PreferencesUtil.getIntegerPreference(PreferencesUtil.PREF_USER_ID, 0) != 0) {
            makeApiCalls();
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (Twitter.getSessionManager().getActiveSession() != null) {
                        makeApiCalls();
                    } else if (AccessToken.getCurrentAccessToken() != null) {
                        makeApiCalls();
                    } else {
                        Intent login = new Intent(SplashActivity.this, InicioSesionActivity.class);
                        startActivity(login);
                    }
                }
            }, 1500);
        }
    }

    private void makeApiCalls() {
        mApp.getApi().getAllDepartamentos(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<Departamento>>() {
            @Override
            public void onResponse(Call<List<Departamento>> call, Response<List<Departamento>> response) {
                getRestaurantes((ArrayList<Departamento>) response.body());
            }

            @Override
            public void onFailure(Call<List<Departamento>> call, Throwable t) {

            }
        });
    }

    private void getRestaurantes(final ArrayList<Departamento> departamentos) {
        mApp.getApi().getAllRestaurantes(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<Restaurante>>() {
            @Override
            public void onResponse(Call<List<Restaurante>> call, Response<List<Restaurante>> response) {
                getHoteles(departamentos, (ArrayList) response.body());
            }

            @Override
            public void onFailure(Call<List<Restaurante>> call, Throwable t) {

            }
        });
    }

    private void getHoteles(final ArrayList<Departamento> departamentos, final ArrayList<Restaurante> restaurantes) {
        mApp.getApi().getAllHotels(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<Hotel>>() {
            @Override
            public void onResponse(Call<List<Hotel>> call, Response<List<Hotel>> response) {
                getLugares(departamentos, restaurantes, (ArrayList) response.body());
            }

            @Override
            public void onFailure(Call<List<Hotel>> call, Throwable t) {

            }
        });
    }

    private void getLugares(final ArrayList<Departamento> departamentos, final ArrayList<Restaurante> restaurantes,
                            final ArrayList<Hotel> hoteles) {
        mApp.getApi().getAllLugares(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<LugarTuristico>>() {
            @Override
            public void onResponse(Call<List<LugarTuristico>> call, Response<List<LugarTuristico>> response) {
                Intent i = new Intent(SplashActivity.this, NavigationActivity.class);
                i.putParcelableArrayListExtra(DEPARTAMENTOS_TAG, departamentos);
                i.putParcelableArrayListExtra(RESTAURANTES_TAG, restaurantes);
                i.putParcelableArrayListExtra(HOTELES_TAG, hoteles);
                i.putParcelableArrayListExtra(LUGARES_TAG, (ArrayList) response.body());
                startActivity(i);
                finish();
            }

            @Override
            public void onFailure(Call<List<LugarTuristico>> call, Throwable t) {

            }
        });
    }
}
