package com.javieralvarez.kinal.app_turismo.repositories;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentLugarRepository {

    void getLugares(int id);
}
