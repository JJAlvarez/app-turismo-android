package com.javieralvarez.kinal.app_turismo.fragment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.ComentariosAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.LugarAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;
import com.javieralvarez.kinal.app_turismo.presenters.AddComenatarioPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.AddComentarioPresenterImpl;
import com.javieralvarez.kinal.app_turismo.presenters.ComentariosPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.ComentariosPresenterImpl;
import com.javieralvarez.kinal.app_turismo.presenters.DeleteComentarioPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.DeleteComentarioPresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.DeleteComentarioClickListener;
import com.javieralvarez.kinal.app_turismo.views.AddComentarioView;
import com.javieralvarez.kinal.app_turismo.views.ComentariosView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComentariosFragment extends Fragment implements ComentariosView, AddComentarioView, DeleteComentarioClickListener {

    @Bind(R.id.recycler_comentarios)
    RecyclerView recyclerComentarios;
    @Bind(R.id.txt_comentario)
    EditText txtComentario;
    @Bind(R.id.enviar_comentario)
    FloatingActionButton enviarComentario;
    @Bind(R.id.progressBarComentarios)
    ProgressBar progressBarComentarios;
    @Bind(R.id.containerComentarios)
    RelativeLayout containerComentarios;
    @Bind(R.id.swipeContainerComentarios)
    SwipeRefreshLayout swipeContainerComentarios;

    private ProgressDialog progress;

    private ComentariosPresenter presenter;
    private ComentariosAdapter adapter;
    private DeleteComentarioPresenter deleteComentarioPresenter;
    private AddComenatarioPresenter addComenatarioPresenter;
    private int mId;

    AlertDialog dialog;
    private Comentarios deleteComentario;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public enum TIPO_COMENTARIO {
        LUGAR_TURISTICO,
        RESTAURANTE,
        HOTEL
    }

    private TIPO_COMENTARIO tipo;
    private UpdateEvent.TIPO_UPDATE tipo_update;

    public ComentariosFragment(TIPO_COMENTARIO tipo) {
        this.tipo = tipo;
    }

    public static ComentariosFragment newInstance(TIPO_COMENTARIO tipo) {
        return new ComentariosFragment(tipo);
    }

    public ComentariosFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_comentarios, container, false);
        ButterKnife.bind(this, view);
        presenter = new ComentariosPresenterImpl(this, getActivity());
        addComenatarioPresenter = new AddComentarioPresenterImpl(this, getActivity());
        deleteComentarioPresenter = new DeleteComentarioPresenterImpl(this, getActivity());
        deleteComentarioPresenter.onCreate();
        setupDeleteProgress();
        setupRecyclerView();
        setupAdapter();
        createAndShowAlertDialog();
        mId = setupId();
        presenter.getComentarios(tipo, mId);
        setSwipeToRefresh();
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerComentarios.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getComentarios(tipo, mId);
            }
        });

        swipeContainerComentarios.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private int setupId() {
        switch (tipo) {
            case RESTAURANTE:
                if (getActivity().getIntent().hasExtra(RestaurantAdapter.RESTAURANTE_ID_TAG)) {
                    return getActivity().getIntent().getIntExtra(RestaurantAdapter.RESTAURANTE_ID_TAG, 0);
                }
                break;
            case LUGAR_TURISTICO:
                if (getActivity().getIntent().hasExtra(LugarAdapter.LUGAR_ID_TAG)) {
                    return getActivity().getIntent().getIntExtra(LugarAdapter.LUGAR_ID_TAG, 0);
                }
                break;
            case HOTEL:
                if (getActivity().getIntent().hasExtra(HotelAdapter.HOTEL_ID_TAG)) {
                    return getActivity().getIntent().getIntExtra(HotelAdapter.HOTEL_ID_TAG, 0);
                }
                break;
        }
        return 0;
    }

    private void setupAdapter() {
        adapter = new ComentariosAdapter(getActivity(), new ArrayList<Comentarios>(), this);
        recyclerComentarios.setAdapter(adapter);
    }

    private void setupRecyclerView() {
        recyclerComentarios.setHasFixedSize(true);
        recyclerComentarios.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void hideComentarios() {
        recyclerComentarios.setVisibility(View.GONE);
    }

    @Override
    public void showComentarios() {
        recyclerComentarios.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressBar() {
        progressBarComentarios.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarComentarios.setVisibility(View.GONE);
    }

    @Override
    public void onError(String msg) {
        Snackbar.make(containerComentarios, getString(R.string.comentarios_error), Snackbar.LENGTH_LONG).show();
        if(swipeContainerComentarios.isRefreshing()) {
            swipeContainerComentarios.setRefreshing(false);
        }
    }

    @Override
    public void setComentarios(List<Comentarios> comentarios) {
        adapter.setComentarios(comentarios);
        showComentarios();
        hideProgressBar();
        recyclerComentarios.scrollToPosition(adapter.getItemCount() - 1);
        if(swipeContainerComentarios.isRefreshing()) {
            swipeContainerComentarios.setRefreshing(false);
        }
    }

    @Override
    public void onComentarioAdded(Comentarios comentario) {
        adapter.add(comentario);
        txtComentario.setText("");
        recyclerComentarios.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void onComentarioAddedError(String error) {
        Snackbar.make(containerComentarios, getString(R.string.comentarios_add_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onResume() {
        presenter.onResume();
        addComenatarioPresenter.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        presenter.onPause();
        addComenatarioPresenter.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        addComenatarioPresenter.onDestroy();
        deleteComentarioPresenter.onDestroy();
        super.onDestroy();
    }

    @OnClick(R.id.enviar_comentario)
    public void enviarComentario() {
        addComenatarioPresenter.addComentario(tipo, mId, txtComentario.getText().toString());
    }

    @Override
    public void onDelete(Comentarios comentarios) {
        deleteComentario = comentarios;
        dialog.show();
    }

    private void createAndShowAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Esta seguro de eliminar este comentario?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteComentarioPresenter.deleteComentario(deleteComentario);
                progress.show();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        dialog = builder.create();
    }

    @Override
    public void deleteComentario() {
        adapter.deleteComentario(deleteComentario);
        progress.dismiss();
    }

    @Override
    public void onDeleteError() {
        Snackbar.make(containerComentarios, getString(R.string.comentarios_delete_error), Snackbar.LENGTH_LONG).show();
    }

    private void setupDeleteProgress() {
        progress = new ProgressDialog(getActivity());
        progress.setTitle(getString(R.string.comentarios_progress));
        progress.setMessage(getString(R.string.please_wait));
    }
}
