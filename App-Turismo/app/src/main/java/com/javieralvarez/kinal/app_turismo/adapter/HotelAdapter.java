package com.javieralvarez.kinal.app_turismo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesHotelActivity;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Javier on 18/05/2016.
 */
public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.HotelViewHolder> {

    private Context mContext;
    private ArrayList<Hotel> mData;
    private TelefonoClickListener listener;

    public static String HOTEL_DETALLES_TAG = "hotel";
    public static String HOTEL_ID_TAG = "hotelID";
    private int lastPosition = -1;

    public HotelAdapter(Context context, ArrayList<Hotel> data, TelefonoClickListener listener) {
        this.mData = data;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public HotelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hotel, parent, false);

        HotelViewHolder hvh = new HotelViewHolder(itemView);

        return hvh;
    }

    @Override
    public void onBindViewHolder(HotelViewHolder holder, int position) {
        Hotel item = mData.get(position);
        setAnimation(holder.mView, position);
        holder.bindHotel(item);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void updateAll(List<Hotel> updates) {
        mData.clear();
        mData.addAll(updates);
        notifyDataSetChanged();
    }

    public class HotelViewHolder extends RecyclerView.ViewHolder {

        private ImageView mLogo;
        private TextView mLblNombre;
        private TextView mPbx;
        private CardView mView;
        @Bind(R.id.btn_call_hotel)
        CircleImageView btnCallHotel;

        public HotelViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = (CardView) itemView.findViewById(R.id.card_view_hotel);
            this.mLogo = (ImageView) itemView.findViewById(R.id.img_hotel);
            this.mLblNombre = (TextView) itemView.findViewById(R.id.lbl_name_hotel);
            this.mPbx = (TextView) itemView.findViewById(R.id.lbl_phone_hotel);
        }

        public void bindHotel(final Hotel hotel) {
            mLblNombre.setText(hotel.getmNombre());
            this.mPbx.setText(String.valueOf(hotel.getmPbx()));
            Glide.with(mContext).load(hotel.getmUrlLogo()).into(mLogo);
            this.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, DetallesHotelActivity.class);
                    intent.putExtra(HOTEL_ID_TAG, hotel.getmIdHotel());
                    mContext.startActivity(intent);
                }
            });
            this.btnCallHotel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onTelefonoClick(hotel.getmPbx());
                }
            });
        }

        public void clearAnimation()
        {
            mView.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            animation.setDuration(500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(HotelViewHolder holder) {
        ((HotelViewHolder)holder).clearAnimation();
    }
}
