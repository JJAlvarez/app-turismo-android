package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.DeleteComentarioEvent;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;

/**
 * Created by Javier on 06/07/2016.
 */
public interface DeleteComentarioPresenter {

    void deleteComentario(Comentarios comentarios);
    void onCreate();
    void onDestroy();

    void onEventMainThread(DeleteComentarioEvent event);
}
