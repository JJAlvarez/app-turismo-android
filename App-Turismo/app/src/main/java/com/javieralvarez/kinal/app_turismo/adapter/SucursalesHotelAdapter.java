package com.javieralvarez.kinal.app_turismo.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;
import com.javieralvarez.kinal.app_turismo.utils.SucursalHotelClickListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Javier on 07/07/2016.
 */
public class SucursalesHotelAdapter extends RecyclerView.Adapter<SucursalesHotelAdapter.ViewHolder> {

    private List<SucursalHotel> sucursales;
    private Context context;
    private SucursalHotelClickListener listener;

    public SucursalesHotelAdapter(List<SucursalHotel> sucursales, Context context, SucursalHotelClickListener listener) {
        this.sucursales = sucursales;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_address, parent, false);

        ViewHolder hvh = new ViewHolder(itemView);

        return hvh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SucursalHotel sucursal = sucursales.get(position);
        holder.txtAddressDetail.setText(sucursal.getmDireccion());
        holder.txtPhoneDetail.setText(String.valueOf(sucursal.getTelefono()));
        holder.setGoClick(sucursal);
    }

    @Override
    public int getItemCount() {
        return sucursales.size();
    }

    public void setSucursales(List<SucursalHotel> updates) {
        sucursales.clear();
        sucursales.addAll(updates);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.img_go_address)
        ImageView imgGoAddress;
        @Bind(R.id.txt_address_detail)
        TextView txtAddressDetail;
        @Bind(R.id.txt_phone_detail)
        TextView txtPhoneDetail;
        @Bind(R.id.card_view_address)
        CardView cardViewAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setGoClick(final SucursalHotel sucursal) {
            cardViewAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(sucursal);
                }
            });
        }
    }
}
