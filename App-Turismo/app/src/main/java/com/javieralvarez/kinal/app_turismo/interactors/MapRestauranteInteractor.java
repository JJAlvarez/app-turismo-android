package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 20/07/2016.
 */
public interface MapRestauranteInteractor {

    void execute(int idRestaurante);
}
