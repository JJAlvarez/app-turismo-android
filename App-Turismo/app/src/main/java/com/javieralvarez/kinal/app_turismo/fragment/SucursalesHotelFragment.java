package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.SucursalesHotelAdapter;
import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;
import com.javieralvarez.kinal.app_turismo.presenters.SucursalesHotelPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.SucursalesHotelPresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.SucursalHotelClickListener;
import com.javieralvarez.kinal.app_turismo.views.SucursalesHotelView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SucursalesHotelFragment extends Fragment implements SucursalesHotelView, SucursalHotelClickListener {

    @Bind(R.id.sucursal_rec_view)
    RecyclerView sucursalRecView;
    @Bind(R.id.swipeContainerSucursales)
    SwipeRefreshLayout swipeContainerSucursales;
    @Bind(R.id.progressBarSucursal)
    ProgressBar progressBarSucursal;
    @Bind(R.id.container_sucursales)
    RelativeLayout containerSucursales;

    private SucursalesHotelPresenter presenter;
    private SucursalesHotelAdapter adapter;
    private int idHotel;

    public SucursalesHotelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sucursales, container, false);
        ButterKnife.bind(this, view);
        presenter = new SucursalesHotelPresenterImpl(this, getActivity());
        presenter.onCreate();
        setupHotelId();
        setupRecycler();
        setSwipeToRefresh();
        setupAdapter();
        getSucursales(idHotel);
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerSucursales.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getSucursales(idHotel);
            }
        });

        swipeContainerSucursales.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setupAdapter() {
        adapter = new SucursalesHotelAdapter(new ArrayList<SucursalHotel>(), getContext(), this);
        sucursalRecView.setAdapter(adapter);
        sucursalRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void setupRecycler() {
        sucursalRecView.setHasFixedSize(true);
    }

    private void setupHotelId() {
        if (getActivity().getIntent().hasExtra(HotelAdapter.HOTEL_ID_TAG)) {
            idHotel = getActivity().getIntent().getIntExtra(HotelAdapter.HOTEL_ID_TAG, 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void getSucursales(int id) {
        presenter.getSucursales(id);
    }

    @Override
    public void onSucursalesReady(List<SucursalHotel> sucursales) {
        adapter.setSucursales(sucursales);
        showElements();
        hideProgress();
        if(swipeContainerSucursales.isRefreshing()) {
            swipeContainerSucursales.setRefreshing(false);
        }
    }

    @Override
    public void showElements() {
        sucursalRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressBarSucursal.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        sucursalRecView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBarSucursal.setVisibility(View.GONE);
    }

    @Override
    public void onError(String error) {
        Snackbar.make(containerSucursales, getString(R.string.sucursales_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onClick(SucursalHotel sucursal) {
        try
        {
            String url = "waze://?ll=" + sucursal.getmLatitud() + "," + sucursal.getmLongitud() + "&z=10&navigate=yes";
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
            startActivity( intent );
        }
        catch ( ActivityNotFoundException ex  )
        {
            Intent intent =
                    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
            startActivity(intent);
        }
    }

    public static SucursalesHotelFragment newInstance() {
        return new SucursalesHotelFragment();
    }
}
