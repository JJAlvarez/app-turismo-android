package com.javieralvarez.kinal.app_turismo.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

/**
 * Created by JOSUE on 10/29/2015.
 */
public class ListViewWithoutScroll extends LinearLayout {

    private BaseAdapter mAdapter;
    private View[] mViews;
    private AdapterView.OnItemClickListener mOnItemClickListener;
    private boolean mIsPopulated = false;

    public ListViewWithoutScroll(Context context) {
        super(context);
        setOrientation(VERTICAL);
    }

    public ListViewWithoutScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
    }

    public ListViewWithoutScroll(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(VERTICAL);

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
        if (isPopulated()) {
            for (int i = 0; i < mViews.length; i++) {
                final int aux = i;
                mViews[i].setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null) {
                            mOnItemClickListener.onItemClick(null, mViews[aux], aux, aux);
                        }
                    }
                });
            }
        }
    }

    public void setAdapter(BaseAdapter adapter) {
        removeAllViews();
        mAdapter = adapter;
        populateView();
        requestLayout();
    }

    private synchronized void populateView() {
        int count = mAdapter.getCount();

        mViews = new View[count];

        for(int i = 0; i < count; i++) {
            if (mViews[i] == null) {
                mViews[i] = mAdapter.getView(i, null, null);
                addView(mViews[i]);
                if (mOnItemClickListener != null) {
                    final int aux = i;
                    mViews[i].setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mOnItemClickListener != null) {
                                mOnItemClickListener.onItemClick(null, mViews[aux], aux, aux);
                            }
                        }
                    });
                }
            } else {
                addView(mViews[i]);
            }
        }
        mIsPopulated = true;
    }

    private synchronized boolean isPopulated() {
        return mIsPopulated;
    }
}
