package com.javieralvarez.kinal.app_turismo.repositories;

import com.javieralvarez.kinal.app_turismo.events.FavoritesLugaresEvent;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.raizlabs.android.dbflow.list.FlowCursorList;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesLugaresRepositoryImpl implements FavoritesLugaresRepository {

    private EventBus eventBus;
    private FavoritesLugaresEvent event;

    public FavoritesLugaresRepositoryImpl() {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.event = new FavoritesLugaresEvent();
    }

    @Override
    public void getPlaces() {
        FlowCursorList<LugarTuristico> storedRecipes = new FlowCursorList<LugarTuristico>(false, LugarTuristico.class);
        post(storedRecipes.getAll());
    }

    private void post(List<LugarTuristico> lugares) {
        event.setError(null);
        event.setLugares(lugares);
        eventBus.post(event);
    }
}
