package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.DepartmentLugarEvent;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentLugarPresenter {

    void onCreate();
    void onDestroy();

    void getLugares(int id);
    void onEventMainThread(DepartmentLugarEvent event);
}
