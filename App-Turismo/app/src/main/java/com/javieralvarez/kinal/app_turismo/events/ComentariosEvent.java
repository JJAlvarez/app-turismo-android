package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Comentarios;

import java.util.List;

/**
 * Created by Javier on 27/06/2016.
 */
public class ComentariosEvent {

    private String error;
    private List<Comentarios> comentariosList;

    public ComentariosEvent() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Comentarios> getComentariosList() {
        return comentariosList;
    }

    public void setComentariosList(List<Comentarios> comentariosList) {
        this.comentariosList = comentariosList;
    }
}
