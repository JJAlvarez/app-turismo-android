package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.SucursalRestauranteEvent;
import com.javieralvarez.kinal.app_turismo.interactors.SucursalesRestauranteInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.SucursalesRestauranteInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.SucursalesRestauranteView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 06/07/2016.
 */
public class SucursalesRestaurantePresenterImpl implements SucursalesRestaurantePresenter {

    private EventBus eventBus;
    private SucursalesRestauranteView view;
    private SucursalesRestauranteInteractor interactor;

    public SucursalesRestaurantePresenterImpl(SucursalesRestauranteView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new SucursalesRestauranteInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        if(view != null) {
            eventBus.unregister(this);
            view = null;
        }
    }

    @Override
    public void getSucursales(int id) {
        interactor.execute(id);
    }

    @Override
    @Subscribe
    public void onEventMainThread(SucursalRestauranteEvent event) {
        if(view != null) {
            if (event.getError() == null) {
                view.onSucursalesReady(event.getSucursales());
            } else {
                view.onError();
            }
        }
    }
}
