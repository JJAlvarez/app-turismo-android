package com.javieralvarez.kinal.app_turismo.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.model.Departamento;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.model.Token;
import com.javieralvarez.kinal.app_turismo.model.TokenTwitter;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InicioSesionActivity extends AppCompatActivity {

    public static String DEPARTAMENTOS_TAG = "departamentos";
    public static String RESTAURANTES_TAG = "restaurantes";
    public static String HOTELES_TAG = "hoteles";
    public static String LUGARES_TAG = "lugares";
    @Bind(R.id.btn_twitter)
    TwitterLoginButton btnTwitter;
    @Bind(R.id.container_login)
    RelativeLayout containerLogin;
    @Bind(R.id.facebook_login)
    LoginButton facebookLogin;
    @Bind(R.id.txt_username)
    EditText txtUsername;
    @Bind(R.id.txt_password)
    EditText txtPassword;
    @Bind(R.id.btn_login)
    Button btnLogin;
    @Bind(R.id.btn_registrarme)
    Button btnRegistrarme;

    private TurismoApp mApp;
    private ProgressDialog progress;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);

        ButterKnife.bind(this);
        callbackManager = CallbackManager.Factory.create();
        facebookLogin.setPublishPermissions(Arrays.asList("publish_actions"));
        setFacebookCallback();
        setTwitterCallback();
        PreferencesUtil.init(this);
        mApp = (TurismoApp) getApplication();

        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.show();
            }
        });
        progress = new ProgressDialog(InicioSesionActivity.this);
        progress.setTitle(getString(R.string.iniciando));
        progress.setMessage(getString(R.string.please_wait));

        setOnClicks();
    }

    private void setOnClicks() {
        btnRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrar = new Intent(InicioSesionActivity.this, RegistroActivity.class);
                startActivity(registrar);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress.show();
                mApp.getApi().login(txtUsername.getText().toString(), txtPassword.getText().toString()).enqueue(new Callback<Token>() {
                    @Override
                    public void onResponse(Call<Token> call, Response<Token> response) {
                        if (response.body() != null) {
                            saveUser(response);
                            makeApiCalls();
                        } else {
                            progress.dismiss();
                            Toast.makeText(InicioSesionActivity.this, "Credenciales invalidas, vuelva a intentarlo", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Token> call, Throwable t) {

                    }
                });
            }
        });
    }

    private void setTwitterCallback() {
        btnTwitter.setCallback(new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                saveTwitterUser();
                navigateToMainScreen();
            }

            @Override
            public void failure(TwitterException exception) {
                String messageError = String.format(getString(R.string.twitter_error), exception.getLocalizedMessage());
                Snackbar.make(containerLogin, messageError, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void setFacebookCallback() {
        facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                saveFacebookUser();
                navigateToMainScreen();
            }

            @Override
            public void onCancel() {
                Snackbar.make(containerLogin, getString(R.string.facebook_cancel), Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                String msgError = String.format(getString(R.string.facebook_error), error.getLocalizedMessage());
                Snackbar.make(containerLogin, msgError, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void saveTwitterUser() {
        PreferencesUtil.setStringPreference(PreferencesUtil.PREF_USER_NAME, Twitter.getSessionManager().getActiveSession().getUserName());
    }

    private void saveFacebookUser() {
        PreferencesUtil.setStringPreference(PreferencesUtil.PREF_USER_NAME, Profile.getCurrentProfile().getName());
    }

    private void makeApiCalls() {
        mApp.getApi().getAllDepartamentos(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<Departamento>>() {
            @Override
            public void onResponse(Call<List<Departamento>> call, Response<List<Departamento>> response) {
                getRestaurantes((ArrayList<Departamento>) response.body());
            }

            @Override
            public void onFailure(Call<List<Departamento>> call, Throwable t) {

            }
        });
    }

    private void saveUser(Response<Token> response) {
        PreferencesUtil.setIntegerPreference(PreferencesUtil.PREF_USER_ID, response.body().getUsuario().getmIdUsuario());
        PreferencesUtil.setStringPreference(PreferencesUtil.PREF_USER_MAIL, response.body().getUsuario().getmCorreo());
        PreferencesUtil.setStringPreference(PreferencesUtil.PREF_USER_NAME, response.body().getUsuario().getmNombre());
        PreferencesUtil.setStringPreference(PreferencesUtil.PREF_USER_TOKEN, response.body().getToken());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE == requestCode) {
            btnTwitter.onActivityResult(requestCode, resultCode, data);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void navigateToMainScreen() {
        mApp.getApi().getTokenForTwitterUser().enqueue(new Callback<TokenTwitter>() {
            @Override
            public void onResponse(Call<TokenTwitter> call, Response<TokenTwitter> response) {
                PreferencesUtil.setStringPreference(PreferencesUtil.PREF_USER_TOKEN, response.body().getmToken());
                makeApiCalls();
            }

            @Override
            public void onFailure(Call<TokenTwitter> call, Throwable t) {
                progress.dismiss();
            }
        });
    }

    private void getRestaurantes(final ArrayList<Departamento> departamentos) {
        mApp.getApi().getAllRestaurantes(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<Restaurante>>() {
            @Override
            public void onResponse(Call<List<Restaurante>> call, Response<List<Restaurante>> response) {
                getHoteles(departamentos, (ArrayList) response.body());
            }

            @Override
            public void onFailure(Call<List<Restaurante>> call, Throwable t) {

            }
        });
    }

    private void getHoteles(final ArrayList<Departamento> departamentos, final ArrayList<Restaurante> restaurantes) {
        mApp.getApi().getAllHotels(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<Hotel>>() {
            @Override
            public void onResponse(Call<List<Hotel>> call, Response<List<Hotel>> response) {
                getLugares(departamentos, restaurantes, (ArrayList) response.body());
            }

            @Override
            public void onFailure(Call<List<Hotel>> call, Throwable t) {

            }
        });
    }

    private void getLugares(final ArrayList<Departamento> departamentos, final ArrayList<Restaurante> restaurantes,
                            final ArrayList<Hotel> hoteles) {
        mApp.getApi().getAllLugares(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, "")).enqueue(new Callback<List<LugarTuristico>>() {
            @Override
            public void onResponse(Call<List<LugarTuristico>> call, Response<List<LugarTuristico>> response) {
                Intent i = new Intent(InicioSesionActivity.this, NavigationActivity.class);
                i.putParcelableArrayListExtra(DEPARTAMENTOS_TAG, departamentos);
                i.putParcelableArrayListExtra(RESTAURANTES_TAG, restaurantes);
                i.putParcelableArrayListExtra(HOTELES_TAG, hoteles);
                i.putParcelableArrayListExtra(LUGARES_TAG, (ArrayList) response.body());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                progress.dismiss();
                Toast.makeText(InicioSesionActivity.this, R.string.bienvenido, Toast.LENGTH_SHORT).show();
                startActivity(i);
                InicioSesionActivity.this.finish();
            }

            @Override
            public void onFailure(Call<List<LugarTuristico>> call, Throwable t) {

            }
        });
    }
}
