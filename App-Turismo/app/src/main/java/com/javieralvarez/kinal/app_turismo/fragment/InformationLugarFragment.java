package com.javieralvarez.kinal.app_turismo.fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.SendButton;
import com.facebook.share.widget.ShareButton;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesLugarActivity;
import com.javieralvarez.kinal.app_turismo.adapter.LugarAdapter;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.presenters.InformationLugarPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.InformationLugarPresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.InformationLugarView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformationLugarFragment extends Fragment implements InformationLugarView {

    @Bind(R.id.img_inf_lugar)
    ImageView imgInfLugar;
    @Bind(R.id.txt_inf_lugar_descripcion)
    TextView txtInfLugarDescripcion;
    @Bind(R.id.fbShare)
    ShareButton fbShare;
    @Bind(R.id.fbSend)
    SendButton fbSend;
    @Bind(R.id.progressInformationLugar)
    ProgressBar progressInformationLugar;
    @Bind(R.id.container)
    NestedScrollView container;
    @Bind(R.id.elements_information_lugar)
    RelativeLayout elementsInformationLugar;

    private int idLugar;
    private LugarTuristico lugarTuristico;

    private InformationLugarPresenter presenter;

    public InformationLugarFragment() {
        // Required empty public constructor
    }

    public static InformationLugarFragment newInstance() {
        InformationLugarFragment fragment = new InformationLugarFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_information_lugar, container, false);
        ButterKnife.bind(this, view);
        presenter = new InformationLugarPresenterImpl(this, getActivity());
        presenter.onCreate();
        if (getActivity().getIntent().hasExtra(LugarAdapter.LUGAR_ID_TAG)) {
            idLugar = getActivity().getIntent().getIntExtra(LugarAdapter.LUGAR_ID_TAG, 0);
        }
        getLugar(idLugar);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void getLugar(int id) {
        presenter.getLugar(id);
    }

    @Override
    public void onError(String error) {
        Snackbar.make(container, getString(R.string.restaurante_error_message), Snackbar.LENGTH_LONG).show();
        hideProgress();
        showElements();
    }

    @Override
    public void onLugarReady(LugarTuristico lugarTuristico) {
        this.lugarTuristico = lugarTuristico;
        setTitleLugar();
        txtInfLugarDescripcion.setText(lugarTuristico.getmDescripcion());
        Glide.with(this).load(lugarTuristico.getmUrlLogo()).into(imgInfLugar);
        setContentFacebook();
        hideProgress();
        showElements();
    }

    private void setTitleLugar() {
        ((DetallesLugarActivity)getActivity()).onLugarReady(lugarTuristico);
    }

    private void setContentFacebook() {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(lugarTuristico.getUrlWeb()))
                .build();
        fbShare.setShareContent(content);
        fbSend.setShareContent(content);
    }

    @Override
    public void hideElements() {
        elementsInformationLugar.setVisibility(View.GONE);
    }

    @Override
    public void showElements() {
        elementsInformationLugar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressInformationLugar.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressInformationLugar.setVisibility(View.VISIBLE);
    }
}
