package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

/**
 * Created by Javier on 20/07/2016.
 */
public interface MapHotelView {

    void getSucursales(int idHotel);
    void onError(String error);
    void onSucursalesReady(Hotel hotel);
}
