package com.javieralvarez.kinal.app_turismo.events;

import java.util.List;

/**
 * Created by Javier on 30/06/2016.
 */
public class UpdateEvent {

    public static enum TIPO_UPDATE {
        UPDATE_COMENTARIO,
        UPDATE_HOTEL,
        UPDATE_LUGAR,
        UPDATE_RESTAURANTE
    }

    public String error;
    public List<Object> updates;
    public TIPO_UPDATE tipo;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Object> getUpdates() {
        return updates;
    }

    public void setUpdates(List<Object> updates) {
        this.updates = updates;
    }

    public TIPO_UPDATE getTipo() {
        return tipo;
    }

    public void setTipo(TIPO_UPDATE tipo) {
        this.tipo = tipo;
    }
}
