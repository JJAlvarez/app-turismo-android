package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

import java.util.List;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentHotelEvent {

    private String error;
    private List<Hotel> hoteles;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Hotel> getHoteles() {
        return hoteles;
    }

    public void setHoteles(List<Hotel> hoteles) {
        this.hoteles = hoteles;
    }
}
