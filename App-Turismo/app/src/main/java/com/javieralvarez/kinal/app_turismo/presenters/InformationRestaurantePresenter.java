package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.InformationRestauranteEvent;

/**
 * Created by Javier on 15/07/2016.
 */
public interface InformationRestaurantePresenter {

    void onCreate();
    void onDestroy();

    void getRestaurante(int id);
    void onEventMainThread(InformationRestauranteEvent event);
}
