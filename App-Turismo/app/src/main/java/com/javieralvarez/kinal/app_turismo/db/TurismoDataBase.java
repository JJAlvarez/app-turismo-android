package com.javieralvarez.kinal.app_turismo.db;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Javier on 28/06/2016.
 */
@Database(name = TurismoDataBase.NAME, version = TurismoDataBase.VERSION)
public class TurismoDataBase {

    public static final int VERSION = 5;
    public static final String NAME = "TurismoGuatemalaV5";
}
