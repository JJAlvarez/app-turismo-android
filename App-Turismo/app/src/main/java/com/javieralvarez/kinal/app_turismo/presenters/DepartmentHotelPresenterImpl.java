package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.DepartmentHotelEvent;
import com.javieralvarez.kinal.app_turismo.interactors.DepartmentHotelInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.DepartmentHotelInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.DepartmentHotelView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentHotelPresenterImpl implements DepartmentHotelPresenter {

    private EventBus eventBus;
    private DepartmentHotelInteractor interactor;
    private DepartmentHotelView view;

    public DepartmentHotelPresenterImpl(DepartmentHotelView view, Activity activity) {
        this.view = view;
        interactor = new DepartmentHotelInteractorImpl(activity);
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void getHotels(int id) {
        interactor.execute(id);
    }

    @Override
    @Subscribe
    public void onEventMainThread(DepartmentHotelEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onHotelesReady(event.getHoteles());
            } else {
                view.onError(event.getError());
            }
        }
    }
}
