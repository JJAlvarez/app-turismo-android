package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Restaurante;

import java.util.List;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentRestaurantView {

    void showElements();
    void showProgress();
    void hideElements();
    void hideProgress();
    void getRestaurantes();
    void onRestaurantesReady(List<Restaurante> restaurantes);

    void onError(String error);
}
