package com.javieralvarez.kinal.app_turismo.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;
import com.javieralvarez.kinal.app_turismo.presenters.MapHotelPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.MapHotelPresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.MapHotelView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapHotelActicity extends AppCompatActivity implements MapHotelView, OnMapReadyCallback {

    MapFragment supportMapFragment;
    @Bind(R.id.toolbar_map)
    Toolbar toolbarMap;
    @Bind(R.id.container_map)
    RelativeLayout containerMap;

    private GoogleMap mMap;

    private MapHotelPresenter presenter;
    private int idHotel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        presenter = new MapHotelPresenterImpl(this, this);
        presenter.onCreate();
        supportMapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
        setSupportActionBar(toolbarMap);
        setupHotelId();
        getSucursales(idHotel);
    }

    private void setupHotelId() {
        if (getIntent().hasExtra(HotelAdapter.HOTEL_ID_TAG)) {
            idHotel = getIntent().getIntExtra(HotelAdapter.HOTEL_ID_TAG, 0);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap = googleMap;
    }

    @Override
    public void getSucursales(int id) {
        presenter.getSucursales(id);
    }

    @Override
    public void onSucursalesReady(Hotel hotel) {
        mMap.clear();
        for (SucursalHotel sucursal : hotel.getmSucursales()) {
            LatLng HOTEL = new LatLng(sucursal.getmLatitud(), sucursal.getmLongitud());
            mMap.addMarker(new MarkerOptions()
                    .position(HOTEL)
                    .title(hotel.getmNombre())
                    .snippet("Descripcion: " + hotel.getmDescripcion()));
        }
        getSupportActionBar().setTitle(hotel.getmNombre());
    }

    @Override
    public void onError(String error) {
        Snackbar.make(containerMap, getString(R.string.sucursales_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
