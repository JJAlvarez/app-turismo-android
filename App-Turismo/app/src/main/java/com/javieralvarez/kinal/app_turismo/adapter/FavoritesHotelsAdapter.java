package com.javieralvarez.kinal.app_turismo.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.utils.ClickHotelListener;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Javier on 04/07/2016.
 */
public class FavoritesHotelsAdapter extends RecyclerView.Adapter<FavoritesHotelsAdapter.ViewHolder> {

    private ArrayList<Hotel> hotels;
    private Context context;
    private ClickHotelListener listener;
    private TelefonoClickListener telListener;
    private int lastPosition = -1;

    public FavoritesHotelsAdapter(ArrayList<Hotel> data, Context context, ClickHotelListener listener, TelefonoClickListener telListener) {
        this.hotels = data;
        this.context = context;
        this.listener = listener;
        this.telListener = telListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hotel, parent, false);

        ViewHolder hvh = new ViewHolder(itemView);

        return hvh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Hotel hotel = hotels.get(position);
        setAnimation(holder.cardViewHotel, position);
        holder.lblNameHotel.setText(hotel.getmNombre());
        holder.lblPhoneHotel.setText(String.valueOf(hotel.getmPbx()));
        Glide.with(context).load(hotel.getmUrlLogo()).into(holder.imgHotel);
        holder.setClickListener(hotel, listener);
    }

    @Override
    public int getItemCount() {
        return hotels.size();
    }

    public void setFavorites(List<Hotel> favorites) {
        hotels.clear();
        hotels.addAll(favorites);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.btn_call_hotel)
        CircleImageView btnCallHotel;
        @Bind(R.id.img_hotel)
        ImageView imgHotel;
        @Bind(R.id.lbl_name_hotel)
        TextView lblNameHotel;
        @Bind(R.id.lbl_phone_hotel)
        TextView lblPhoneHotel;
        @Bind(R.id.card_view_hotel)
        CardView cardViewHotel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setClickListener(final Hotel hotel, final ClickHotelListener listener) {
            cardViewHotel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(hotel);
                }
            });
            this.btnCallHotel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    telListener.onTelefonoClick(hotel.getmPbx());
                }
            });
        }

        public void clearAnimation() {
            cardViewHotel.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            animation.setDuration(500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        ((ViewHolder) holder).clearAnimation();
    }
}
