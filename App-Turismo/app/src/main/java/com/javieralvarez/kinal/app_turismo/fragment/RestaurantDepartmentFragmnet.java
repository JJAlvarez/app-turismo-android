package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.DepartamentoAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.model.Departamento;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.presenters.DepartmentRestaurantPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.DepartmentRestaurantPresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;
import com.javieralvarez.kinal.app_turismo.views.DepartmentRestaurantView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantDepartmentFragmnet extends Fragment implements DepartmentRestaurantView, TelefonoClickListener {


    @Bind(R.id.restaurant_rec_view)
    RecyclerView restaurantRecView;
    @Bind(R.id.swipeContainerRestaurante)
    SwipeRefreshLayout swipeContainerRestaurante;
    @Bind(R.id.progressBarRestaurantes)
    ProgressBar progressBarRestaurantes;
    @Bind(R.id.container_restaurante)
    RelativeLayout container;

    private RestaurantAdapter adapter;

    private Departamento mDepartamento;
    private DepartmentRestaurantPresenter presenter;

    public RestaurantDepartmentFragmnet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);
        if (getActivity().getIntent().hasExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG)) {
            mDepartamento = getActivity().getIntent().getParcelableExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG);
        }
        ButterKnife.bind(this, view);
        presenter = new DepartmentRestaurantPresenterImpl(this, getActivity());
        presenter.onCreate();
        setAdapter();
        setupRecycler();
        setSwipeToRefresh();
        getRestaurantes();
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerRestaurante.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRestaurantes();
            }
        });

        swipeContainerRestaurante.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setupRecycler() {
        restaurantRecView.setHasFixedSize(true);
    }

    private void setAdapter() {
        adapter = new RestaurantAdapter(getActivity(), getContext(), new ArrayList<Restaurante>(), this);
        restaurantRecView.setAdapter(adapter);
        restaurantRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showElements() {
        restaurantRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressBarRestaurantes.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        restaurantRecView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBarRestaurantes.setVisibility(View.GONE);
    }

    @Override
    public void getRestaurantes() {
        hideElements();
        showProgress();
        presenter.getRestaurantes(mDepartamento.getmIdDepartamento());
    }

    @Override
    public void onRestaurantesReady(List<Restaurante> restaurantes) {
        adapter.addLugares(restaurantes);
        hideProgress();
        showElements();
        if (swipeContainerRestaurante.isRefreshing()) {
            swipeContainerRestaurante.setRefreshing(false);
        }
    }

    @Override
    public void onError(String error) {
        Snackbar.make(container, getString(R.string.departamento_restaurante_error), Snackbar.LENGTH_LONG).show();
        if (swipeContainerRestaurante.isRefreshing()) {
            swipeContainerRestaurante.setRefreshing(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static RestaurantDepartmentFragmnet newInstance() {
        return new RestaurantDepartmentFragmnet();
    }

    @Override
    public void onTelefonoClick(int telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + String.valueOf(telefono)));
        startActivity(intent);
    }
}
