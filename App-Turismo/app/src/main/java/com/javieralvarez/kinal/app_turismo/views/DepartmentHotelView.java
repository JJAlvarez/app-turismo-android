package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

import java.util.List;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentHotelView {

    void showElements();
    void showProgress();
    void hideElements();
    void hideProgress();
    void getHoteles();
    void onHotelesReady(List<Hotel> hoteles);
    void onError(String error);
}
