package com.javieralvarez.kinal.app_turismo.fragment;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.LugarAdapter;
import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.presenters.UpdatePresenter;
import com.javieralvarez.kinal.app_turismo.presenters.UpdatePresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.UpdateView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class LugarTuristicoFragment extends Fragment implements UpdateView {

    @Bind(R.id.swipeContainerLugar)
    SwipeRefreshLayout swipeContainerLugar;
    @Bind(R.id.container_lugar)
    RelativeLayout containerLugar;
    @Bind(R.id.lugar_rec_view)
    RecyclerView lugarRecView;
    @Bind(R.id.progressBarLugares)
    ProgressBar progressBarLugares;

    private LugarAdapter adapter;

    private UpdatePresenter presenter;

    public LugarTuristicoFragment() {
        // Required empty public constructor
    }

    public static LugarTuristicoFragment newInstance() {
        LugarTuristicoFragment fragment = new LugarTuristicoFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lugar_turistico, container, false);
        ButterKnife.bind(this, view);
        presenter = new UpdatePresenterImpl(this, getActivity());
        presenter.onCreate();
        setSwipeToRefresh();
        setupRecycler();
        setupAdapter();
        showProgress();
        getUpdates();
        return view;
    }

    private void setupAdapter() {
        adapter = new LugarAdapter(getActivity(), getActivity(), new ArrayList<LugarTuristico>());
        lugarRecView.setAdapter(adapter);
        lugarRecView.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false));
    }

    private void setupRecycler() {
        lugarRecView.setHasFixedSize(true);
    }

    private void hideProgress() {
        lugarRecView.setVisibility(View.VISIBLE);
        progressBarLugares.setVisibility(View.GONE);
    }

    private void showProgress() {
        lugarRecView.setVisibility(View.GONE);
        progressBarLugares.setVisibility(View.VISIBLE);
    }

    private void setSwipeToRefresh() {
        swipeContainerLugar.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUpdates();
            }
        });

        swipeContainerLugar.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void getUpdates() {
        presenter.getUpdates(UpdateEvent.TIPO_UPDATE.UPDATE_LUGAR);
    }

    @Override
    public void onUpdate(List<Object> updates) {
        ArrayList<LugarTuristico> lugarUpdates = (ArrayList<LugarTuristico>) (Object) updates;
        adapter.updateAll(lugarUpdates);
        swipeContainerLugar.setRefreshing(false);
        if(progressBarLugares.getVisibility() == View.VISIBLE) {
            hideProgress();
        }
    }

    @Override
    public void onError() {
        Snackbar.make(containerLugar, getString(R.string.lugar_error_message), Snackbar.LENGTH_LONG).show();
        if (swipeContainerLugar.isRefreshing()) {
            swipeContainerLugar.setRefreshing(false);
        }
    }
}
