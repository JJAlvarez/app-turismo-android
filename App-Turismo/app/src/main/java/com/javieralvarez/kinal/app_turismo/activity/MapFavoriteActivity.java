package com.javieralvarez.kinal.app_turismo.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;
import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;
import com.javieralvarez.kinal.app_turismo.presenters.MapFavoritePresenter;
import com.javieralvarez.kinal.app_turismo.presenters.MapFavoritePresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.MapFavoriteView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapFavoriteActivity extends AppCompatActivity implements OnMapReadyCallback, MapFavoriteView {

    @Bind(R.id.toolbar_map)
    Toolbar toolbarMap;
    MapFragment supportMapFragment;
    @Bind(R.id.container_map)
    RelativeLayout containerMap;

    private GoogleMap mMap;

    private MapFavoritePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        presenter = new MapFavoritePresenterImpl(this);
        presenter.onCreate();
        supportMapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
        setSupportActionBar(toolbarMap);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap = googleMap;
    }

    @Override
    public void onRestaurantesReady(List<Restaurante> restaurantes) {
        mMap.clear();
        for(Restaurante restaurante: restaurantes) {
            for(SucursalRestaurante sucursal: restaurante.getmSucursales()) {
                LatLng RESTAURANTE = new LatLng(sucursal.getmLatitud(), sucursal.getmLongitud());
                mMap.addMarker(new MarkerOptions()
                        .position(RESTAURANTE)
                        .title(restaurante.getmNombre())
                        .snippet("Descripcion: " + restaurante.getmDescripcion()));
            }
        }
    }

    @Override
    public void onHotelesReady(List<Hotel> hoteles) {
        mMap.clear();
        for(Hotel hotel: hoteles) {
            for(SucursalHotel sucursal: hotel.getmSucursales()) {
                LatLng HOTEL = new LatLng(sucursal.getmLatitud(), sucursal.getmLongitud());
                mMap.addMarker(new MarkerOptions()
                        .position(HOTEL)
                        .title(hotel.getmNombre())
                        .snippet("Descripcion: " + hotel.getmDescripcion()));
            }
        }
    }

    @Override
    public void onLugaresReady(List<LugarTuristico> lugares) {
        mMap.clear();
        for(LugarTuristico lugar: lugares) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lugar.getmLatitud(), lugar.getmLongitud()))
                    .title(lugar.getmNombre())
                    .snippet("Descripcion: " + lugar.getmDescripcion()));
        }
    }

    @Override
    public void onError(String error) {
        Snackbar.make(containerMap, getString(R.string.mapa_error_message), Snackbar.LENGTH_LONG);
    }

    @Override
    public void getRestaurantes() {
        getSupportActionBar().setTitle(getString(R.string.mapa_title_restaurante));
        presenter.getRestaurantes();
    }

    @Override
    public void getHoteles() {
        getSupportActionBar().setTitle(getString(R.string.mapa_title_hoteles));
        presenter.getHoteles();
    }

    @Override
    public void getLugares() {
        getSupportActionBar().setTitle(getString(R.string.mapa_title_lugares));
        presenter.getLugares();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (supportMapFragment != null) {
            supportMapFragment.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (supportMapFragment != null) {
            supportMapFragment.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (supportMapFragment != null) {
            supportMapFragment.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_map_restaurantes) {
            getRestaurantes();
            return true;
        } else if (id == R.id.action_map_hoteles) {
            getHoteles();
            return true;
        } else if (id == R.id.action_map_lugares) {
            getLugares();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
