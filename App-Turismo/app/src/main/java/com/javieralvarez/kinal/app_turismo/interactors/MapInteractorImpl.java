package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.MapRepository;
import com.javieralvarez.kinal.app_turismo.repositories.MapRepositoryImpl;

/**
 * Created by Javier on 01/07/2016.
 */
public class MapInteractorImpl implements MapInteractor {

    private MapRepository repository;

    public MapInteractorImpl(Activity activity) {
        this.repository = new MapRepositoryImpl(activity);
    }

    @Override
    public void getRestaurantesLocations() {
        repository.getRestaurantesLocations();
    }

    @Override
    public void getHotelsLocations() {
        repository.getHotelsLocations();
    }

    @Override
    public void getLuaresLocation() {
        repository.getLuaresLocation();
    }
}
