package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.DeleteComentarioEvent;
import com.javieralvarez.kinal.app_turismo.interactors.ComentariosInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.ComentariosInteractorImpl;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.ComentariosView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 06/07/2016.
 */
public class DeleteComentarioPresenterImpl implements DeleteComentarioPresenter {

    private EventBus eventBus;
    private ComentariosView view;
    private ComentariosInteractor interactor;

    public DeleteComentarioPresenterImpl(ComentariosView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new ComentariosInteractorImpl(activity);
    }
    @Override
    public void deleteComentario(Comentarios comentarios) {
        interactor.deleteComentario(comentarios);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    @Subscribe
    public void onEventMainThread(DeleteComentarioEvent event) {
        if(view != null) {
            if(event.isSuccessful()) {
                view.deleteComentario();
            } else {
                view.onDeleteError();
            }
        }
    }
}
