package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.javieralvarez.kinal.app_turismo.db.TurismoDataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.ArrayList;

/**
 * Created by Javier on 27/04/2016.
 */
@Table(database = TurismoDataBase.class)
public class Hotel extends BaseModel implements Parcelable {

    @PrimaryKey
    @SerializedName("id_hotel")
    public int mIdHotel;

    @Column
    @SerializedName("nombre")
    public String mNombre;

    @Column
    @SerializedName("clasificacion")
    public int mClasificacion;

    @Column
    @SerializedName("pbx")
    public int mPbx;

    @Column
    @SerializedName("descripcion")
    public String mDescripcion;

    @Column
    @SerializedName("urlLogo")
    public String mUrlLogo;

    @Column
    @SerializedName("urlImagen")
    public String mUrlImagen;

    @Column
    @SerializedName("urlWeb")
    public String urlWeb;

    @Column
    public boolean favorite;

    @SerializedName("sucursalhotels")
    public ArrayList<SucursalHotel> mSucursales;

    public int getmPbx() {
        return mPbx;
    }

    public void setmPbx(int mPbx) {
        this.mPbx = mPbx;
    }

    public String getmUrlLogo() {
        return mUrlLogo;
    }

    public void setmUrlLogo(String mUrlLogo) {
        this.mUrlLogo = mUrlLogo;
    }

    public String getmUrlImagen() {
        return mUrlImagen;
    }

    public void setmUrlImagen(String mUrlImagen) {
        this.mUrlImagen = mUrlImagen;
    }

    public String getmDescripcion() {
        return mDescripcion;
    }

    public void setmDescripcion(String mDescripcion) {
        this.mDescripcion = mDescripcion;
    }

    public String getUrlWeb() {
        return urlWeb;
    }

    public void setUrlWeb(String urlWeb) {
        this.urlWeb = urlWeb;
    }

    public boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public ArrayList<SucursalHotel> getmSucursales() {
        return mSucursales;
    }

    public void setmSucursales(ArrayList<SucursalHotel> mSucursales) {
        this.mSucursales = mSucursales;
    }

    public Hotel() {

    }

    public int getmIdHotel() {
        return mIdHotel;
    }

    public void setmIdHotel(int mIdHotel) {
        this.mIdHotel = mIdHotel;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public int getmClasificacion() {
        return mClasificacion;
    }

    public void setmClasificacion(int mClasificacion) {
        this.mClasificacion = mClasificacion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIdHotel);
        dest.writeString(this.mNombre);
        dest.writeInt(this.mClasificacion);
        dest.writeInt(this.mPbx);
        dest.writeString(this.mUrlLogo);
        dest.writeString(this.mUrlImagen);
        dest.writeString(this.mDescripcion);
        dest.writeString(this.urlWeb);
        dest.writeByte((byte) (favorite ? 1 : 0));
        dest.writeTypedList(this.mSucursales);
    }

    protected Hotel(Parcel in) {
        this.mIdHotel = in.readInt();
        this.mNombre = in.readString();
        this.mClasificacion = in.readInt();
        this.mPbx = in.readInt();
        this.mUrlImagen = in.readString();
        this.mUrlLogo = in.readString();
        this.mDescripcion = in.readString();
        this.urlWeb = in.readString();
        this.favorite = in.readByte() != 0;
        this.mSucursales = in.createTypedArrayList(SucursalHotel.CREATOR);
    }

    public static final Creator<Hotel> CREATOR = new Creator<Hotel>() {
        @Override
        public Hotel createFromParcel(Parcel source) {
            return new Hotel(source);
        }

        @Override
        public Hotel[] newArray(int size) {
            return new Hotel[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        boolean equal = false;

        if (obj instanceof Hotel) {
            Hotel hotel = (Hotel) obj;
            equal = this.mIdHotel == hotel.getmIdHotel();
        }

        return equal;
    }

}
