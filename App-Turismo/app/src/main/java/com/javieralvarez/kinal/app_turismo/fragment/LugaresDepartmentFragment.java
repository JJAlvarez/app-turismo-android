package com.javieralvarez.kinal.app_turismo.fragment;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.DepartamentoAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.LugarAdapter;
import com.javieralvarez.kinal.app_turismo.model.Departamento;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.presenters.DepartmentLugarPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.DepartmentLugarPresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.DepartmentLugarView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class LugaresDepartmentFragment extends Fragment implements DepartmentLugarView {


    @Bind(R.id.lugar_rec_view)
    RecyclerView lugarRecView;
    @Bind(R.id.swipeContainerLugar)
    SwipeRefreshLayout swipeContainerLugar;
    @Bind(R.id.container_lugar)
    RelativeLayout container;
    @Bind(R.id.progressBarLugares)
    ProgressBar progressBarLugares;
    private DepartmentLugarPresenter presenter;

    private Departamento mDepartamento;
    private LugarAdapter lugaresAdapter;

    public LugaresDepartmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lugar_turistico, container, false);
        if (getActivity().getIntent().hasExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG)) {
            mDepartamento = getActivity().getIntent().getParcelableExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG);
        }
        ButterKnife.bind(this, view);
        presenter = new DepartmentLugarPresenterImpl(this, getActivity());
        presenter.onCreate();
        setAdapter();
        setupRecycler();
        setSwipeToRefresh();
        getLugares();
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerLugar.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLugares();
            }
        });

        swipeContainerLugar.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setAdapter() {
        lugaresAdapter = new LugarAdapter(getActivity(), getContext(), new ArrayList<LugarTuristico>());
        lugarRecView.setAdapter(lugaresAdapter);
        lugarRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    private void setupRecycler() {
        lugarRecView.setHasFixedSize(true);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showElements() {
        lugarRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressBarLugares.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        lugarRecView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBarLugares.setVisibility(View.GONE);
    }

    @Override
    public void getLugares() {
        hideElements();
        showProgress();
        presenter.getLugares(mDepartamento.getmIdDepartamento());
    }

    @Override
    public void onLugaresReady(List<LugarTuristico> lugares) {
        lugaresAdapter.addLugares(lugares);
        hideProgress();
        showElements();
        if (swipeContainerLugar.isRefreshing()) {
            swipeContainerLugar.setRefreshing(false);
        }
    }

    @Override
    public void onError(String error) {
        Snackbar.make(container, getString(R.string.departamento_lugar_error), Snackbar.LENGTH_LONG).show();
        if (swipeContainerLugar.isRefreshing()) {
            swipeContainerLugar.setRefreshing(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static LugaresDepartmentFragment newInstance() {
        return new LugaresDepartmentFragment();
    }
}
