package com.javieralvarez.kinal.app_turismo.repositories;

/**
 * Created by Javier on 07/07/2016.
 */
public interface SucursalesHotelRepository {

    void getSucursales(int id);
}
