package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationHotelEvent {

    private String error;
    private Hotel hotel;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
