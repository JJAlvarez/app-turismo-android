package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;

import java.util.List;

/**
 * Created by Javier on 07/07/2016.
 */
public class SucursalHotelEvent {

    private String error;
    private List<SucursalHotel> hotels;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<SucursalHotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<SucursalHotel> hotels) {
        this.hotels = hotels;
    }
}
