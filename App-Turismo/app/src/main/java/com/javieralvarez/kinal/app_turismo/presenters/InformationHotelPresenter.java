package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.InformationHotelEvent;

/**
 * Created by Javier on 14/07/2016.
 */
public interface InformationHotelPresenter {

    void onCreate();
    void onDestroy();

    void getFavoriteHotel(int id);
    void getAPIHotel(int id);

    void onEventMainThread(InformationHotelEvent event);
}
