package com.javieralvarez.kinal.app_turismo.repositories;

/**
 * Created by Javier on 14/07/2016.
 */
public interface InformationHotelRepository {

    void getFavoriteHotel(int id);
    void getAPIHotel(int id);
}
