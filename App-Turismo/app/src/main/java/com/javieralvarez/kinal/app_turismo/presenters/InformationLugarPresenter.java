package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.InformationLugarEvent;

/**
 * Created by Javier on 14/07/2016.
 */
public interface InformationLugarPresenter {

    void onCreate();
    void onDestroy();

    void onEventMainThread(InformationLugarEvent event);
    void getLugar(int id);
}
