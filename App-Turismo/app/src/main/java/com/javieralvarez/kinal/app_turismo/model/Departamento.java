package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.javieralvarez.kinal.app_turismo.db.TurismoDataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Javier on 27/04/2016.
 */
@Table(database = TurismoDataBase.class)
public class Departamento extends BaseModel implements Parcelable{

    @PrimaryKey
    @SerializedName("id_departamento")
    public int mIdDepartamento;

    @Column
    @SerializedName("nombre")
    public String mNombre;

    @Column
    @SerializedName("descripcion")
    public String mDescripcion;

    @Column
    @SerializedName("urlImagen")
    public String mUrlImagen;

    public Departamento () {

    }

    public int getmIdDepartamento() {
        return mIdDepartamento;
    }

    public void setmIdDepartamento(int mIdDepartamento) {
        this.mIdDepartamento = mIdDepartamento;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public String getmDescripcion() {
        return mDescripcion;
    }

    public void setmDescripcion(String mDescripcion) {
        this.mDescripcion = mDescripcion;
    }

    public String getmUrlImagen() {
        return mUrlImagen;
    }

    public void setmUrlImagen(String mUrlImagen) {
        this.mUrlImagen = mUrlImagen;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIdDepartamento);
        dest.writeString(this.mNombre);
        dest.writeString(this.mDescripcion);
        dest.writeString(this.mUrlImagen);
    }

    protected Departamento(Parcel in) {
        this.mIdDepartamento = in.readInt();
        this.mNombre = in.readString();
        this.mDescripcion = in.readString();
        this.mUrlImagen = in.readString();
    }

    public static final Parcelable.Creator<Departamento> CREATOR = new Parcelable.Creator<Departamento>() {
        @Override
        public Departamento createFromParcel(Parcel source) {
            return new Departamento(source);
        }

        @Override
        public Departamento[] newArray(int size) {
            return new Departamento[size];
        }
    };
}
