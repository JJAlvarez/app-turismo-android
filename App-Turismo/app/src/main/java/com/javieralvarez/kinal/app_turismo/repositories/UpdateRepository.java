package com.javieralvarez.kinal.app_turismo.repositories;

import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;

/**
 * Created by Javier on 30/06/2016.
 */
public interface UpdateRepository {

    void getUpdates(UpdateEvent.TIPO_UPDATE tipo_update);
}
