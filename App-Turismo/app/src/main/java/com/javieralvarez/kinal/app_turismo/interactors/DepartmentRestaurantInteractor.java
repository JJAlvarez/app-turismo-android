package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentRestaurantInteractor {

    void execute(int id);
}
