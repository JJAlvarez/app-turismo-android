package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Comentarios;

/**
 * Created by Javier on 28/06/2016.
 */
public interface AddComentarioView {

    void onComentarioAdded(Comentarios comentario);
    void onComentarioAddedError(String error);
}
