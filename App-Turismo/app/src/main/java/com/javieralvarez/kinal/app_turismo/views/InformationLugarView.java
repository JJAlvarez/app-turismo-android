package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;

/**
 * Created by Javier on 14/07/2016.
 */
public interface InformationLugarView
{

    void getLugar(int id);
    void onError(String error);
    void onLugarReady(LugarTuristico lugarTuristico);

    void hideElements();
    void showElements();
    void hideProgress();
    void showProgress();
}
