package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesHotelsView {

    void getFavoritesHotels();
    void onFavoritesReady(List<Hotel> hotels);

    void showElements();
    void hideElements();
    void showProgress();
    void hideProgress();
}
