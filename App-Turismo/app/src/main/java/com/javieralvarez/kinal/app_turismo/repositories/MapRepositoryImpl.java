package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.MapEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 01/07/2016.
 */
public class MapRepositoryImpl implements MapRepository {

    private EventBus eventBus;
    private TurismoApp app;
    private MapEvent event;

    public MapRepositoryImpl(Activity activity) {
        PreferencesUtil.init(activity);
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
        this.event = new MapEvent();
    }

    @Override
    public void getRestaurantesLocations() {
        app.getApi().getAllRestaurantes(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""))
                .enqueue(new Callback<List<Restaurante>>() {
                    @Override
                    public void onResponse(Call<List<Restaurante>> call, Response<List<Restaurante>> response) {
                        post(MapEvent.TIPO_MAPA.MAPA_RESTAURANTE, response.body(), null, null);
                    }

                    @Override
                    public void onFailure(Call<List<Restaurante>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    @Override
    public void getHotelsLocations() {
        app.getApi().getAllHotels(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""))
                .enqueue(new Callback<List<Hotel>>() {
                    @Override
                    public void onResponse(Call<List<Hotel>> call, Response<List<Hotel>> response) {
                        post(MapEvent.TIPO_MAPA.MAPA_HOTEL, null, response.body(), null);
                    }

                    @Override
                    public void onFailure(Call<List<Hotel>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    @Override
    public void getLuaresLocation() {
        app.getApi().getAllLugares(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""))
                .enqueue(new Callback<List<LugarTuristico>>() {
                    @Override
                    public void onResponse(Call<List<LugarTuristico>> call, Response<List<LugarTuristico>> response) {
                        post(MapEvent.TIPO_MAPA.MAPA_LUGAR, null, null, response.body());
                    }

                    @Override
                    public void onFailure(Call<List<LugarTuristico>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    private void postError(String msg) {
        event.setError(msg);
        eventBus.post(event);
    }

    private void post(MapEvent.TIPO_MAPA tipo_mapa, List<Restaurante> restaurantes, List<Hotel> hotels
            , List<LugarTuristico> lugarTuristicos) {
        event.setTipo_mapa(tipo_mapa);
        event.setHotels(hotels);
        event.setRestaurantes(restaurantes);
        event.setLugarTuristicos(lugarTuristicos);
        event.setError(null);
        eventBus.post(event);
    }
}
