package com.javieralvarez.kinal.app_turismo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.fragment.InformationHotelFragment;
import com.javieralvarez.kinal.app_turismo.fragment.SucursalesHotelFragment;

/**
 * Created by Javier on 27/05/2016.
 */
public class FragmentHotelAdapter extends FragmentPagerAdapter {

    private int PAGE_COUNT = 3;

    private String tabTitles[] =
            new String[] { "Informacion", "Sucursales", "Comentarios"};

    public FragmentHotelAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = InformationHotelFragment.newInstance(InformationHotelFragment.TIPO_INFORMACION.TIPO_API);
                break;
            case 1:
                fragment = SucursalesHotelFragment.newInstance();
                break;
            case 2:
                fragment = ComentariosFragment.newInstance(ComentariosFragment.TIPO_COMENTARIO.HOTEL);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
