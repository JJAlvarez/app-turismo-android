package com.javieralvarez.kinal.app_turismo.views;

import java.util.List;

/**
 * Created by Javier on 30/06/2016.
 */
public interface UpdateView {

    void getUpdates();
    void onUpdate(List<Object> updates);

    void onError();
}
