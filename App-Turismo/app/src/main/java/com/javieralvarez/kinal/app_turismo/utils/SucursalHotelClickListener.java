package com.javieralvarez.kinal.app_turismo.utils;

import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;

/**
 * Created by Javier on 07/07/2016.
 */
public interface SucursalHotelClickListener {

    void onClick(SucursalHotel sucursalHotel);
}
