package com.javieralvarez.kinal.app_turismo.events;

/**
 * Created by Javier on 06/07/2016.
 */
public class DeleteComentarioEvent {

    private String error;
    private boolean successful;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}
