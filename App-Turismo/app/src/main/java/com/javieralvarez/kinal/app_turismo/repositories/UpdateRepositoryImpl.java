package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 30/06/2016.
 */
public class UpdateRepositoryImpl implements UpdateRepository {

    private EventBus eventBus;
    private TurismoApp app;
    private UpdateEvent.TIPO_UPDATE tipo_update;

    public UpdateRepositoryImpl(Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
        PreferencesUtil.init(activity);
    }

    @Override
    public void getUpdates(UpdateEvent.TIPO_UPDATE tipo_update) {
        this.tipo_update = tipo_update;
        switch (tipo_update) {
            case UPDATE_RESTAURANTE:
                getRestaurantersUpdate();
                break;
            case UPDATE_HOTEL:
                getHotelesUpdate();
                break;
            case UPDATE_LUGAR:
                getLugaresUpdate();
                break;
        }
    }

    private void postError(String error) {
        UpdateEvent event = new UpdateEvent();
        event.setError(error);
        eventBus.post(event);
    }

    private void post(List<Object> response) {
        UpdateEvent event = new UpdateEvent();
        event.setError(null);
        event.setUpdates(response);
        event.setTipo(this.tipo_update);
        eventBus.post(event);
    }

    private void getRestaurantersUpdate() {
        app.getApi().getAllRestaurantes(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""))
                .enqueue(new Callback<List<Restaurante>>() {
                    @Override
                    public void onResponse(Call<List<Restaurante>> call, Response<List<Restaurante>> response) {
                        post((List<Object>)(Object)response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Restaurante>> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }

    private void getHotelesUpdate() {
        app.getApi().getAllHotels(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""))
                .enqueue(new Callback<List<Hotel>>() {
                    @Override
                    public void onResponse(Call<List<Hotel>> call, Response<List<Hotel>> response) {
                        post((List<Object>)(Object)response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Hotel>> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }

    private void getLugaresUpdate() {
        app.getApi().getAllLugares(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""))
                .enqueue(new Callback<List<LugarTuristico>>() {
                    @Override
                    public void onResponse(Call<List<LugarTuristico>> call, Response<List<LugarTuristico>> response) {
                        post((List<Object>)(Object)response.body());
                    }

                    @Override
                    public void onFailure(Call<List<LugarTuristico>> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }
}
