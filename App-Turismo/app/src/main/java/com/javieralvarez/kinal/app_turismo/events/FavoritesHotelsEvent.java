package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesHotelsEvent {

    private String error;
    private List<Hotel> hotels;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(List<Hotel> hotels) {
        this.hotels = hotels;
    }
}
