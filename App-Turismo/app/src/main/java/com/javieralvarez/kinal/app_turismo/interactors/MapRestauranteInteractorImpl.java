package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.MapRestauranteRepository;
import com.javieralvarez.kinal.app_turismo.repositories.MapRestauranteRepositoryImpl;

/**
 * Created by Javier on 20/07/2016.
 */
public class MapRestauranteInteractorImpl implements MapRestauranteInteractor {

    private MapRestauranteRepository repository;

    public MapRestauranteInteractorImpl(Activity activity) {
        this.repository = new MapRestauranteRepositoryImpl(activity);
    }

    @Override
    public void execute(int idRestaurante) {
        repository.execute(idRestaurante);
    }
}
