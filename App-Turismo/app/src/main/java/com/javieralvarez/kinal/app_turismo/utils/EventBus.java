package com.javieralvarez.kinal.app_turismo.utils;

/**
 * Created by Javier on 11/06/2016.
 */
public interface EventBus {

    void register(Object suscriber);
    void unregister(Object suscriber);
    void post(Object event);
}
