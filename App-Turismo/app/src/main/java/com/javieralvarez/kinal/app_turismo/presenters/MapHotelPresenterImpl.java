package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.MapHotelEvent;
import com.javieralvarez.kinal.app_turismo.interactors.MapHotelInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.MapHotelInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.MapHotelView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 20/07/2016.
 */
public class MapHotelPresenterImpl implements MapHotelPresenter {

    private EventBus eventBus;
    private MapHotelView view;
    private MapHotelInteractor interactor;

    public MapHotelPresenterImpl(MapHotelView view, Activity acticity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.view = view;
        this.interactor = new MapHotelInteractorImpl(acticity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void getSucursales(int idHotel) {
        interactor.execute(idHotel);
    }

    @Override
    @Subscribe
    public void onEventMainThread(MapHotelEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onSucursalesReady(event.getHotel());
            } else {
                view.onError(event.getError());
            }
        }
    }
}
