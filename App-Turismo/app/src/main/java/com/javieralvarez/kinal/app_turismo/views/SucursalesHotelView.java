package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;

import java.util.List;

/**
 * Created by Javier on 07/07/2016.
 */
public interface SucursalesHotelView {

    void showElements();
    void showProgress();
    void hideElements();
    void hideProgress();

    void getSucursales(int id);

    void onSucursalesReady(List<SucursalHotel> sucursalHotelList);
    void onError(String error);
}
