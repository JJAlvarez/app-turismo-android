package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.InformationRestauranteEvent;
import com.javieralvarez.kinal.app_turismo.interactors.InformationRestauranteInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.InformationRestauranteInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.InformationRestauranteView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 15/07/2016.
 */
public class InformationRestaurantePresenterImpl implements InformationRestaurantePresenter {

    private EventBus eventBus;
    private InformationRestauranteView view;
    private InformationRestauranteInteractor interactor;

    public InformationRestaurantePresenterImpl(InformationRestauranteView view, Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.view = view;
        this.interactor = new InformationRestauranteInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        this.eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        this.eventBus.unregister(this);
        if(view != null) {
            view = null;
        }
    }

    @Override
    public void getRestaurante(int id) {
        interactor.execute(id);
    }

    @Override
    @Subscribe
    public void onEventMainThread(InformationRestauranteEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onRestauranteReady(event.getRestaurante());
            } else {
                view.onError(event.getError());
            }
        }
    }
}
