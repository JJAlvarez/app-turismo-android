package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.ComentarioAddEvent;
import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.interactors.AddComentarioInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.AddComentarioInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.AddComentarioView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 28/06/2016.
 */
public class AddComentarioPresenterImpl implements AddComenatarioPresenter {

    private AddComentarioView view;
    private EventBus eventBus;
    private AddComentarioInteractor interactor;

    public AddComentarioPresenterImpl(AddComentarioView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new AddComentarioInteractorImpl(activity);
    }

    @Override
    public void onResume() {
        eventBus.register(this);
    }

    @Override
    public void onPause() {
        eventBus.unregister(this);
    }

    @Override
    public void onDestroy() {
        if(view != null) {
            view = null;
        }
    }

    @Override
    public void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario) {
        interactor.execute(tipo, id, comentario);
    }

    @Override
    @Subscribe
    public void onEventMainThread(ComentarioAddEvent event) {
        if(event.getError() == null) {
            view.onComentarioAdded(event.getComentarios());
        } else {
            view.onComentarioAddedError(event.getError());
        }
    }
}
