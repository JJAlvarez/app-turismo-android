package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.MapRestauranteEvent;
import com.javieralvarez.kinal.app_turismo.interactors.MapRestauranteInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.MapRestauranteInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.MapRestauranteView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 20/07/2016.
 */
public class MapRestaurantePresenterImpl implements MapRestaurantePresenter {

    private EventBus eventBus;
    private MapRestauranteView view;
    private MapRestauranteInteractor interactor;

    public MapRestaurantePresenterImpl(MapRestauranteView view, Activity acticity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.view = view;
        this.interactor = new MapRestauranteInteractorImpl(acticity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
        if (view != null) {
            view = null;
        }
    }

    @Override
    public void getSucursales(int idHotel) {
        interactor.execute(idHotel);
    }

    @Override
    @Subscribe
    public void onEventMainThread(MapRestauranteEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                view.onSucursalesReady(event.getRestaurante());
            } else {
                view.onError(event.getError());
            }
        }
    }
}
