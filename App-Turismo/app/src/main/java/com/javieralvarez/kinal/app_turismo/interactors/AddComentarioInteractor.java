package com.javieralvarez.kinal.app_turismo.interactors;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;

/**
 * Created by Javier on 28/06/2016.
 */
public interface AddComentarioInteractor {

    void execute(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario);
}
