package com.javieralvarez.kinal.app_turismo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.javieralvarez.kinal.app_turismo.fragment.FavoritesHotelFragment;
import com.javieralvarez.kinal.app_turismo.fragment.FavoritesLugarFragment;
import com.javieralvarez.kinal.app_turismo.fragment.FavoritesRestaurantFragment;

/**
 * Created by Javier on 04/07/2016.
 */
public class FavoritesFragmentAdapter extends FragmentStatePagerAdapter {

    private int PAGE_COUNT = 3;

    private String tabTitles[] =
            new String[] { "Lugares", "Hoteles", "Restaurantes"};

    public FavoritesFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = null;

        switch(position) {
            case 0:
                f = FavoritesLugarFragment.newInstance();
                break;
            case 1:
                f = FavoritesHotelFragment.newInstance();
                break;
            case 2:
                f = FavoritesRestaurantFragment.newInstance();
                break;
        }

        return f;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
