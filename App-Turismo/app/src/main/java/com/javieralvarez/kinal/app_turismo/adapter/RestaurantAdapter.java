package com.javieralvarez.kinal.app_turismo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesRestauranteActivity;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Javier on 16/05/2016.
 */
public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.RestauranteViewHolder> {

    public static final String RESTAURANTE_ID_TAG = "restauranteID";
    public static String RESTAURANTE_DETALLES_TAG = "detalles_restaurante";
    private int lastPosition = -1;

    private Context mContext;
    private Activity mActivity;
    private ArrayList<Restaurante> mData;
    private TelefonoClickListener listener;

    public RestaurantAdapter(Activity activity, Context context, ArrayList<Restaurante> data, TelefonoClickListener listener) {
        this.mContext = context;
        this.mActivity = activity;
        this.mData = data;
        this.listener = listener;
    }

    @Override
    public RestauranteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_restaurante, parent, false);

        RestauranteViewHolder rvh = new RestauranteViewHolder(itemView);

        return rvh;
    }

    @Override
    public void onBindViewHolder(RestauranteViewHolder holder, int position) {
        Restaurante item = mData.get(position);
        setAnimation(holder.mView, position);
        holder.bindRestaurante(item);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void updateAll(ArrayList<Restaurante> restaurantesUpdates) {
        mData.clear();
        mData.addAll(restaurantesUpdates);
        notifyDataSetChanged();
    }

    public void addLugares(List<Restaurante> restaurantes) {
        mData.clear();
        mData.addAll(restaurantes);
        notifyDataSetChanged();
    }

    public class RestauranteViewHolder extends RecyclerView.ViewHolder {

        private ImageView mLogo;
        private TextView mLblNombre;
        private TextView mPbx;
        private CardView mView;
        @Bind(R.id.btn_call_restaurant)
        CircleImageView btnCallRestaurant;

        public RestauranteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mLblNombre = (TextView) itemView.findViewById(R.id.lbl_name);
            mLogo = (ImageView) itemView.findViewById(R.id.img_restaurant);
            mPbx = (TextView) itemView.findViewById(R.id.lbl_phone);
            mView = (CardView) itemView.findViewById(R.id.card_view_restaurante);
        }

        public void bindRestaurante(final Restaurante restaurante) {
            mLblNombre.setText(restaurante.getmNombre());
            mPbx.setText(String.valueOf(restaurante.getmPbx()));
            Glide.with(mContext).load(restaurante.getmUrlLogo()).into(mLogo);
            this.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, DetallesRestauranteActivity.class);
                    intent.putExtra(RESTAURANTE_DETALLES_TAG, restaurante);
                    intent.putExtra(RESTAURANTE_ID_TAG, restaurante.getmIdRestaurante());
                    mContext.startActivity(intent);
                }
            });
            this.btnCallRestaurant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onTelefonoClick(restaurante.getmPbx());
                }
            });
        }

        public void clearAnimation()
        {
            mView.clearAnimation();
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            animation.setDuration(500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(RestauranteViewHolder holder) {
        ((RestauranteViewHolder)holder).clearAnimation();
    }
}
