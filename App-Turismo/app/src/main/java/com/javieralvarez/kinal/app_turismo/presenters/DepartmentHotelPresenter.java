package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.DepartmentHotelEvent;

/**
 * Created by Javier on 02/07/2016.
 */
public interface DepartmentHotelPresenter {

    void onCreate();
    void onDestroy();

    void getHotels(int id);
    void onEventMainThread(DepartmentHotelEvent event);
}
