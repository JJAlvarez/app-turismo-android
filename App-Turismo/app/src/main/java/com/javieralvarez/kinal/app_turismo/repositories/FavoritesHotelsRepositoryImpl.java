package com.javieralvarez.kinal.app_turismo.repositories;

import com.javieralvarez.kinal.app_turismo.events.FavoritesHotelsEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.raizlabs.android.dbflow.list.FlowCursorList;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesHotelsRepositoryImpl implements FavoritesHotelsRepository {

    private EventBus eventBus;
    private FavoritesHotelsEvent event;

    public FavoritesHotelsRepositoryImpl() {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.event = new FavoritesHotelsEvent();
    }
    @Override
    public void getHoteles() {
        FlowCursorList<Hotel> storedRecipes = new FlowCursorList<Hotel>(false, Hotel.class);
        post(storedRecipes.getAll());
    }

    private void post(List<Hotel> hotelList) {
        event.setError(null);
        event.setHotels(hotelList);
        eventBus.post(event);
    }
}
