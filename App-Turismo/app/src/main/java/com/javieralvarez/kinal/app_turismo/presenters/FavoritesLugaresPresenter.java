package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.FavoritesLugaresEvent;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesLugaresPresenter {

    void onCreate();
    void onDestroy();

    void getPlaces();
    void onEventMainThread(FavoritesLugaresEvent event);
}
