package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesRestaurantsInteractor {

    void execute();
}
