package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Comentarios;

/**
 * Created by Javier on 28/06/2016.
 */
public class ComentarioAddEvent {

    private String error;
    private Comentarios comentarios;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Comentarios getComentarios() {
        return comentarios;
    }

    public void setComentarios(Comentarios comentarios) {
        this.comentarios = comentarios;
    }
}
