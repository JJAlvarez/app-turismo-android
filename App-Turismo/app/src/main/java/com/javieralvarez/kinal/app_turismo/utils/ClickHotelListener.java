package com.javieralvarez.kinal.app_turismo.utils;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

/**
 * Created by Javier on 04/07/2016.
 */
public interface ClickHotelListener {

    void onClick(Hotel hotel);
}
