package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Hotel;

/**
 * Created by Javier on 14/07/2016.
 */
public interface InformationHotelView {

    void getAPIHotel(int id);
    void getFavoriteHotel(int id);

    void onHotelReady(Hotel hotel);
    void onError(String error);

    void hideElements();
    void showElements();
    void hideProgress();
    void showProgress();
}
