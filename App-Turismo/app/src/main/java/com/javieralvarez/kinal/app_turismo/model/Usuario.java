package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 22/05/2016.
 */
public class Usuario implements Parcelable {

    @SerializedName("id_usuario")
    public int mIdUsuario;

    @SerializedName("nombre")
    private String mNombre;

    @SerializedName("telefono")
    private int mTelefono;

    @SerializedName("correo")
    public String mCorreo;

    @SerializedName("nick")
    private String mNick;

    @SerializedName("contrasena")
    private String mPassword;

    @SerializedName("direccion")
    private String mDireccion;

    @SerializedName("message")
    private String mMessage;

    public int getmIdUsuario() {
        return mIdUsuario;
    }

    public void setmIdUsuario(int mIdUsuario) {
        this.mIdUsuario = mIdUsuario;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public int getmTelefono() {
        return mTelefono;
    }

    public void setmTelefono(int mTelefono) {
        this.mTelefono = mTelefono;
    }

    public String getmCorreo() {
        return mCorreo;
    }

    public void setmCorreo(String mCorreo) {
        this.mCorreo = mCorreo;
    }

    public String getmNick() {
        return mNick;
    }

    public void setmNick(String mNick) {
        this.mNick = mNick;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getmDireccion() {
        return mDireccion;
    }

    public void setmDireccion(String mDireccion) {
        this.mDireccion = mDireccion;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIdUsuario);
        dest.writeString(this.mNombre);
        dest.writeString(this.mCorreo);
        dest.writeInt(this.mTelefono);
        dest.writeString(this.mNick);
        dest.writeString(this.mPassword);
        dest.writeString(this.mDireccion);
        dest.writeString(this.mMessage);
    }

    protected Usuario(Parcel in) {
        this.mIdUsuario = in.readInt();
        this.mNombre = in.readString();
        this.mCorreo = in.readString();
        this.mNick = in.readString();
        this.mTelefono = in.readInt();
        this.mPassword = in.readString();
        this.mDireccion = in.readString();
        this.mMessage = in.readString();
    }

    public static final Parcelable.Creator<Usuario> CREATOR = new Parcelable.Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel source) {
            return new Usuario(source);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };
}
