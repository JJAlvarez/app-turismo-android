package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.MapEvent;
import com.javieralvarez.kinal.app_turismo.interactors.MapInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.MapInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.MapView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 01/07/2016.
 */
public class MapPresenterImpl implements MapPresenter {

    private EventBus eventBus;
    private MapInteractor interactor;
    private MapView view;

    public MapPresenterImpl(MapView view, Activity activity) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new MapInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
        if(view != null) {
            view = null;
        }
    }

    @Override
    public void getRestaurantesLocations() {
        interactor.getRestaurantesLocations();
    }

    @Override
    public void getHotelsLocations() {
        interactor.getHotelsLocations();
    }

    @Override
    public void getLuaresLocation() {
        interactor.getLuaresLocation();
    }

    @Override
    @Subscribe
    public void onEventMainThread(MapEvent event) {
        if(event.getError() == null) {
            switch (event.getTipo_mapa()) {
                case MAPA_RESTAURANTE:
                    view.onRestaurantesReady(event.getRestaurantes());
                    break;
                case MAPA_HOTEL:
                    view.onHotelsReady(event.getHotels());
                    break;
                case MAPA_LUGAR:
                    view.onLugaresReady(event.getLugarTuristicos());
                    break;
            }
        } else {
            view.onError(event.getError());
        }
    }
}
