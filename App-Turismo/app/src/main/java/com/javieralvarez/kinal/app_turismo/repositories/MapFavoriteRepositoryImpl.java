package com.javieralvarez.kinal.app_turismo.repositories;

import com.javieralvarez.kinal.app_turismo.events.MapEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.raizlabs.android.dbflow.list.FlowCursorList;

import java.util.List;

/**
 * Created by Javier on 13/07/2016.
 */
public class MapFavoriteRepositoryImpl implements MapFavoriteRepository {

    private EventBus eventBus;

    public MapFavoriteRepositoryImpl() {
        this.eventBus = GreenRobotEventBus.getInstance();
    }

    @Override
    public void getRestaurantes() {
        FlowCursorList<Restaurante> storedRecipes = new FlowCursorList<Restaurante>(false, Restaurante.class);
        post(MapEvent.TIPO_MAPA.MAPA_RESTAURANTE, storedRecipes.getAll(), null, null);
    }

    @Override
    public void getHoteles() {
        FlowCursorList<Hotel> storedRecipes = new FlowCursorList<Hotel>(false, Hotel.class);
        post(MapEvent.TIPO_MAPA.MAPA_HOTEL, null, storedRecipes.getAll(), null);
    }

    @Override
    public void getLugares() {
        FlowCursorList<LugarTuristico> storedRecipes = new FlowCursorList<LugarTuristico>(false, LugarTuristico.class);
        post(MapEvent.TIPO_MAPA.MAPA_LUGAR, null, null, storedRecipes.getAll());
    }

    private void post(MapEvent.TIPO_MAPA tipo_mapa, List<Restaurante> restaurantes, List<Hotel> hotels
            , List<LugarTuristico> lugarTuristicos) {
        MapEvent event = new MapEvent();
        event.setTipo_mapa(tipo_mapa);
        event.setHotels(hotels);
        event.setRestaurantes(restaurantes);
        event.setLugarTuristicos(lugarTuristicos);
        event.setError(null);
        eventBus.post(event);
    }
}
