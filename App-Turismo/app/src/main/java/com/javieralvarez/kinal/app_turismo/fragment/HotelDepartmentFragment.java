package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.DepartamentoAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.model.Departamento;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.presenters.DepartmentHotelPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.DepartmentHotelPresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;
import com.javieralvarez.kinal.app_turismo.views.DepartmentHotelView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HotelDepartmentFragment extends Fragment implements DepartmentHotelView, TelefonoClickListener {


    @Bind(R.id.hotel_rec_view)
    RecyclerView hotelRecView;
    @Bind(R.id.swipeContainerHotel)
    SwipeRefreshLayout swipeContainerHotel;
    @Bind(R.id.progressBarHoteles)
    ProgressBar progressBarHoteles;
    @Bind(R.id.container_hotel)
    RelativeLayout container;

    private HotelAdapter adapter;

    private Departamento mDepartamento;
    private DepartmentHotelPresenter presenter;

    public HotelDepartmentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel, container, false);
        if (getActivity().getIntent().hasExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG)) {
            mDepartamento = getActivity().getIntent().getParcelableExtra(DepartamentoAdapter.DEPARTAMENTO_DETALLES_TAG);
        }
        ButterKnife.bind(this, view);
        presenter = new DepartmentHotelPresenterImpl(this, getActivity());
        presenter.onCreate();
        setAdapter();
        setupRecycler();
        setSwipeToRefresh();
        getHoteles();
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerHotel.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHoteles();
            }
        });

        swipeContainerHotel.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setupRecycler() {
        hotelRecView.setHasFixedSize(true);
    }

    private void setAdapter() {
        adapter = new HotelAdapter(getContext(), new ArrayList<Hotel>(), this);
        hotelRecView.setAdapter(adapter);
        hotelRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showElements() {
        hotelRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressBarHoteles.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        hotelRecView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBarHoteles.setVisibility(View.GONE);
    }

    @Override
    public void getHoteles() {
        hideElements();
        showProgress();
        presenter.getHotels(mDepartamento.getmIdDepartamento());
    }

    @Override
    public void onHotelesReady(List<Hotel> hoteles) {
        adapter.updateAll(hoteles);
        hideProgress();
        showElements();
        if (swipeContainerHotel.isRefreshing()) {
            swipeContainerHotel.setRefreshing(false);
        }
    }

    @Override
    public void onError(String error) {
        Snackbar.make(container, getString(R.string.departamento_hotel_error), Snackbar.LENGTH_LONG).show();
        if (swipeContainerHotel.isRefreshing()) {
            swipeContainerHotel.setRefreshing(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static HotelDepartmentFragment newInstance() {
        return new HotelDepartmentFragment();
    }

    @Override
    public void onTelefonoClick(int telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + String.valueOf(telefono)));
        startActivity(intent);
    }
}
