package com.javieralvarez.kinal.app_turismo.fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.SendButton;
import com.facebook.share.widget.ShareButton;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesRestauranteActivity;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.presenters.InformationRestaurantePresenter;
import com.javieralvarez.kinal.app_turismo.presenters.InformationRestaurantePresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.InformationRestauranteView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class InformationRestauranteFragment extends Fragment implements InformationRestauranteView {

    @Bind(R.id.img_inf_restaurante)
    ImageView imgInfRestaurante;
    @Bind(R.id.txt_inf_restaurante_descripcion)
    TextView txtInfRestauranteDescripcion;
    @Bind(R.id.fbShare)
    ShareButton fbShare;
    @Bind(R.id.fbSend)
    SendButton fbSend;
    @Bind(R.id.elements_information_restaurante)
    RelativeLayout elementsInformationRestaurante;
    @Bind(R.id.progressInformationRestaurante)
    ProgressBar progressInformationRestaurante;
    @Bind(R.id.container)
    NestedScrollView container;

    private Restaurante mRestaurante;
    private int idRestaurante;

    private InformationRestaurantePresenter presenter;

    public InformationRestauranteFragment() {
        // Required empty public constructor
    }

    public static InformationRestauranteFragment newInstance() {
        InformationRestauranteFragment fragment = new InformationRestauranteFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_information_restaurante, container, false);
        ButterKnife.bind(this, view);
        presenter = new InformationRestaurantePresenterImpl(this, getActivity());
        presenter.onCreate();
        if (getActivity().getIntent().hasExtra(RestaurantAdapter.RESTAURANTE_ID_TAG)) {
            idRestaurante = getActivity().getIntent().getIntExtra(RestaurantAdapter.RESTAURANTE_ID_TAG, 0);
        }
        getRestaurante(idRestaurante);
        return view;
    }

    private void setContentFacebook() {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(mRestaurante.getUrlWeb()))
                .build();
        fbShare.setShareContent(content);
        fbSend.setShareContent(content);
    }

    @Override
    public void getRestaurante(int id) {
        presenter.getRestaurante(id);
        hideElements();
        showProgress();
    }

    @Override
    public void onRestauranteReady(Restaurante restaurante) {
        setupRestaurante(restaurante);
    }

    private void setupRestaurante(Restaurante restaurante) {
        this.mRestaurante = restaurante;
        setTitleRestaurante();
        txtInfRestauranteDescripcion.setText(restaurante.getmDescripcion());
        Glide.with(this).load(restaurante.getmUrlImagen()).into(imgInfRestaurante);
        setContentFacebook();
        hideProgress();
        showElements();
    }

    private void setTitleRestaurante() {
        ((DetallesRestauranteActivity)getActivity()).onRestaurantReady(mRestaurante);
    }

    @Override
    public void onError(String error) {
        Snackbar.make(container, getString(R.string.restaurante_error_message), Snackbar.LENGTH_LONG).show();
        hideProgress();
        showElements();
    }

    @Override
    public void hideElements() {
        elementsInformationRestaurante.setVisibility(View.GONE);
    }

    @Override
    public void showElements() {
        elementsInformationRestaurante.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressInformationRestaurante.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressInformationRestaurante.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
