package com.javieralvarez.kinal.app_turismo.presenters;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;
import com.javieralvarez.kinal.app_turismo.interactors.UpdateInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.UpdateInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.UpdateView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 30/06/2016.
 */
public class UpdatePresenterImpl implements UpdatePresenter {

    private UpdateView view;
    private EventBus eventBus;
    private UpdateEvent.TIPO_UPDATE tipo_update;
    private UpdateInteractor interactor;

    public UpdatePresenterImpl(UpdateView view, Activity activity) {
        this.view = view;
        eventBus = GreenRobotEventBus.getInstance();
        interactor = new UpdateInteractorImpl(activity);
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void getUpdates(UpdateEvent.TIPO_UPDATE tipo_update) {
        this.tipo_update = tipo_update;
        interactor.execute(tipo_update);
    }

    @Override
    @Subscribe
    public void onEventMainThread(UpdateEvent event) {
        if(view != null) {
            if(event.getError() == null) {
                if(this.tipo_update == event.getTipo()) {
                    view.onUpdate(event.getUpdates());
                }
            } else {
                view.onError();
            }
        }
    }
}
