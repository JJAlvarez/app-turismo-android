package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.SucursalHotelEvent;
import com.javieralvarez.kinal.app_turismo.model.SucursalHotel;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 07/07/2016.
 */
public class SucursalesHotelRepositoryImpl implements SucursalesHotelRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public SucursalesHotelRepositoryImpl(Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
        PreferencesUtil.init(activity);
    }

    @Override
    public void getSucursales(int id) {
        app.getApi().getHotelSucursales(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<SucursalHotel>>() {
                    @Override
                    public void onResponse(Call<List<SucursalHotel>> call, Response<List<SucursalHotel>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<SucursalHotel>> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }

    private void post(List<SucursalHotel> sucursales) {
        SucursalHotelEvent event = new SucursalHotelEvent();
        event.setError(null);
        event.setHotels(sucursales);
        eventBus.post(event);
    }

    private void postError(String error) {
        SucursalHotelEvent event = new SucursalHotelEvent();
        event.setError(error);
        eventBus.post(event);
    }
}
