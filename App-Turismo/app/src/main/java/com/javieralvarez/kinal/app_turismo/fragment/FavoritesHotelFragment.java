package com.javieralvarez.kinal.app_turismo.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesHotelActivity;
import com.javieralvarez.kinal.app_turismo.adapter.FavoritesHotelsAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.HotelAdapter;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.presenters.FavoritesHotelsPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.FavoritesHotelsPresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.ClickHotelListener;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;
import com.javieralvarez.kinal.app_turismo.views.FavoritesHotelsView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesHotelFragment extends Fragment implements FavoritesHotelsView, ClickHotelListener, TelefonoClickListener {

    @Bind(R.id.hotel_rec_view)
    RecyclerView hotelRecView;
    @Bind(R.id.swipeContainerHotel)
    SwipeRefreshLayout swipeContainerHotel;
    @Bind(R.id.progressBarHoteles)
    ProgressBar progressBarHoteles;
    @Bind(R.id.container_hotel)
    RelativeLayout container;

    private FavoritesHotelsPresenter presenter;
    private FavoritesHotelsAdapter adapter;

    public FavoritesHotelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hotel, container, false);
        ButterKnife.bind(this, view);

        presenter = new FavoritesHotelsPresenterImpl(this);
        presenter.onCreate();
        setupRecycler();
        setupAdapter();
        setSwipeToRefresh();
        getFavoritesHotels();
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerHotel.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFavoritesHotels();
            }
        });

        swipeContainerHotel.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setupRecycler() {
        hotelRecView.setHasFixedSize(true);
    }

    private void setupAdapter() {
        adapter = new FavoritesHotelsAdapter(new ArrayList<Hotel>(), getContext(), this, this);
        hotelRecView.setAdapter(adapter);
        hotelRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void getFavoritesHotels() {
        presenter.getHoteles();
    }

    @Override
    public void onFavoritesReady(List<Hotel> hotels) {
        adapter.setFavorites(hotels);
        showElements();
        hideProgress();
        if(swipeContainerHotel.isRefreshing()) {
            swipeContainerHotel.setRefreshing(false);
        }
    }

    @Override
    public void showElements() {
        hotelRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        hotelRecView.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressBarHoteles.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarHoteles.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onClick(Hotel hotel) {
        Intent intent = new Intent(getActivity(), DetallesHotelActivity.class);
        intent.putExtra(HotelAdapter.HOTEL_ID_TAG, hotel.getmIdHotel());
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static FavoritesHotelFragment newInstance() {
        return new FavoritesHotelFragment();
    }

    @Override
    public void onTelefonoClick(int telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + Uri.encode(String.valueOf(telefono))));
        startActivity(intent);
    }
}
