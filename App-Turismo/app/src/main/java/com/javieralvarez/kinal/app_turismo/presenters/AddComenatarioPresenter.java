package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.ComentarioAddEvent;
import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;

/**
 * Created by Javier on 28/06/2016.
 */
public interface AddComenatarioPresenter {

    void onResume();
    void onPause();
    void onDestroy();

    void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario);
    void onEventMainThread(ComentarioAddEvent event);
}
