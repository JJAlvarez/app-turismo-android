package com.javieralvarez.kinal.app_turismo.interactors;

import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;

/**
 * Created by Javier on 30/06/2016.
 */
public interface UpdateInteractor {

    void execute(UpdateEvent.TIPO_UPDATE tipo_update);
}
