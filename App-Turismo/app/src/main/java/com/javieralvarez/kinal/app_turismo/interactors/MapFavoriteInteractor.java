package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 13/07/2016.
 */
public interface MapFavoriteInteractor {

    void getRestaurantes();
    void getHotels();
    void getLugares();

}
