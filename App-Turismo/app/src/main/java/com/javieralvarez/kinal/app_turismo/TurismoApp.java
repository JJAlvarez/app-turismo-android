package com.javieralvarez.kinal.app_turismo;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.javieralvarez.kinal.app_turismo.api.TurismoApi;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Javier on 28/04/2016.
 */
public class TurismoApp extends Application{

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "Ju1YQ0fiXlDhStAsjBxQsFtv9";
    private static final String TWITTER_SECRET = "cn6x1F1GoFSL0qMF1X4xvv33b4CkfOUJAyE24MhjZK6dgPj5AC";


    private static final String TAG = TurismoApp.class.getSimpleName();

    private TurismoApi mApi;

    @Override
    public void onCreate() {
        super.onCreate();
        initFabric();
        initDB();
        initFacebook();
        setRetrofit();
    }

    private void initFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

    private void setRetrofit() {
        Retrofit baseApi = new Retrofit.Builder()
                .baseUrl(TurismoApi.API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient())
                .build();
        mApi = baseApi.create(TurismoApi.class);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DBTearDown();
    }

    private void DBTearDown() {
        FlowManager.destroy();
    }

    private void initDB() {
        FlowManager.init(this);
    }

    private void initFabric() {
        TwitterAuthConfig authConfig = new TwitterAuthConfig(BuildConfig.TWITTER_KEY,
                BuildConfig.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
    }

    public TurismoApi getApi() {
        return this.mApi;
    }
}
