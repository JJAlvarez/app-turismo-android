package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.MapEvent;

/**
 * Created by Javier on 01/07/2016.
 */
public interface MapPresenter {

    void onCreate();
    void onDestroy();

    void getRestaurantesLocations();
    void getHotelsLocations();
    void getLuaresLocation();

    void onEventMainThread(MapEvent event);
}
