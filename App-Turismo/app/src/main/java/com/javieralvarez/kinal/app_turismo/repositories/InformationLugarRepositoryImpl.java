package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.InformationLugarEvent;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationLugarRepositoryImpl implements InformationLugarRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public InformationLugarRepositoryImpl(Activity activity) {
        PreferencesUtil.init(activity);
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
    }

    @Override
    public void getLugar(int id) {
        app.getApi().lugarPorId(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<LugarTuristico>() {
                    @Override
                    public void onResponse(Call<LugarTuristico> call, Response<LugarTuristico> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<LugarTuristico> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }

    private void post(LugarTuristico lugar) {
        InformationLugarEvent event = new InformationLugarEvent();
        event.setError(null);
        event.setLugares(lugar);
        eventBus.post(event);
    }

    private void postError(String error) {
        InformationLugarEvent event = new InformationLugarEvent();
        event.setError(error);
        eventBus.post(event);
    }
}
