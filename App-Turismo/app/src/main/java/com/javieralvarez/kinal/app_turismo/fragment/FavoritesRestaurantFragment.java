package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesRestauranteActivity;
import com.javieralvarez.kinal.app_turismo.adapter.FavoritesRestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.presenters.FavoritesRestaurantsPresenter;
import com.javieralvarez.kinal.app_turismo.presenters.FavoritesRestaurantsPresenterImpl;
import com.javieralvarez.kinal.app_turismo.utils.ClickRestauranteListener;
import com.javieralvarez.kinal.app_turismo.utils.TelefonoClickListener;
import com.javieralvarez.kinal.app_turismo.views.FavoritesRestaurantsView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesRestaurantFragment extends Fragment implements FavoritesRestaurantsView, ClickRestauranteListener, TelefonoClickListener {

    @butterknife.Bind(R.id.restaurant_rec_view)
    android.support.v7.widget.RecyclerView restaurantRecView;
    @butterknife.Bind(R.id.swipeContainerRestaurante)
    android.support.v4.widget.SwipeRefreshLayout swipeContainerRestaurante;
    @butterknife.Bind(R.id.progressBarRestaurantes)
    ProgressBar progressBarRestaurantes;
    @butterknife.Bind(R.id.container_restaurante)
    RelativeLayout container;

    private FavoritesRestaurantsPresenter presenter;
    private FavoritesRestaurantAdapter adapter;

    public FavoritesRestaurantFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);
        ButterKnife.bind(this, view);
        presenter = new FavoritesRestaurantsPresenterImpl(this);
        presenter.onCreate();
        setupRecycler();
        setupAdapter();
        setSwipeToRefresh();
        getFavoritesRestaurants();
        return view;
    }

    private void setSwipeToRefresh() {
        swipeContainerRestaurante.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFavoritesRestaurants();
            }
        });

        swipeContainerRestaurante.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    private void setupRecycler() {
        restaurantRecView.setHasFixedSize(true);
    }

    private void setupAdapter() {
        adapter = new FavoritesRestaurantAdapter(new ArrayList<Restaurante>(), getContext(), this, this);
        restaurantRecView.setAdapter(adapter);
        restaurantRecView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void getFavoritesRestaurants() {
        presenter.getRestaurantes();
    }

    @Override
    public void onFavoritesReady(List<Restaurante> restauranteList) {
        adapter.favorites(restauranteList);
        showElements();
        hideProgress();
        if(swipeContainerRestaurante.isRefreshing()) {
            swipeContainerRestaurante.setRefreshing(false);
        }
    }

    @Override
    public void showElements() {
        restaurantRecView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideElements() {
        restaurantRecView.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressBarRestaurantes.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarRestaurantes.setVisibility(View.GONE);
    }

    @Override
    public void onClick(Restaurante restaurante) {
        Intent intent = new Intent(getActivity(), DetallesRestauranteActivity.class);
        intent.putExtra(RestaurantAdapter.RESTAURANTE_ID_TAG, restaurante.getmIdRestaurante());
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static FavoritesRestaurantFragment newInstance() {
        return new FavoritesRestaurantFragment();
    }

    @Override
    public void onTelefonoClick(int telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:" + Uri.encode(String.valueOf(telefono))));
        intent.setData(Uri.parse("tel:" + Uri.encode("24494895")));
        startActivity(intent);
    }
}
