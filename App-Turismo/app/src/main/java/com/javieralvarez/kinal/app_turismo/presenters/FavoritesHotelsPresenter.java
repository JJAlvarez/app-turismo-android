package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.FavoritesHotelsEvent;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesHotelsPresenter {

    void onCreate();
    void onDestroy();

    void getHoteles();
    void onEventMainThread(FavoritesHotelsEvent event);
}
