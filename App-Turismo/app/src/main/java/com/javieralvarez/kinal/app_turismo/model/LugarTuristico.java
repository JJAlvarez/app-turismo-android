package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.javieralvarez.kinal.app_turismo.db.TurismoDataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Javier on 03/05/2016.
 */
@Table(database = TurismoDataBase.class)
public class LugarTuristico extends BaseModel implements Parcelable {

    @PrimaryKey
    @SerializedName("id_lugarturistico")
    public int mIdLugarturistico;

    @Column
    @SerializedName("nombre")
    public String mNombre;

    @Column
    @SerializedName("direccion")
    public String mDireccion;

    @Column
    @SerializedName("descripcion")
    public String mDescripcion;

    @Column
    @SerializedName("urlImagen")
    public String mUrlLogo;

    @Column
    @SerializedName("latitud")
    public float mLatitud;

    @Column
    @SerializedName("longitud")
    public float mLongitud;

    @Column
    @SerializedName("urlWeb")
    public String urlWeb;

    @Column
    public boolean favorite;

    @Column
    @SerializedName("departamentoIdDepartamento")
    public int mIdDepartamento;

    public LugarTuristico() {

    }

    public int getmIdLugarturistico() {
        return mIdLugarturistico;
    }

    public void setmIdLugarturistico(int mIdLugarturistico) {
        this.mIdLugarturistico = mIdLugarturistico;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public String getmDireccion() {
        return mDireccion;
    }

    public void setmDireccion(String mDireccion) {
        this.mDireccion = mDireccion;
    }

    public String getmDescripcion() {
        return mDescripcion;
    }

    public void setmDescripcion(String mDescripcion) {
        this.mDescripcion = mDescripcion;
    }

    public String getmUrlLogo() {
        return mUrlLogo;
    }

    public void setmUrlLogo(String mUrlLogo) {
        this.mUrlLogo = mUrlLogo;
    }

    public float getmLatitud() {
        return mLatitud;
    }

    public void setmLatitud(float mLatitud) {
        this.mLatitud = mLatitud;
    }

    public float getmLongitud() {
        return mLongitud;
    }

    public void setmLongitud(float mLongitud) {
        this.mLongitud = mLongitud;
    }

    public String getUrlWeb() {
        return urlWeb;
    }

    public void setUrlWeb(String urlWeb) {
        this.urlWeb = urlWeb;
    }

    public boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public int getmIdDepartamento() {
        return mIdDepartamento;
    }

    public void setmIdDepartamento(int mIdDepartamento) {
        this.mIdDepartamento = mIdDepartamento;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mIdLugarturistico);
        dest.writeString(mNombre);
        dest.writeString(mDireccion);
        dest.writeString(mDescripcion);
        dest.writeString(mUrlLogo);
        dest.writeFloat(this.mLatitud);
        dest.writeFloat(this.mLongitud);
        dest.writeString(this.urlWeb);
        dest.writeByte((byte) (favorite ? 1 : 0));
        dest.writeInt(mIdDepartamento);
    }

    protected LugarTuristico(Parcel in) {
        mIdLugarturistico = in.readInt();
        mNombre = in.readString();
        mDireccion = in.readString();
        mDescripcion = in.readString();
        mUrlLogo = in.readString();
        this.mLatitud = in.readFloat();
        this.mLongitud = in.readFloat();
        this.urlWeb = in.readString();
        this.favorite = in.readByte() != 0;
        mIdDepartamento = in.readInt();
    }

    public static final Creator<LugarTuristico> CREATOR = new Creator<LugarTuristico>() {
        @Override
        public LugarTuristico createFromParcel(Parcel in) {
            return new LugarTuristico(in);
        }

        @Override
        public LugarTuristico[] newArray(int size) {
            return new LugarTuristico[size];
        }
    };
}
