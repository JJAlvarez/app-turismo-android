package com.javieralvarez.kinal.app_turismo.interactors;

import com.javieralvarez.kinal.app_turismo.repositories.MapFavoriteRepository;
import com.javieralvarez.kinal.app_turismo.repositories.MapFavoriteRepositoryImpl;

/**
 * Created by Javier on 13/07/2016.
 */
public class MapFavoriteInteractorImpl implements MapFavoriteInteractor {

    private MapFavoriteRepository repository;

    public MapFavoriteInteractorImpl() {
        repository = new MapFavoriteRepositoryImpl();
    }

    @Override
    public void getRestaurantes() {
        repository.getRestaurantes();
    }

    @Override
    public void getHotels() {
        repository.getHoteles();
    }

    @Override
    public void getLugares() {
        repository.getLugares();
    }
}
