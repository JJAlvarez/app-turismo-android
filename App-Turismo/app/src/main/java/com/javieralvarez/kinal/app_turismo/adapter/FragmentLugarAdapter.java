package com.javieralvarez.kinal.app_turismo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.fragment.InformationLugarFragment;

/**
 * Created by Javier on 10/06/2016.
 */
public class FragmentLugarAdapter extends FragmentPagerAdapter {

    private int PAGE_COUNT = 2;

    private String tabTitles[] =
            new String[]{"Informacion", "Comentarios"};

    public FragmentLugarAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = InformationLugarFragment.newInstance();
                break;
            case 1:
                fragment = ComentariosFragment.newInstance(ComentariosFragment.TIPO_COMENTARIO.LUGAR_TURISTICO);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
