package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.ComentarioAddEvent;
import com.javieralvarez.kinal.app_turismo.events.ComentariosEvent;
import com.javieralvarez.kinal.app_turismo.events.DeleteComentarioEvent;
import com.javieralvarez.kinal.app_turismo.fragment.ComentariosFragment;
import com.javieralvarez.kinal.app_turismo.model.Comentarios;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 27/06/2016.
 */
public class ComentarioRepositoryImpl implements ComentarioRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public ComentarioRepositoryImpl(Activity activity) {
        eventBus = GreenRobotEventBus.getInstance();
        app = (TurismoApp)activity.getApplication();
        PreferencesUtil.init(activity.getApplicationContext());
    }

    @Override
    public void getComentarios(ComentariosFragment.TIPO_COMENTARIO tipo, int id) {
        switch (tipo) {
            case RESTAURANTE:
                getComentariosRestaurante(id);
                break;
            case HOTEL:
                getComentariosHotel(id);
                break;
            case LUGAR_TURISTICO:
                getComentariosLugares(id);
                break;
        }
    }

    @Override
    public void addComentario(ComentariosFragment.TIPO_COMENTARIO tipo, int id, String comentario) {
        switch (tipo) {
            case RESTAURANTE:
                addComentarioRestaurante(id, comentario);
                break;
            case HOTEL:
                addComentarioHotel(id, comentario);
                break;
            case LUGAR_TURISTICO:
                addComentarioLugar(id, comentario);
                break;
        }
    }

    @Override
    public void deleteComentario(Comentarios comentarios) {
        app.getApi().deleteComentario(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""),
                comentarios.getmIdComentario()).enqueue(new Callback<Comentarios>() {
            @Override
            public void onResponse(Call<Comentarios> call, Response<Comentarios> response) {
                postDelete();
            }

            @Override
            public void onFailure(Call<Comentarios> call, Throwable t) {
                postDeleteError(t.getLocalizedMessage());
            }
        });
    }

    private void addComentarioLugar(int id, String comentario) {
        app.getApi().addComentarioLugar(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id, comentario,
                PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_NAME, ""))
                .enqueue(new Callback<Comentarios>() {
                    @Override
                    public void onResponse(Call<Comentarios> call, Response<Comentarios> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<Comentarios> call, Throwable t) {
                        postAddError(t.getMessage());
                    }
                });
    }

    private void addComentarioHotel(int id, String comentario) {
        app.getApi().addComentarioHotel(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id, comentario,
                PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_NAME, ""))
                .enqueue(new Callback<Comentarios>() {
                    @Override
                    public void onResponse(Call<Comentarios> call, Response<Comentarios> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<Comentarios> call, Throwable t) {
                        postAddError(t.getMessage());
                    }
                });
    }

    private void addComentarioRestaurante(int id, final String comentario) {
        app.getApi().addComentarioRestaurante(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id, comentario,
                PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_NAME, ""))
                .enqueue(new Callback<Comentarios>() {
                    @Override
                    public void onResponse(Call<Comentarios> call, Response<Comentarios> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<Comentarios> call, Throwable t) {
                        postAddError(t.getMessage());
                    }
                });
    }

    private void post(List<Comentarios> comentarios) {
        ComentariosEvent event = new ComentariosEvent();
        event.setError(null);
        event.setComentariosList(comentarios);
        eventBus.post(event);
    }

    private void postError(String msg) {
        ComentariosEvent event = new ComentariosEvent();
        event.setError(msg);
        eventBus.post(event);
    }

    private void post(Comentarios comentarios) {
        ComentarioAddEvent event = new ComentarioAddEvent();
        event.setError(null);
        event.setComentarios(comentarios);
        eventBus.post(event);
    }

    private void postAddError(String msg) {
        ComentarioAddEvent event = new ComentarioAddEvent();
        event.setError(msg);
        eventBus.post(event);
    }

    private void postDelete() {
        DeleteComentarioEvent event = new DeleteComentarioEvent();
        event.setError(null);
        event.setSuccessful(true);
        eventBus.post(event);
    }

    private void postDeleteError(String error) {
        DeleteComentarioEvent event = new DeleteComentarioEvent();
        event.setError(error);
        event.setSuccessful(false);
        eventBus.post(event);
    }

    public void getComentariosRestaurante(int id) {
        app.getApi().getRestauranteComentarios(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<Comentarios>>() {
                    @Override
                    public void onResponse(Call<List<Comentarios>> call, Response<List<Comentarios>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Comentarios>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    public void getComentariosHotel(int id) {
        app.getApi().getHotelComentarios(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<Comentarios>>() {
                    @Override
                    public void onResponse(Call<List<Comentarios>> call, Response<List<Comentarios>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Comentarios>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    public void getComentariosLugares(int id) {
        app.getApi().getLugarComentarios(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<Comentarios>>() {
                    @Override
                    public void onResponse(Call<List<Comentarios>> call, Response<List<Comentarios>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Comentarios>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }
}
