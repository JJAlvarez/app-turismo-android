package com.javieralvarez.kinal.app_turismo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.javieralvarez.kinal.app_turismo.fragment.DepartamentoFragment;
import com.javieralvarez.kinal.app_turismo.fragment.HotelFragment;
import com.javieralvarez.kinal.app_turismo.fragment.LugarTuristicoFragment;
import com.javieralvarez.kinal.app_turismo.fragment.RestaurantFragment;

/**
 * Created by Javier on 15/05/2016.
 */
public class FragmentsAdapter extends FragmentStatePagerAdapter {

    private int PAGE_COUNT = 4;

    private String tabTitles[] =
            new String[] { "Departamentos", "Lugares", "Hoteles", "Restaurantes"};

    public FragmentsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = null;

        switch(position) {
            case 0:
                f = DepartamentoFragment.newInstance();
                break;
            case 1:
                f = LugarTuristicoFragment.newInstance();
                break;
            case 2:
                f = HotelFragment.newInstance();
                break;
            case 3:
                f = RestaurantFragment.newInstance();
                break;
        }

        return f;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
