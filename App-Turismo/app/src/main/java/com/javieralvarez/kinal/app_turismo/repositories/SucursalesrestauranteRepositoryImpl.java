package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.SucursalRestauranteEvent;
import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 06/07/2016.
 */
public class SucursalesrestauranteRepositoryImpl implements SucursalesRestauranteRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public SucursalesrestauranteRepositoryImpl(Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
        PreferencesUtil.init(activity);
    }

    @Override
    public void getSucursales(int id) {
        app.getApi().getRestaurantSucursales(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<SucursalRestaurante>>() {
                    @Override
                    public void onResponse(Call<List<SucursalRestaurante>> call, Response<List<SucursalRestaurante>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<SucursalRestaurante>> call, Throwable t) {
                        postError(t.getLocalizedMessage());
                    }
                });
    }

    private void post(List<SucursalRestaurante> sucursales) {
        SucursalRestauranteEvent event = new SucursalRestauranteEvent();
        event.setError(null);
        event.setSucursales(sucursales);
        eventBus.post(event);
    }

    private void postError(String error) {
        SucursalRestauranteEvent event = new SucursalRestauranteEvent();
        event.setError(error);
        eventBus.post(event);
    }
}
