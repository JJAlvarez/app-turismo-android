package com.javieralvarez.kinal.app_turismo.interactors;

import com.javieralvarez.kinal.app_turismo.repositories.FavoritesRestaurantsRepository;
import com.javieralvarez.kinal.app_turismo.repositories.FavoritesRestaurantsRepositoryImpl;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesRestaurantsInteractorImpl implements FavoritesRestaurantsInteractor {

    private FavoritesRestaurantsRepository repository;

    public FavoritesRestaurantsInteractorImpl() {
        repository = new FavoritesRestaurantsRepositoryImpl();
    }

    @Override
    public void execute() {
        repository.getRestaurants();
    }
}
