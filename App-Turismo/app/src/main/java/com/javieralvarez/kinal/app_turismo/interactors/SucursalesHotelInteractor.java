package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 07/07/2016.
 */
public interface SucursalesHotelInteractor {

    void execute(int id);
}
