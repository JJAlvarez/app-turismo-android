package com.javieralvarez.kinal.app_turismo.interactors;

import com.javieralvarez.kinal.app_turismo.repositories.FavoritesLugaresRepository;
import com.javieralvarez.kinal.app_turismo.repositories.FavoritesLugaresRepositoryImpl;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesLugaresInteractorImpl implements FavoritesLugaresInteractor {

    private FavoritesLugaresRepository repository;

    public FavoritesLugaresInteractorImpl() {
        this.repository = new FavoritesLugaresRepositoryImpl();
    }

    @Override
    public void execute() {
        repository.getPlaces();
    }
}
