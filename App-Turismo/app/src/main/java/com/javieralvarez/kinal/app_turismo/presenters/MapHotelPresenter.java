package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.MapHotelEvent;

/**
 * Created by Javier on 20/07/2016.
 */
public interface MapHotelPresenter {

    void onCreate();
    void onDestroy();
    void getSucursales(int idHotel);
    void onEventMainThread(MapHotelEvent event);
}
