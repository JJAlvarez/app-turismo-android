package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.MapHotelEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 20/07/2016.
 */
public class MapHotelRepositoryImpl implements MapHotelRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public MapHotelRepositoryImpl(Activity activity) {
        PreferencesUtil.init(activity);
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
    }

    @Override
    public void execute(int idHotel) {
        app.getApi().hotelPorId(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), idHotel)
                .enqueue(new Callback<Hotel>() {
                    @Override
                    public void onResponse(Call<Hotel> call, Response<Hotel> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<Hotel> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    private void postError(String msg) {
        MapHotelEvent event = new MapHotelEvent();
        event.setError(msg);
        eventBus.post(event);
    }

    private void post(Hotel hotel) {
        MapHotelEvent event = new MapHotelEvent();
        event.setHotel(hotel);
        event.setError(null);
        eventBus.post(event);
    }
}
