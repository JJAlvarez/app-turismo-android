package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.InformationRestauranteRepository;
import com.javieralvarez.kinal.app_turismo.repositories.InformationRestauranteRepositoryImpl;

/**
 * Created by Javier on 15/07/2016.
 */
public class InformationRestauranteInteractorImpl implements InformationRestauranteInteractor {

    private InformationRestauranteRepository repository;

    public InformationRestauranteInteractorImpl(Activity activity) {
        this.repository = new InformationRestauranteRepositoryImpl(activity);
    }

    @Override
    public void execute(int id) {
        repository.getResturante(id);
    }
}
