package com.javieralvarez.kinal.app_turismo.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.RestaurantAdapter;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.model.SucursalRestaurante;
import com.javieralvarez.kinal.app_turismo.presenters.MapRestaurantePresenter;
import com.javieralvarez.kinal.app_turismo.presenters.MapRestaurantePresenterImpl;
import com.javieralvarez.kinal.app_turismo.views.MapRestauranteView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapRestauranteActivity extends AppCompatActivity implements MapRestauranteView, OnMapReadyCallback {

    MapFragment supportMapFragment;
    @Bind(R.id.toolbar_map)
    Toolbar toolbarMap;
    @Bind(R.id.container_map)
    RelativeLayout containerMap;

    private GoogleMap mMap;

    private MapRestaurantePresenter presenter;
    private int idRestaurante;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        presenter = new MapRestaurantePresenterImpl(this, this);
        presenter.onCreate();
        supportMapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
        setSupportActionBar(toolbarMap);
        setupRestauranteId();
        getSucursales(idRestaurante);
    }

    private void setupRestauranteId() {
        if (getIntent().hasExtra(RestaurantAdapter.RESTAURANTE_ID_TAG)) {
            idRestaurante = getIntent().getIntExtra(RestaurantAdapter.RESTAURANTE_ID_TAG, 0);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap = googleMap;
    }

    @Override
    public void getSucursales(int id) {
        presenter.getSucursales(id);
    }

    @Override
    public void onSucursalesReady(Restaurante restaurante) {
        mMap.clear();
        for (SucursalRestaurante sucursal : restaurante.getmSucursales()) {
            LatLng RESTAURANTE = new LatLng(sucursal.getmLatitud(), sucursal.getmLongitud());
            mMap.addMarker(new MarkerOptions()
                    .position(RESTAURANTE)
                    .title(restaurante.getmNombre())
                    .snippet("Descripcion: " + restaurante.getmDescripcion()));
        }
        getSupportActionBar().setTitle(restaurante.getmNombre());
    }

    @Override
    public void onError(String error) {
        Snackbar.make(containerMap, getString(R.string.sucursales_error), Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
