package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public interface FavoritesLugaresView {

    void getFavoritesPlaces();
    void onFavoritesReady(List<LugarTuristico> lugares);

    void showElements();
    void hideElements();
    void showProgress();
    void hideProgress();
}
