package com.javieralvarez.kinal.app_turismo.repositories;

import com.javieralvarez.kinal.app_turismo.events.FavoritesRestaurantsEvent;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.raizlabs.android.dbflow.list.FlowCursorList;

import java.util.List;

/**
 * Created by Javier on 03/07/2016.
 */
public class FavoritesRestaurantsRepositoryImpl implements FavoritesRestaurantsRepository {

    private EventBus eventBus;
    private FavoritesRestaurantsEvent event;

    public FavoritesRestaurantsRepositoryImpl() {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.event = new FavoritesRestaurantsEvent();
    }

    @Override
    public void getRestaurants() {
        FlowCursorList<Restaurante> storedRecipes = new FlowCursorList<Restaurante>(false, Restaurante.class);
        post(storedRecipes.getAll());
    }

    private void post(List<Restaurante> restaurantes) {
        event.setError(null);
        event.setRestaurantes(restaurantes);
        eventBus.post(event);
    }
}
