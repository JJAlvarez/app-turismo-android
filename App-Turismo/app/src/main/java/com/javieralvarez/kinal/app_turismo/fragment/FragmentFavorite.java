package com.javieralvarez.kinal.app_turismo.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.MapFavoriteActivity;
import com.javieralvarez.kinal.app_turismo.adapter.FavoritesFragmentAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFavorite extends Fragment {


    @Bind(R.id.toolbar_main)
    Toolbar toolbarMain;
    @Bind(R.id.appbartabs)
    TabLayout appbartabs;
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.launch_map_button)
    FloatingActionButton launchMapButton;
    @Bind(R.id.main_content)
    FrameLayout mainContent;

    public FragmentFavorite() {
        // Required empty public constructor
    }


    public static FragmentFavorite newInstance() {
        return new FragmentFavorite();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);
        ButterKnife.bind(this, view);
        setViewPager();
        setTabs();
        return view;
    }

    private void setTabs() {
        appbartabs.setTabMode(TabLayout.MODE_FIXED);
        appbartabs.setupWithViewPager(viewpager);
    }

    private void setViewPager() {
        viewpager.setOffscreenPageLimit(3);
        viewpager.setAdapter(new FavoritesFragmentAdapter(
                getActivity().getSupportFragmentManager()));
    }

    @OnClick(R.id.launch_map_button)
    public void onClick() {
        Intent mapIntent = new Intent(getActivity(), MapFavoriteActivity.class);
        startActivity(mapIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
