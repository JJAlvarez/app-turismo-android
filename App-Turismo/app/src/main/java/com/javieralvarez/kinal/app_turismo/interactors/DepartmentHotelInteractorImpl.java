package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.DepartmentHotelRepository;
import com.javieralvarez.kinal.app_turismo.repositories.DepartmentHotelRepositoryImpl;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentHotelInteractorImpl implements DepartmentHotelInteractor {

    private DepartmentHotelRepository repository;

    public DepartmentHotelInteractorImpl(Activity activity) {
        this.repository = new DepartmentHotelRepositoryImpl(activity);
    }

    @Override
    public void execute(int id) {
        repository.getHotels(id);
    }
}
