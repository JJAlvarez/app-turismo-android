package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 29/04/2016.
 */
public class ServerResponse implements Parcelable {

    @SerializedName("error")
    private boolean mError;

    @SerializedName("message")
    private String mMessage;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(mError ? (byte) 1 : (byte) 0);
        dest.writeString(this.mMessage);
    }

    public ServerResponse() {
    }

    protected ServerResponse(Parcel in) {
        this.mError = in.readByte() != 0;
        this.mMessage = in.readString();
    }

    public static final Parcelable.Creator<ServerResponse> CREATOR = new Parcelable.Creator<ServerResponse>() {
        public ServerResponse createFromParcel(Parcel source) {
            return new ServerResponse(source);
        }

        public ServerResponse[] newArray(int size) {
            return new ServerResponse[size];
        }
    };
}
