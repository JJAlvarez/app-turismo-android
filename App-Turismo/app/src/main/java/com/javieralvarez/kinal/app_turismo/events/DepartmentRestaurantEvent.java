package com.javieralvarez.kinal.app_turismo.events;

import com.javieralvarez.kinal.app_turismo.model.Restaurante;

import java.util.List;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentRestaurantEvent {

    private String error;
    private List<Restaurante> restaurantes;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Restaurante> getRestaurantes() {
        return restaurantes;
    }

    public void setRestaurantes(List<Restaurante> restaurantes) {
        this.restaurantes = restaurantes;
    }
}
