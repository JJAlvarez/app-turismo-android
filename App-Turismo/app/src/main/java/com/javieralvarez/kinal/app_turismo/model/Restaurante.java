package com.javieralvarez.kinal.app_turismo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.javieralvarez.kinal.app_turismo.db.TurismoDataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

/**
 * Created by Javier on 27/04/2016.
 */
@Table(database = TurismoDataBase.class)
public class Restaurante extends BaseModel implements Parcelable{

    @PrimaryKey
    @SerializedName("id_restaurante")
    public int mIdRestaurante;

    @Column
    @SerializedName("nombre")
    public String mNombre;

    @Column
    @SerializedName("clasificacion")
    public int mClasificacion;

    @Column
    @SerializedName("descripcion")
    public String mDescripcion;

    @Column
    @SerializedName("pbx")
    public int mPbx;

    @Column
    @SerializedName("urlLogo")
    public String mUrlLogo;

    @Column
    @SerializedName("urlImagen")
    public String mUrlImagen;

    @Column
    @SerializedName("urlWeb")
    public String urlWeb;

    @Column
    public boolean favorite;

    @SerializedName("tipocomidas")
    public List<TipoComida> mTiposComida;

    @SerializedName("sucursalrestaurantes")
    public List<SucursalRestaurante> mSucursales;

    public Restaurante() {
    }

    public int getmIdRestaurante() {
        return mIdRestaurante;
    }

    public void setmIdRestaurante(int mIdRestaurante) {
        this.mIdRestaurante = mIdRestaurante;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public int getmClasificacion() {
        return mClasificacion;
    }

    public void setmClasificacion(int mClasificacion) {
        this.mClasificacion = mClasificacion;
    }

    public int getmPbx() {
        return mPbx;
    }

    public void setmPbx(int mPbx) {
        this.mPbx = mPbx;
    }

    public String getmUrlLogo() {
        return mUrlLogo;
    }

    public void setmUrlLogo(String mUrlLogo) {
        this.mUrlLogo = mUrlLogo;
    }

    public String getmUrlImagen() {
        return mUrlImagen;
    }

    public void setmUrlImagen(String mUrlImagen) {
        this.mUrlImagen = mUrlImagen;
    }

    public String getmDescripcion() {
        return mDescripcion;
    }

    public void setmDescripcion(String mDescripcion) {
        this.mDescripcion = mDescripcion;
    }

    public String getUrlWeb() {
        return urlWeb;
    }

    public void setUrlWeb(String urlWeb) {
        this.urlWeb = urlWeb;
    }

    public boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public List<TipoComida> getmTiposComida() {
        return mTiposComida;
    }

    public void setmTiposComida(List<TipoComida> mTiposComida) {
        this.mTiposComida = mTiposComida;
    }

    public List<SucursalRestaurante> getmSucursales() {
        return mSucursales;
    }

    public void setmSucursales(List<SucursalRestaurante> mSucursales) {
        this.mSucursales = mSucursales;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mIdRestaurante);
        dest.writeString(this.mNombre);
        dest.writeInt(this.mClasificacion);
        dest.writeString(this.mDescripcion);
        dest.writeInt(this.mPbx);
        dest.writeString(this.mUrlLogo);
        dest.writeString(this.mUrlImagen);
        dest.writeString(this.urlWeb);
        dest.writeByte((byte) (favorite ? 1 : 0));
        dest.writeTypedList(mTiposComida);
        dest.writeTypedList(mSucursales);
    }

    protected Restaurante(Parcel in) {
        this.mIdRestaurante = in.readInt();
        this.mNombre = in.readString();
        this.mClasificacion = in.readInt();
        this.mDescripcion = in.readString();
        this.mPbx = in.readInt();
        this.mUrlImagen = in.readString();
        this.mUrlLogo = in.readString();
        this.urlWeb = in.readString();
        this.favorite = in.readByte() != 0;
        this.mTiposComida = in.createTypedArrayList(TipoComida.CREATOR);
        this.mSucursales = in.createTypedArrayList(SucursalRestaurante.CREATOR);
    }

    public static final Parcelable.Creator<Restaurante> CREATOR = new Parcelable.Creator<Restaurante>() {
        @Override
        public Restaurante createFromParcel(Parcel source) {
            return new Restaurante(source);
        }

        @Override
        public Restaurante[] newArray(int size) {
            return new Restaurante[size];
        }
    };
}
