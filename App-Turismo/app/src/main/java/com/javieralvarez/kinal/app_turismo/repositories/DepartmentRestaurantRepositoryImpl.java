package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.DepartmentRestaurantEvent;
import com.javieralvarez.kinal.app_turismo.model.Restaurante;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentRestaurantRepositoryImpl implements DepartmentRestaurantRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public DepartmentRestaurantRepositoryImpl(Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        this.app = (TurismoApp)activity.getApplication();
        PreferencesUtil.init(activity);
    }
    @Override
    public void getRestaurantes(int id) {
        app.getApi().getRestaurantesDepartamento(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<Restaurante>>() {
                    @Override
                    public void onResponse(Call<List<Restaurante>> call, Response<List<Restaurante>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Restaurante>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    private void post(List<Restaurante> restaurantes) {
        DepartmentRestaurantEvent event = new DepartmentRestaurantEvent();
        event.setError(null);
        event.setRestaurantes(restaurantes);
        eventBus.post(event);
    }

    private void postError(String msg) {
        DepartmentRestaurantEvent event = new DepartmentRestaurantEvent();
        event.setError(msg);
        eventBus.post(event);
    }
}
