package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.SucursalRestauranteEvent;

/**
 * Created by Javier on 06/07/2016.
 */
public interface SucursalesRestaurantePresenter {

    void onCreate();
    void onDestroy();
    void getSucursales(int id);
    void onEventMainThread(SucursalRestauranteEvent event);
}
