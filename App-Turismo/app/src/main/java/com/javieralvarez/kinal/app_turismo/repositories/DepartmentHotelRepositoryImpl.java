package com.javieralvarez.kinal.app_turismo.repositories;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.TurismoApp;
import com.javieralvarez.kinal.app_turismo.events.DepartmentHotelEvent;
import com.javieralvarez.kinal.app_turismo.model.Hotel;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.utils.PreferencesUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Javier on 02/07/2016.
 */
public class DepartmentHotelRepositoryImpl implements DepartmentHotelRepository {

    private EventBus eventBus;
    private TurismoApp app;

    public DepartmentHotelRepositoryImpl(Activity activity) {
        this.eventBus = GreenRobotEventBus.getInstance();
        app = (TurismoApp)activity.getApplication();
        PreferencesUtil.init(activity);
    }

    @Override
    public void getHotels(int id) {
        app.getApi().getHotelesDepartamento(PreferencesUtil.getStringPreference(PreferencesUtil.PREF_USER_TOKEN, ""), id)
                .enqueue(new Callback<List<Hotel>>() {
                    @Override
                    public void onResponse(Call<List<Hotel>> call, Response<List<Hotel>> response) {
                        post(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<Hotel>> call, Throwable t) {
                        postError(t.getMessage());
                    }
                });
    }

    private void post(List<Hotel> hoteles) {
        DepartmentHotelEvent event = new DepartmentHotelEvent();
        event.setError(null);
        event.setHoteles(hoteles);
        eventBus.post(event);
    }

    private void postError(String msg) {
        DepartmentHotelEvent event = new DepartmentHotelEvent();
        event.setError(msg);
        eventBus.post(event);
    }
}
