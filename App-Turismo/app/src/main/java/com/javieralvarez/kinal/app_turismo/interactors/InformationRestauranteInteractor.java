package com.javieralvarez.kinal.app_turismo.interactors;

/**
 * Created by Javier on 15/07/2016.
 */
public interface InformationRestauranteInteractor {

    void execute(int id);
}
