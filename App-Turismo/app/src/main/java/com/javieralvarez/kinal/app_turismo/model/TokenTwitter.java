package com.javieralvarez.kinal.app_turismo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Javier on 25/06/2016.
 */
public class TokenTwitter {

    @SerializedName("token")
    private String mToken;

    @SerializedName("token_twitter")
    private boolean mTokenTwitter;

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }

    public boolean ismTokenTwitter() {
        return mTokenTwitter;
    }

    public void setmTokenTwitter(boolean mTokenTwitter) {
        this.mTokenTwitter = mTokenTwitter;
    }

    public TokenTwitter() {
    }
}
