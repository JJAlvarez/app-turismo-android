package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.events.UpdateEvent;
import com.javieralvarez.kinal.app_turismo.repositories.UpdateRepository;
import com.javieralvarez.kinal.app_turismo.repositories.UpdateRepositoryImpl;

/**
 * Created by Javier on 30/06/2016.
 */
public class UpdateInteractorImpl implements UpdateInteractor {
    private UpdateRepository repository;

    public UpdateInteractorImpl(Activity activity) {
        this.repository = new UpdateRepositoryImpl(activity);
    }

    @Override
    public void execute(UpdateEvent.TIPO_UPDATE tipo_update) {
        repository.getUpdates(tipo_update);
    }
}
