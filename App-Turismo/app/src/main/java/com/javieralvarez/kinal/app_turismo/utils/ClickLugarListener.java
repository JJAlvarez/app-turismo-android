package com.javieralvarez.kinal.app_turismo.utils;

import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;

/**
 * Created by Javier on 04/07/2016.
 */
public interface ClickLugarListener {

    void onClick(LugarTuristico lugarTuristico);
}
