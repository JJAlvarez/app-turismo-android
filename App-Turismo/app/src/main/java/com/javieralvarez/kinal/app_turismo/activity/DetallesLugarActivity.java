package com.javieralvarez.kinal.app_turismo.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.adapter.FragmentLugarAdapter;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico;
import com.javieralvarez.kinal.app_turismo.model.LugarTuristico_Table;
import com.raizlabs.android.dbflow.sql.language.Select;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetallesLugarActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @Bind(R.id.mapa_lugar)
    FloatingActionButton mapaLugar;
    @Bind(R.id.appbartabs_lugar)
    TabLayout tabLayout;
    @Bind(R.id.viewpager_lugar)
    ViewPager viewPager;
    @Bind(R.id.toolbar_detalles_lugar)
    Toolbar mToolbar;
    private LugarTuristico mLugar;
    private static final int PAGE_COMENTARIOS = 1;
    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles_lugar);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setViewPager();
        setTabs();
        mapaLugar.setEnabled(false);
    }

    private void setTabs() {
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setViewPager() {
        viewPager.setAdapter(new FragmentLugarAdapter(
                getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(this);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == PAGE_COMENTARIOS) {
            mapaLugar.hide();
        } else {
            mapaLugar.show();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        if (mLugar != null) {
            getMenuInflater().inflate(R.menu.description_menu, menu);
            MenuItem menuItem = menu.findItem(R.id.action_do_favorite);
            menuItem.setIcon(mLugar.getFavorite() ? R.drawable.ic_favorite : R.drawable.ic_unfavorite);
        }
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_do_favorite) {
            if (mLugar != null) {
                if (mLugar.getFavorite()) {
                    item.setIcon(R.drawable.ic_unfavorite);
                    mLugar.setFavorite(false);
                    deleteFavorite();
                } else {
                    item.setIcon(R.drawable.ic_favorite);
                    mLugar.setFavorite(true);
                    saveFavorite();
                }
            }

            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteFavorite() {
        mLugar.delete();
    }

    private void saveFavorite() {
        mLugar.save();
    }

    private boolean getDatabaseReferenceObject() {
        LugarTuristico lugar = new Select().from(LugarTuristico.class).where(LugarTuristico_Table.mIdLugarturistico
                .is(mLugar.getmIdLugarturistico())).querySingle();
        return lugar == null;
    }

    private void setFavorite() {
        if (getDatabaseReferenceObject()) {
            mLugar.setFavorite(false);
        } else {
            mLugar.setFavorite(true);
        }
    }

    public void onLugarReady(LugarTuristico lugarTuristico) {
        mLugar = lugarTuristico;
        getSupportActionBar().setTitle(mLugar.getmNombre());
        setFavorite();
        invalidateOptionsMenu();
        onCreateOptionsMenu(mMenu);
        mapaLugar.setEnabled(true);
    }

    @OnClick(R.id.mapa_lugar)
    public void onClick() {
        try
        {
            String url = "waze://?ll=" + mLugar.getmLatitud() + "," + mLugar.getmLongitud() + "&z=10&navigate=yes";
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
            startActivity( intent );
        }
        catch ( ActivityNotFoundException ex  )
        {
            Intent intent =
                    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
            startActivity(intent);
        }
    }
}
