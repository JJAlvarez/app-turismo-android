package com.javieralvarez.kinal.app_turismo.presenters;

import com.javieralvarez.kinal.app_turismo.events.MapEvent;
import com.javieralvarez.kinal.app_turismo.interactors.MapFavoriteInteractor;
import com.javieralvarez.kinal.app_turismo.interactors.MapFavoriteInteractorImpl;
import com.javieralvarez.kinal.app_turismo.utils.EventBus;
import com.javieralvarez.kinal.app_turismo.utils.GreenRobotEventBus;
import com.javieralvarez.kinal.app_turismo.views.MapFavoriteView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Javier on 13/07/2016.
 */
public class MapFavoritePresenterImpl implements MapFavoritePresenter {

    private EventBus eventBus;
    private MapFavoriteInteractor interactor;
    private MapFavoriteView view;

    public MapFavoritePresenterImpl(MapFavoriteView view) {
        this.view = view;
        this.eventBus = GreenRobotEventBus.getInstance();
        this.interactor = new MapFavoriteInteractorImpl();
    }

    @Override
    public void onCreate() {
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
        if(view != null) {
            view = null;
        }
    }

    @Override
    public void getRestaurantes() {
        interactor.getRestaurantes();
    }

    @Override
    public void getHoteles() {
        interactor.getHotels();
    }

    @Override
    public void getLugares() {
        interactor.getLugares();
    }

    @Override
    @Subscribe
    public void onEventMainThread(MapEvent event) {
        if(event.getError() == null) {
            switch (event.getTipo_mapa()) {
                case MAPA_RESTAURANTE:
                    view.onRestaurantesReady(event.getRestaurantes());
                    break;
                case MAPA_HOTEL:
                    view.onHotelesReady(event.getHotels());
                    break;
                case MAPA_LUGAR:
                    view.onLugaresReady(event.getLugarTuristicos());
                    break;
            }
        } else {
            view.onError(event.getError());
        }
    }
}
