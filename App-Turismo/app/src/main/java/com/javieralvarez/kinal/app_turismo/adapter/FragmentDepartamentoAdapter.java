package com.javieralvarez.kinal.app_turismo.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.javieralvarez.kinal.app_turismo.fragment.DescriptionDepartmentFragment;
import com.javieralvarez.kinal.app_turismo.fragment.HotelDepartmentFragment;
import com.javieralvarez.kinal.app_turismo.fragment.LugaresDepartmentFragment;
import com.javieralvarez.kinal.app_turismo.fragment.RestaurantDepartmentFragmnet;

/**
 * Created by Javier on 03/07/2016.
 */
public class FragmentDepartamentoAdapter extends FragmentPagerAdapter {

    private int PAGE_COUNT = 4;

    private String tabTitles[] =
            new String[] { "Informacion", "Hoteles", "Restaurantes", "Lugares"};

    public FragmentDepartamentoAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = DescriptionDepartmentFragment.newInstance();
                break;
            case 1:
                fragment = HotelDepartmentFragment.newInstance();
                break;
            case 2:
                fragment = RestaurantDepartmentFragmnet.newInstance();
                break;
            case 3:
                fragment = LugaresDepartmentFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
