package com.javieralvarez.kinal.app_turismo.interactors;

import android.app.Activity;

import com.javieralvarez.kinal.app_turismo.repositories.InformationLugarRepository;
import com.javieralvarez.kinal.app_turismo.repositories.InformationLugarRepositoryImpl;

/**
 * Created by Javier on 14/07/2016.
 */
public class InformationLugarInteractorImpl implements InformationLugarInteractor {

    private InformationLugarRepository repository;

    public InformationLugarInteractorImpl(Activity activity) {
        this.repository = new InformationLugarRepositoryImpl(activity);
    }

    @Override
    public void execute(int id) {
        repository.getLugar(id);
    }
}
