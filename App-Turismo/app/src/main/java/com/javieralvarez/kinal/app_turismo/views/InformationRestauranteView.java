package com.javieralvarez.kinal.app_turismo.views;

import com.javieralvarez.kinal.app_turismo.model.Restaurante;

/**
 * Created by Javier on 15/07/2016.
 */
public interface InformationRestauranteView {

    void getRestaurante(int id);
    void onRestauranteReady(Restaurante restaurante);
    void onError(String error);

    void hideElements();
    void showElements();
    void hideProgress();
    void showProgress();
}
