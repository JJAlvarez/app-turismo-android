package com.javieralvarez.kinal.app_turismo.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.javieralvarez.kinal.app_turismo.R;
import com.javieralvarez.kinal.app_turismo.activity.DetallesDepartamentoActivity;
import com.javieralvarez.kinal.app_turismo.model.Departamento;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javier on 12/05/2016.
 */
public class DepartamentoAdapter extends RecyclerView.Adapter<DepartamentoAdapter.DepartamentoViewHolder> {

    private Context mContext;
    private ArrayList<Departamento> mData;
    public static String DEPARTAMENTO_DETALLES_TAG = "departamento";
    private int lastPosition = -1;

    public DepartamentoAdapter(Context context, ArrayList<Departamento> data) {
        this.mData = data;
        this.mContext = context;
    }

    @Override
    public DepartamentoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_departamento, parent, false);

        itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(mContext, DetallesDepartamentoActivity.class);
                mContext.startActivity(intent);
                return true;
            }
        });

        DepartamentoViewHolder hvh = new DepartamentoViewHolder(itemView);

        return hvh;
    }

    @Override
    public void onBindViewHolder(DepartamentoViewHolder holder, int position) {
        Departamento item = mData.get(position);

        setAnimation(holder.mContainer, position);
        holder.bindDepartamento(item);
    }

    @Override
    public void onViewDetachedFromWindow(DepartamentoViewHolder holder) {
        ((DepartamentoViewHolder)holder).clearAnimation();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            animation.setDuration(500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Departamento> items) {
        mData.addAll(items);
        notifyDataSetChanged();
    }

    public class DepartamentoViewHolder extends RecyclerView.ViewHolder {

        private ImageView mLogo;
        private TextView mLblNombre;
        private TextView mLblDescripcion;
        private CardView mCardView;
        private FrameLayout mContainer;

        public DepartamentoViewHolder(View itemView) {
            super(itemView);
            this.mCardView = (CardView) itemView.findViewById(R.id.card_view_departamento);
            this.mLogo = (ImageView) itemView.findViewById(R.id.img_departamento);
            this.mLblNombre = (TextView) itemView.findViewById(R.id.lbl_nombre);
            this.mLblDescripcion = (TextView) itemView.findViewById(R.id.lbl_descripcion);
            this.mContainer = (FrameLayout) itemView.findViewById(R.id.frame_departamento);
        }

        public void bindDepartamento(final Departamento departamento) {
            mLblNombre.setText(departamento.getmNombre());
            this.mLblDescripcion.setText(departamento.getmDescripcion());
            Glide.with(mContext).load(departamento.getmUrlImagen()).into(mLogo);

            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, DetallesDepartamentoActivity.class);
                    intent.putExtra(DEPARTAMENTO_DETALLES_TAG, departamento);
                    mContext.startActivity(intent);
                }
            });
        }

        public void clearAnimation()
        {
            mContainer.clearAnimation();
        }
    }

}
